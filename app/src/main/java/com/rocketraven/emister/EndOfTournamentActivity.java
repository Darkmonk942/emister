package com.rocketraven.emister;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.rocketraven.emister.ads.ShowAds;
import com.rocketraven.emister.objects.AuthObject;
import com.rocketraven.emister.objects.EndOfTournamentObject;
import com.rocketraven.emister.optimization.Optimization;
import com.rocketraven.emister.retrofit.RetrofitRequestCallback;
import com.rocketraven.emister.retrofit.requests.PostUserScore;
import com.rocketraven.emister.retrofit.requests.RetrofitMainClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Darkmonk on 28.06.2016.
 */
public class EndOfTournamentActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.rate_frame1) FrameLayout rateFrame1;
    @BindView(R.id.rate_frame2) FrameLayout rateFrame2;
    @BindView(R.id.rate_frame3) FrameLayout rateFrame3;
    @BindView(R.id.rate_frame4) FrameLayout rateFrame4;
    // ImageView
    @BindView(R.id.first_place_icon) ImageView firstPlaceIcon;
    @BindView(R.id.second_place_icon) ImageView secondPlaceIcon;
    @BindView(R.id.third_place_icon) ImageView thirdPlaceIcon;
    @BindView(R.id.icon_face_mini1) ImageView iconFace1;
    @BindView(R.id.icon_face_mini2) ImageView iconFace2;
    @BindView(R.id.icon_face_mini3) ImageView iconFace3;
    @BindView(R.id.icon_face_mini4) ImageView iconFace4;
    @BindView(R.id.trophy_icon1) ImageView trophyIcon1;
    @BindView(R.id.trophy_icon2) ImageView trophyIcon2;
    @BindView(R.id.trophy_icon3) ImageView trophyIcon3;
    // TextView
    @BindView(R.id.icon_face_name1) TextView iconName1;
    @BindView(R.id.icon_face_name2) TextView iconName2;
    @BindView(R.id.icon_face_name3) TextView iconName3;
    @BindView(R.id.icon_face_name4) TextView iconName4;
    @BindView(R.id.icon_face_score1) TextView iconScore1;
    @BindView(R.id.icon_face_score2) TextView iconScore2;
    @BindView(R.id.icon_face_score3) TextView iconScore3;
    @BindView(R.id.icon_face_score4) TextView iconScore4;
    @BindView(R.id.trophy_count_text1) TextView trophyCountText1;
    @BindView(R.id.trophy_count_text2) TextView trophyCountText2;
    @BindView(R.id.trophy_count_text3) TextView trophyCountText3;
    // Button
    @BindView(R.id.btn_main_menu) Button mainMenuBut;
    @BindView(R.id.btn_restart) Button restartBut;
    @BindView(R.id.btn_top_players) Button topPlayerBut;
    Button lastButton=null;
    // Примитивы
    RetrofitMainClass retrofitMainClass;
    PostUserScore postUserScore;
    String token;
    private RewardedVideoAd mAd;
    ShowAds showAds;
    boolean adsCount=true;
    boolean premiumUser;
    public static final String TOURNAMENT_TOP_PLAYER="top_players";
    int playerPoints=0;
    int winScore=0;
    Handler handler=new Handler();
    int bangCount=0;
    ArrayList<ImageView> allFaceImage=new ArrayList();
    ArrayList<Integer> allFaceIcon=new ArrayList();
    EndOfTournamentObject allPlayers[];
    boolean soundOn;
    EverydayTasksCheck taskCheck;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_of_tournament);
        ButterKnife.bind(this);

        // Отправляем статистику по завершению турнира
        metricaEvent("Окончание турнира",0);

        // Инициализация ретрофита
        retrofitMainClass=new RetrofitMainClass(this);
        postUserScore=retrofitMainClass.retrofit.create(PostUserScore.class);

        // Получаем токен
        token=Paper.book().read("token","");

        // Проверка на премиум
        premiumUser=Paper.book("inapp").read("premium",false);

        // Определяем выполнение ежедневных заданий
        taskCheck=new EverydayTasksCheck();

        // Проверка на прохождение любого турнира
        taskCheck.checkTask(5);

        // Проверка на прохождение обычного/рейтингового турнира
        if(getIntent().getStringExtra("gameType").equals("common")){
            taskCheck.checkTask(7);
        }else{
            taskCheck.checkTask(6);
        }

        // Инициализация admob (Проверка на премиум)
            showAds = new ShowAds();
            showAds.loadInterstitial(this, getResources().getString(R.string.admob_interstitial_key));
            showAds.mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    lastButton.callOnClick();
                }
            });
            mAd = MobileAds.getRewardedVideoAdInstance(this);
            showAds.rewardedVideo(mAd,getResources().getString(R.string.admob_rewarded_video_key));


        // Оптимизация интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(mainFrame);
        optimization.Optimization(rateFrame1);
        optimization.Optimization(rateFrame2);
        optimization.Optimization(rateFrame3);
        optimization.Optimization(rateFrame4);

        // Определяем параметры звука
        soundOn=Paper.book("settings").read("sound",true);

        // Запускаем плеер с аплодисментами
        MediaPlayer mPlayer  = MediaPlayer.create(this, R.raw.apploud);


        // Запускаем фоновую музыку
        MediaPlayer mPlayerBack  = MediaPlayer.create(this, R.raw.sound_end_screen);

        if(soundOn) {
            mPlayer.start();
            mPlayerBack.start();
        }

        // Получаем очки и места всех участников
        allPlayers= Paper.book().read("tournamentEnd");

        // Отправляем на сервер очки текущего пользователя при рейтинговом турнире
        sendPointsToBD();

        // Создаём листы
        createLists();

        // SmallBang
        starBang();
    }

    // Появление иконок победителей
    public void starBang(){

        handler.postDelayed(new Runnable() {
            public void run() {
                allFaceImage.get(bangCount).setImageResource(allFaceIcon.get(bangCount));
                bangCount=bangCount+1;
                if(bangCount!=3){
                    starBang();
                }

            }
        }, 50);
    }

    // Переход на главный экран
    public void mainScreenClick(View v){
        if(showAds.mInterstitialAd.isLoaded()&&adsCount&&!premiumUser){
           showAds.showInterstitial();
            adsCount=false;
            lastButton=mainMenuBut;
        }else {
            startActivity(new Intent(this, MainActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
    }

    // Рестарт турнира
    public void restartTournamentClick(View v){
        if(showAds.mInterstitialAd.isLoaded()&&adsCount&&!premiumUser){
            showAds.showInterstitial();
            adsCount=false;
            lastButton=restartBut;
        }else {
            // Проверка на тип текущего уровня
            if(getIntent().getStringExtra("gameType").equals("common")) {
                startActivity(new Intent(this, TournamentPendingActivity.class)
                        .putExtra("gameType","common"));
                finish();
            }else{
                // Проверка на премиум при рейтинговом
                if(premiumUser){
                    startActivity(new Intent(this, TournamentPendingActivity.class)
                            .putExtra("gameType","rate"));
                    finish();
                }else{
                    // Сверяем кол-во оставшихся рейтинговых игр
                    if(Paper.book("tournament").read("rate_count",3)!=0){
                        Paper.book("tournament").write("rate_count",
                                Paper.book("tournament").read("rate_count",3)-1);
                        startActivity(new Intent(this, TournamentPendingActivity.class)
                                .putExtra("gameType","rate"));
                        finish();
                    }else{
                        // Если рейтинговых игр не осталось - то показываем диалог
                        showAlertRates();
                    }
                }
            }
        }
    }

    // Таблица рекордов
    public void recordsTableClick(View v){
        if(showAds.mInterstitialAd.isLoaded()&&adsCount&&!premiumUser){
            showAds.showInterstitial();
            adsCount=false;
            lastButton=topPlayerBut;
        }else {
            startActivity(new Intent(this, TournamentTopPlayersActivity.class));
            finish();
        }
    }

    // Заполняем листы и подгружаем результаты
    public void createLists(){
        allFaceImage.add(firstPlaceIcon); allFaceImage.add(secondPlaceIcon);
        allFaceImage.add(thirdPlaceIcon);

        allFaceIcon.add(allPlayers[0].icon); allFaceIcon.add(allPlayers[1].icon);
        allFaceIcon.add(allPlayers[2].icon);

        // Игрок1
        iconFace1.setImageResource(allPlayers[0].icon);
        iconName1.setText(allPlayers[0].name);
        iconScore1.setText(String.valueOf(allPlayers[0].score)+" очков");

        // Проверка на то, занял ли игрок первое место
        if(allPlayers[0].type.equals("player")){
            taskCheck.checkTask(4);
        }

        // Игрок2
        iconFace2.setImageResource(allPlayers[1].icon);
        iconName2.setText(allPlayers[1].name);
        iconScore2.setText(String.valueOf(allPlayers[1].score)+" очков");

        // Игрок3
        iconFace3.setImageResource(allPlayers[2].icon);
        iconName3.setText(allPlayers[2].name);
        iconScore3.setText(String.valueOf(allPlayers[2].score)+" очков");

        // Игрок4
        iconFace4.setImageResource(allPlayers[3].icon);
        iconName4.setText(allPlayers[3].name);
        iconScore4.setText(String.valueOf(allPlayers[3].score)+" очков");

    }

    // Отправляем на сервер очки текущего пользователя
    public void sendPointsToBD(){

        // Если обычный уровень, то очки не добавляем
        if(getIntent().getStringExtra("gameType").equals("common")){
            trophyCountText1.setVisibility(View.GONE);
            trophyCountText2.setVisibility(View.GONE);
            trophyCountText3.setVisibility(View.GONE);
            trophyIcon1.setVisibility(View.GONE);
            trophyIcon2.setVisibility(View.GONE);
            trophyIcon3.setVisibility(View.GONE);
            return;
        }

        // Перебор по всем пользователям
        for (int i=0;i<allPlayers.length;i++){
            if(allPlayers[i].type.equals("player")){

                // Узнаём кол-во очков
                switch (i){
                    case 0: playerPoints=5;winScore=1;break;
                    case 1: playerPoints=3; break;
                    case 2: playerPoints=1; break;
                    default: playerPoints=0; break;
                }

                // Отправляем кол-во очков в аналитику
                metricaEvent("Очки: "+String.valueOf(playerPoints),1);
            }
        }

        // Отправляем на сервер очки игрока
        Call<AuthObject> sendUserScore=postUserScore.sendUserScore(token,playerPoints);
        retrofitMainClass.createRequest(sendUserScore);

        retrofitMainClass.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object response) {
                Response<AuthObject> currentResponse=(Response<AuthObject>)response;

                if(currentResponse.code()==200){
                    createToast("Ваш рейтинг успешно обновлён");
                }
            }

            @Override
            public void onEventFailure(Throwable error) {

            }
        });

    }

    // AlertDialog для получения дополнительного рейтингового уровня
    // Прописываем появление AlertDialog
    public void showAlertRates(){
        final Dialog openDialog = new Dialog(this);
        openDialog.setContentView(R.layout.custom_alertdialog_rategames);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        openDialog.setCancelable(false);
        openDialog.show();

        // Получение жизни и показ рекламы
        ImageView btnGetLives = (ImageView) openDialog.findViewById(R.id.btn_get_games);
        btnGetLives.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Показываем рекламный ролик для получения жизни
                if(mAd.isLoaded()) {
                    mAd.show();
                    openDialog.dismiss();
                }else{
                    createToast("В данный момент нет видео для просмотра");
                }
            }
        });

        // Закрываем диалог
        Button btnCloseDialog = (Button) openDialog.findViewById(R.id.btn_alert_close);
        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.dismiss();
            }
        });
    }

    // Создание тоастов
    public void createToast(String text){
        Toast.makeText(this, text,
                Toast.LENGTH_SHORT).show();
    }
    // Статистика Конец турнира
    public void metricaEvent(String tournamentType, int eventType){
        Map<String, Object> eventAttributes = new HashMap<String, Object>();

        if(eventType==0) {
            eventAttributes.put("Tournament", tournamentType);
        }else{
            eventAttributes.put("Points", tournamentType);
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
