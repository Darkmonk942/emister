package com.rocketraven.emister;

import android.app.Application;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.google.android.gms.ads.MobileAds;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

import java.util.Locale;

import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Darkmonk on 05.05.2016.
 */

public class EmisterApp extends Application {
    public static Typeface robotoLight,robotoCondensed;
    public static TextToSpeech tts;

    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
                Log.d("vkToken","invalid");
            }else{
                Log.d("vkToken","valid");
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/roboto_light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        // Инициализация Paper (NoSQL bd)
        Paper.init(this);

        // Получаем параметры экрана
        getScreenParams();

        // Инициализация Вк сдк
        vkAccessTokenTracker.startTracking();
        VKSdk.initialize(this);

        // Инициализация AdMob
        MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.admob_key));

        // Назначаем шрифты
        robotoLight=Typeface.createFromAsset(getAssets(), "fonts/roboto_light.ttf");
        robotoCondensed=Typeface.createFromAsset(getAssets(), "fonts/roboto_condensed.ttf");

        // Подключение TTS
        TtsInit();

        // Инициализация Facebook sdk
        FacebookSdk.sdkInitialize(getApplicationContext(), new FacebookSdk.InitializeCallback() {
            @Override
            public void onInitialized() {
                if(AccessToken.getCurrentAccessToken() == null){
                    Log.d("фбТокен","Токена нет");
                } else {
                    Log.d("фбТокен",AccessToken.getCurrentAccessToken().getToken());
                }
            }
        });
    }

    // Подгрузка TTS
    public void TtsInit(){
        tts=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    tts.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {

                        @Override
                        public void onUtteranceCompleted(String utteranceId) {

                        }
                    });
                }
                if(status != TextToSpeech.ERROR) {
                    tts.setSpeechRate(0.80f);
                    try {
                        tts.setLanguage(Locale.ENGLISH);
                    }catch (Exception e){
                      try {
                          tts.setLanguage(Locale.ENGLISH);
                      }catch (Exception e1){

                      }
                    }
                }
            }
        });

    }

    // Получаем размеры экрана
    public void getScreenParams(){
        // Получаем размеры экрана
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display =wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height=size.y;
        int width=size.x;

        // Сохраняем параметры экрана
            Paper.book().write("height",height);
            Paper.book().write("width",width);
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
