package com.rocketraven.emister;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.liulishuo.magicprogresswidget.MagicProgressCircle;
import com.rocketraven.emister.objects.AllIcons;
import com.rocketraven.emister.objects.EndOfTournamentObject;
import com.rocketraven.emister.objects.PlayerObject;
import com.rocketraven.emister.objects.TournamentRound;
import com.rocketraven.emister.objects.UserMainObject;
import com.rocketraven.emister.optimization.Optimization;
import com.rocketraven.emister.trains.CharsCreator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Darkmonk on 15.06.2016.
 */
public class TournamentActivity extends AppCompatActivity implements View.OnClickListener {
    // ProgressCircle
    @BindView(R.id.progress_circle) MagicProgressCircle progressCircle;
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.player_frame) FrameLayout playerFrame;
    @BindView(R.id.words_frame) FrameLayout wordsFrame;
    @BindView(R.id.bot1_frame) FrameLayout bot1Frame;
    @BindView(R.id.bot2_frame) FrameLayout bot2Frame;
    @BindView(R.id.bot3_frame) FrameLayout bot3Frame;
    // LinearLayout
    @BindView(R.id.bottom_linear) LinearLayout bottomLinear;
    @BindView(R.id.points_linear) LinearLayout pointsLinear;
    // TextView
    @BindView(R.id.answer_field) TextView answerField;
    @BindView(R.id.round_text_type) TextView roundTextType;
    @BindView(R.id.timer_text) TextView timerText;
    @BindView(R.id.question_text) TextView questionText;
    @BindView(R.id.round_text) TextView roundText;
    @BindView(R.id.round_text_top) TextView roundTextTop;
    @BindView(R.id.player_points_text) TextView playerPoints;
    @BindView(R.id.bot1_points_text) TextView bot1Points;
    @BindView(R.id.bot2_points_text) TextView bot2Points;
    @BindView(R.id.bot3_points_text) TextView bot3Points;
    @BindView(R.id.player_nickname_text) TextView playerName;
    @BindView(R.id.bot1_text) TextView bot1Name;
    @BindView(R.id.bot2_text) TextView bot2Name;
    @BindView(R.id.bot3_text) TextView bot3Name;
    // ImageView
    @BindView(R.id.btn_delete) ImageView btnDelete;
    @BindView(R.id.player_icon) ImageView playerIcon;
    @BindView(R.id.bot1_icon) ImageView bot1Icon;
    @BindView(R.id.bot2_icon) ImageView bot2Icon;
    @BindView(R.id.bot3_icon) ImageView bot3Icon;
    @BindView(R.id.answers_done_image1) ImageView answerDoneImage1;
    @BindView(R.id.answers_done_image2) ImageView answerDoneImage2;
    @BindView(R.id.answers_done_image3) ImageView answerDoneImage3;
    @BindView(R.id.answers_done_image4) ImageView answerDoneImage4;
    @BindView(R.id.true_answer1) ImageView trueAnswer1;
    @BindView(R.id.true_answer2) ImageView trueAnswer2;
    @BindView(R.id.true_answer3) ImageView trueAnswer3;
    @BindView(R.id.true_answer4) ImageView trueAnswer4;
    // Button
    @BindView(R.id.answer1) Button answerBut1;
    @BindView(R.id.answer2) Button answerBut2;
    @BindView(R.id.answer3) Button answerBut3;
    @BindView(R.id.answer4) Button answerBut4;
    Button pressedButton=null;
    // Примитивы
    String aboutTaskList[]={"Перевод слова","Перевод слова","Собираем перевод","Неправильный глагол",
                            "Перевод слова","Собираем перевод","Выбрать перевод",""};
    int FUll_ROUND_TIME=21;
    int answersCount=0;
    int lastAnswer=0;
    Runnable postRound;
    AtomicBoolean stopRunnable;
    Random rand;
    MediaPlayer backgroundPlayer=null;
    MediaPlayer answerPlayer;
    MediaPlayer endSound;
    Timer myTimer;
    TimerTask timerTask;
    Handler handler=new Handler();
    Integer allIconScope[];
    ArrayList<Button> allAnswersBut=new ArrayList();
    ArrayList<ImageView> allGreenCircles=new ArrayList();
    ArrayList<ImageView> allTrueAnswers=new ArrayList();
    ArrayList<TournamentRound> tasksList=new ArrayList();
    ArrayList<PlayerObject> allPlayersObject=new ArrayList();
    int answersTimeMas[]={1,1,1};
    boolean answerCorrect[]={false,false,false};
    int currentRound=0;
    int roundType=1;
    String taskCurrentQuestion;
    String taskCurrentAnswer;
    String taskCurrentAnswers[];
    String playerNameSaved;
    String bot1NameSaved;
    String bot2NameSaved;
    String bot3NameSaved;
    int timeCount=1;
    int playerScore=0;
    int bot1Score=0;
    int bot2Score=0;
    int bot3Score=0;
    boolean soundOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament);
        ButterKnife.bind(this);

        stopRunnable=new AtomicBoolean(false);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // Получаем все иконки аватарок
        AllIcons allIcons=new AllIcons();
        allIconScope=allIcons.getIcons();

        // Создаём postRunnable
        createNewRunnable();

        // Узнаём включена ли музыка
        soundOn=Paper.book("settings").read("sound",true);

        // Получаем текущего пользователя
        UserMainObject userMainObject=Paper.book("user").read("mainInfo",new UserMainObject());

        playerNameSaved=userMainObject.userName;
        playerIcon.setImageResource(allIconScope[userMainObject.userPic]);

        // Плеер для звуков ответов
        answerPlayer  = MediaPlayer.create(this, R.raw.tournament_answer);

        // Звук окончания ответа
        endSound = MediaPlayer.create(this, R.raw.sound_end_lvl);

        // Фоновая музыка
        backgroundPlayer  = MediaPlayer.create(this, R.raw.tournament_theme);

        // Получаем список заданий текущего раунда
        tasksList= Paper.book().read("taskList",new ArrayList<TournamentRound>());

        // Получаем список игроков
        allPlayersObject=Paper.book().read("players_scope");

        // Меняем имена и иконки игроков
        changePlayersUI();

        // Оптимизация интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(mainFrame);
        optimization.Optimization(playerFrame);
        optimization.Optimization(bot1Frame);
        optimization.Optimization(bot2Frame);
        optimization.Optimization(bot3Frame);
        optimization.OptimizationLinear(pointsLinear);

        // Меняем толщину круга
        int height=Paper.book().read("height",1920);
        progressCircle.setStrokeWidth(height/60);

        // Заполняем листы
        fillAllLists();

        // Объявлеие таймера и рандом
        rand=new Random();

        // Начало первого раунда
        roundStart();
    }

    // Меняем имена и иконки игроков
    public void changePlayersUI(){
        bot1NameSaved=allPlayersObject.get(0).name;
        bot1Icon.setImageResource(allIconScope[allPlayersObject.get(0).iconId]);

        bot2NameSaved=allPlayersObject.get(1).name;
        bot2Icon.setImageResource(allIconScope[allPlayersObject.get(1).iconId]);

        bot3NameSaved=allPlayersObject.get(2).name;
        bot3Icon.setImageResource(allIconScope[allPlayersObject.get(2).iconId]);

        // Проверяем имена на длинну и, если надо, обрезаем
        if(playerNameSaved.length()>9){
            playerNameSaved=playerNameSaved.substring(0,8)+"...";
        }
        if(bot1NameSaved.length()>9){
            bot1NameSaved=bot1NameSaved.substring(0,8)+"...";
        }
        if(bot2NameSaved.length()>9){
            bot2NameSaved=bot2NameSaved.substring(0,8)+"...";
        }
        if(bot3NameSaved.length()>9){
            bot3NameSaved=bot3NameSaved.substring(0,8)+"...";
        }

        // Подписываем всех участников
        playerName.setText(playerNameSaved);
        bot1Name.setText(bot1NameSaved);
        bot2Name.setText(bot2NameSaved);
        bot3Name.setText(bot3NameSaved);
    }

    // Создаём поток для Handler после раундов
    public void createNewRunnable(){
        postRound= new Runnable() {
            public void run() {

                if(roundType==1) {
                    if(pressedButton==null){
                        for (int i = 0; i < allAnswersBut.size(); i++) {
                            if (allAnswersBut.get(i).getText().toString().toLowerCase()
                                    .equals(questionText.getTag().toString().toLowerCase())) {
                                allAnswersBut.get(i).setBackgroundResource(R.drawable.tournament_but_green);
                            }
                        }
                        Log.d("Ответики","Ответик1");
                        checkAnswerPoints(0);
                        return;
                    }
                    if (pressedButton.getText().toString().toLowerCase().equals(questionText.getTag().toString().toLowerCase())) {
                        checkAnswerPoints(1);
                    } else {
                        pressedButton.setBackgroundResource(R.drawable.tournament_but_red);
                        for (int i = 0; i < allAnswersBut.size(); i++) {
                            if (allAnswersBut.get(i).getText().toString().toLowerCase()
                                    .equals(questionText.getTag().toString().toLowerCase())) {
                                allAnswersBut.get(i).setBackgroundResource(R.drawable.tournament_but_green);
                            }
                        }
                        Log.d("Ответики","Ответик2");
                        checkAnswerPoints(0);
                    }
                }else{
                        checkAnswerPoints(1);
                }
            }
        };
    }

    // Набор эвентов
    public void roundStart(){

        // Проверка интернет соединения
        if(!checkInternetConnection()){
            createToast("Отсутствует интернет соединение");
            finish();
        }

        if(currentRound+1==8){
            createEndOfTournamentObject();

            startActivity(new Intent(this,EndOfTournamentActivity.class)
                    .putExtra("gameType",getIntent().getStringExtra("gameType")));
            finish();
            return;
        }
        getCurrentTask();
        createAnswersTime();

        roundText.setText("Раунд "+String.valueOf(currentRound+1));
        roundTextTop.setText("Раунд "+String.valueOf(currentRound+1));

        // Анимация появления номера текущего раунда
        AnimatorSet fadeAndUnfade = new AnimatorSet();
        ValueAnimator unfadeAnim = ObjectAnimator.ofFloat(roundText, "alpha", 0f, 1f).setDuration(2000);
        ValueAnimator fadeAnim = ObjectAnimator.ofFloat(roundText, "alpha", 1f, 0f).setDuration(1000);
        fadeAndUnfade.play(fadeAnim).after(unfadeAnim);
        fadeAndUnfade.start();

        // Появление вопроса и вариантов ответа
        fadeAnim.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {

                // Запускаем первый раунд
                nextRound();
            }
        });
    }

    // Обработка правильности ответа
    public void answerClick(View v){
        lastAnswer=Integer.valueOf(timerText.getText().toString());
        pressedButton=(Button)v;
        pressedButton.setBackgroundResource(R.drawable.tournament_but_green);
        answerSoundStart();
        answersCount=answersCount+1;
        allGreenCircles.get(0).setVisibility(View.VISIBLE);

        // Убираем клики с кнопок
        setClickable(false);
        if(answersCount==4){
            // Останавливаем таймер
            endSoundStart();
            answersCount=0;
            myTimer.cancel();
            timeCount=1;
            backgroundPlayer.pause();
            backgroundPlayer.seekTo(0);
            progressCircle.setPercent(1.0f);

            // Каунтер раундов
            currentRound=currentRound+1;

            // Проверяем правильный ответ
            // Поток для Handler после раунда
            //createNewRunnable();
            handler.postDelayed(postRound, 1500);
        }
    }

    // Создаём кольцо для таймера
    public void nextRound(){
        pressedButton=null;
        // Возвращаем видимость ответам и вопросу
        setAnswers();

        // Определяем правильность ответа и время ответа


        handler.postDelayed(new Runnable() {
            public void run() {
        // Запускаем таймер
        timerStart();
            }
        }, 1000);
    }

    // Запуск таймера
    public void timerStart(){

        // Проверка на выход с текущего экрана назад
        if(stopRunnable.toString().equals("true")){
            return;
        }

        // Обнуляем прогресс!
        progressCircle.setPercent(0.0f);
        progressCircle.setSmoothPercent(1.0f,20000);

        // Фоновая музыка

        if (soundOn) {
            backgroundPlayer.start();
        }

        myTimer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        Log.d("Время", String.valueOf(timeCount));
                        if(timeCount==22){
                            // Правильный ответ не получен
                            myTimer.cancel();
                            timeCount=1;
                            answersCount=0;
                            if(roundType==1){
                                for (int i = 0; i < allAnswersBut.size(); i++) {
                                    if (allAnswersBut.get(i).getText().toString().toLowerCase()
                                            .equals(questionText.getTag().toString().toLowerCase())) {
                                        allAnswersBut.get(i).setBackgroundResource(R.drawable.tournament_but_green);
                                    }
                                }

                                    Log.d("Ответики", "Ответик3");
                                    checkAnswerPoints(0);

                            }else{
                                if(!answerField.getText().toString().equals(taskCurrentAnswer)){
                                answerField.setText(taskCurrentAnswer);
                                    Log.d("Ответики","Ответик4");
                                checkAnswerPoints(0);
                                }else{
                                checkAnswerPoints(1);
                                }
                            }

                            // Показываем правильный ответ в зависимости от заданий
                            progressCircle.setPercent(1.0f);
                            // Убираем клики с кнопок
                            setClickable(false);
                            // Каунтер раундов
                            currentRound=currentRound+1;

                        }else{
                            // Проверяем время ответа других игроков
                            if(answersTimeMas[0]==timeCount){
                                Log.d("Ответ1", String.valueOf(answersTimeMas[0]));
                                allGreenCircles.get(1).setVisibility(View.VISIBLE);
                                answersCount=answersCount+1;
                                answerSoundStart();}
                            if(answersTimeMas[1]==timeCount){
                                Log.d("Ответ2", String.valueOf(answersTimeMas[1]));
                                allGreenCircles.get(2).setVisibility(View.VISIBLE);
                                answersCount=answersCount+1;
                                answerSoundStart();}
                            if(answersTimeMas[2]==timeCount){
                                Log.d("Ответ3", String.valueOf(answersTimeMas[2]));
                                allGreenCircles.get(3).setVisibility(View.VISIBLE);
                                answersCount=answersCount+1;
                                answerSoundStart();}

                            if(answersCount==4){
                                // Останавливаем таймер
                                endSoundStart();
                                answersCount=0;
                                myTimer.cancel();
                                timeCount=1;
                                backgroundPlayer.pause();
                                backgroundPlayer.seekTo(0);
                                progressCircle.setPercent(1.0f);

                                // Убираем клики с кнопок
                                setClickable(false);

                                // Каунтер раундов
                                currentRound=currentRound+1;

                                // Проверяем правильный ответ
                                // Поток для Handler после раунда
                                //createNewRunnable();
                                handler.postDelayed(postRound, 1500);
                            }

                            timerText.setText(String.valueOf(FUll_ROUND_TIME-timeCount));
                            timeCount=timeCount+1;}

                    }
                });
            }};

        myTimer.schedule(timerTask, 0, 1000);
    }

    // Анимация появления очков за правильный ответ
    public void checkAnswerPoints(int points){

        // Обнуляем нажатие на кнопку ответа
        pressedButton=null;

        // Получаем очки игрока
        trueAnswer1.setVisibility(View.VISIBLE);
        Log.d("очёчки", String.valueOf(points));
        if(points==0){
            trueAnswer1.setImageResource(R.drawable.tournament_false_answer);
        }else{
            trueAnswer1.setImageResource(R.drawable.tournament_true_answer_icon);
        }
        final int answerPoints=(100 + Integer.valueOf(lastAnswer)*2)*points;
        playerPoints.setText(" + "+String.valueOf(answerPoints));
        playerScore=playerScore+answerPoints;

        // Рандомим очки для ботов
        int bot1NewPoints=0;
        trueAnswer2.setVisibility(View.VISIBLE);
        if(answerCorrect[0]) {
            trueAnswer2.setImageResource(R.drawable.tournament_true_answer_icon);
            bot1NewPoints = (21-answersTimeMas[0]) * 2 + 100;
        }else{
            trueAnswer2.setImageResource(R.drawable.tournament_false_answer);
        }
        bot1Points.setText(" + "+String.valueOf(bot1NewPoints));
        bot1Score=bot1Score+bot1NewPoints;

        int bot2NewPoints=0;
        trueAnswer3.setVisibility(View.VISIBLE);
        if(answerCorrect[1]) {
            trueAnswer3.setImageResource(R.drawable.tournament_true_answer_icon);
            bot2NewPoints = (21-answersTimeMas[1]) * 2 + 100;
        }else{
            trueAnswer3.setImageResource(R.drawable.tournament_false_answer);
        }
        bot2Points.setText(" + "+String.valueOf(bot2NewPoints));
        bot2Score=bot2Score+bot2NewPoints;

        int bot3NewPoints=0;
        trueAnswer4.setVisibility(View.VISIBLE);
        if(answerCorrect[2]) {
            trueAnswer4.setImageResource(R.drawable.tournament_true_answer_icon);
            bot3NewPoints = (21-answersTimeMas[2]) * 2 + 100;
        }else{
            trueAnswer4.setImageResource(R.drawable.tournament_false_answer);
        }
        bot3Points.setText(" + "+String.valueOf(bot3NewPoints));
        bot3Score=bot3Score+bot3NewPoints;

        handler.postDelayed(new Runnable() {
            public void run() {
                timerText.setText("20");
                questionText.setText("");
                setVisibility(View.GONE);
                setClickable(true);
                roundStart();

                // Добавляем очки в общую копилку
                playerPoints.setText(String.valueOf(playerScore)+"\nочков");
                bot1Points.setText(String.valueOf(bot1Score)+"\nочков");
                bot2Points.setText(String.valueOf(bot2Score)+"\nочков");
                bot3Points.setText(String.valueOf(bot3Score)+"\nочков");
            }
        }, 5000);

    }

    // Заполняем поля вопроса и ответов
    public void setAnswers(){

        questionText.setText(taskCurrentQuestion);
        questionText.setTag(taskCurrentAnswer);
        if(roundType==1) {

            // Набираем варианты ответов
            HashSet<String> allAnswers = new HashSet();
            allAnswers.add(taskCurrentAnswer);

            while (allAnswers.size() != 4) {
                allAnswers.add(taskCurrentAnswers[rand.nextInt(taskCurrentAnswers.length)]);
            }

            // Мешаем кнопки
            Collections.shuffle(allAnswersBut);

            // Добавляем на кнопки варианты ответов
            Iterator iteratorAnswers = allAnswers.iterator();
            Iterator iteratorButton = allAnswersBut.iterator();

            // Заполняем кнопки вариантами ответов
            while (iteratorAnswers.hasNext()) {
                Button but = (Button) iteratorButton.next();
                but.setText(iteratorAnswers.next().toString());
            }


            // Возвращаем видимость
            questionText.setVisibility(View.VISIBLE);
            setBackground();
            setVisibility(View.VISIBLE);
        }else{

            // Возвращаем видимость объектам для составления слов
            wordsFrame.removeAllViews();
            wordsFrame.setVisibility(View.VISIBLE);
            answerField.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.VISIBLE);
            answerField.setText("");

            // Заполняем фрейм с буквами
            createLetters();
        }
    }

    // Удаление символов с поля сбора слов и предложений
    public void deleteSymbolClick(View v){
        // Удаляем символ с поля ответов
        if(!answerField.getText().toString().equals("")){
            String oldText=answerField.getText().toString();
            String lastSymbol=oldText.substring(oldText.length()-1);
            oldText=oldText.substring(0,oldText.length()-1);
            answerField.setText(oldText);

            // Добавляем символ назад в поле с кнопками
            for (int i=0;i<wordsFrame.getChildCount();i++){
                Button but=(Button)wordsFrame.getChildAt(i);
                if(but.getVisibility()==View.GONE &&
                        but.getText().toString().toLowerCase().equals(lastSymbol.toLowerCase())){
                    but.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    }

    // Узнаём текущее задание
    public void getCurrentTask(){
        roundTextType.setText(aboutTaskList[currentRound]);
        switch (currentRound){
            case 0: // угадываем слово
                roundType=1;
                taskCurrentQuestion=tasksList.get(currentRound).roundQuestion;
                taskCurrentAnswer=tasksList.get(currentRound).roundAnswer;
                taskCurrentAnswers=tasksList.get(currentRound).roundAnswers;
                Log.d("Правильный ответ",taskCurrentAnswer);
                break;
            case 1: // угадываем слово
                roundType=1;
                taskCurrentQuestion=tasksList.get(currentRound).roundQuestion;
                taskCurrentAnswer=tasksList.get(currentRound).roundAnswer;
                taskCurrentAnswers=tasksList.get(currentRound).roundAnswers;
                Log.d("Правильный ответ",taskCurrentAnswer);
                break;
            case 2: // Собираем лёгкое слово
                roundType=2;
                taskCurrentQuestion=tasksList.get(currentRound).roundQuestion;
                taskCurrentAnswer=tasksList.get(currentRound).roundAnswer;
                Log.d("Правильный ответ",taskCurrentAnswer);
                break;
            case 3: // Неправильный глагол
                roundType=1;
                taskCurrentQuestion=tasksList.get(currentRound).roundQuestion;
                taskCurrentAnswer=tasksList.get(currentRound).roundAnswer;
                taskCurrentAnswers=tasksList.get(currentRound).roundAnswers;
                Log.d("Правильный ответ",taskCurrentAnswer);
                break;
            case 4: // угадываем слово
                roundType=1;
                taskCurrentQuestion=tasksList.get(currentRound).roundQuestion;
                taskCurrentAnswer=tasksList.get(currentRound).roundAnswer;
                taskCurrentAnswers=tasksList.get(currentRound).roundAnswers;
                Log.d("Правильный ответ",taskCurrentAnswer);
                break;
            case 5: // Собираем сложное слово
                roundType=2;
                taskCurrentQuestion=tasksList.get(currentRound).roundQuestion;
                taskCurrentAnswer=tasksList.get(currentRound).roundAnswer;
                break;
            case 6: // Вставить в предложение правильный вариант
                roundType=1;
                taskCurrentQuestion=tasksList.get(currentRound).roundQuestion;
                taskCurrentAnswer=tasksList.get(currentRound).roundAnswer;
                taskCurrentAnswers=tasksList.get(currentRound).roundAnswers;
                break;
            case 7: // Собрать предложение из слов
                taskCurrentQuestion=tasksList.get(currentRound).roundQuestion;
                taskCurrentAnswer=tasksList.get(currentRound).roundAnswer;
                taskCurrentAnswers=tasksList.get(currentRound).roundAnswers;
                break;
        }


    }

    // Создаём набор букв для составления слов
    public void createLetters(){
        int height= Paper.book().read("height");
        int width= Paper.book().read("width");
        int tenPx= height/192;

        // Разбиваем правильный ответ на буквы
        char[] lettersMas=taskCurrentAnswer.toCharArray();

        // Определяем кол-во букв для сбора слова
        char[] allLettersMas=new char[15];

        if(lettersMas.length<15){
            char[] additionalLetters= CharsCreator.charsCreator(15-lettersMas.length);
            System.arraycopy(lettersMas, 0, allLettersMas, 0, lettersMas.length);
            System.arraycopy(additionalLetters, 0, allLettersMas, lettersMas.length, additionalLetters.length);
        }

        // Получаем размеры для кнопок-букв
        int letterWidth=tenPx*11;
        int letterMargin=(width-letterWidth*6)/7;
        int raw=0;
        int coloumn=0;

        // Перемешиваем Массив готовый массив букв
        for (int i=0;i<allLettersMas.length;i++){
            char shuffle;
            int randNumber=rand.nextInt(allLettersMas.length);
            shuffle=allLettersMas[i];
            allLettersMas[i]=allLettersMas[randNumber];
            allLettersMas[randNumber]=shuffle;
        }

        for (int i=0;i<allLettersMas.length;i++) {
            Button but = new Button(this);
            FrameLayout.LayoutParams butParams = new FrameLayout.LayoutParams(letterWidth, letterWidth);
            if(i==6||i==12||i==18){
                raw=0;
                coloumn=coloumn+1;
            }
            butParams.setMargins(letterMargin+raw*(letterMargin+letterWidth),coloumn*(letterMargin+letterWidth),0,0);
            but.setOnClickListener(this);
            but.setText(String.valueOf(allLettersMas[i]));
            but.setTextColor(Color.parseColor("#ffffff"));
            but.setTextSize(TypedValue.COMPLEX_UNIT_PX,tenPx*5);
            but.setBackgroundResource(R.drawable.shape_round_button);
            but.setIncludeFontPadding(false);
            but.setPadding(0,0,0,0);
            but.setAllCaps(false);
            raw=raw+1;
            wordsFrame.addView(but, butParams);
        }
    }

    // Добавляем символ с фрейма букв на view ответов
    @Override
    public void onClick(View v) {
        Button but = (Button) v;
        but.setVisibility(View.GONE);
        answerField.setText(answerField.getText().toString() + but.getText().toString());

        if (answerField.getText().toString().equals(taskCurrentAnswer)) {

            allGreenCircles.get(0).setVisibility(View.VISIBLE);
            // Убираем клики с кнопок
            for (int i=0;i<wordsFrame.getChildCount();i++){
                wordsFrame.getChildAt(i).setClickable(false);
            }

            lastAnswer=Integer.valueOf(timerText.getText().toString());
            answersCount=answersCount+1;
            if(answersCount==4){
                // Останавливаем таймер
                answersCount=0;
                myTimer.cancel();
                timeCount=1;
                backgroundPlayer.pause();
                backgroundPlayer.seekTo(0);
                progressCircle.setPercent(1.0f);

                // Убираем клики с кнопок
                setClickable(false);

                // Каунтер раундов
                currentRound=currentRound+1;

                // Проверяем правильный ответ
                // Поток для Handler после раунда
                //createNewRunnable();
                handler.postDelayed(postRound, 1500);
            }
        } else {

        }
    }

    // Заполняем объект для передачи в экран окончания турнира
    public void createEndOfTournamentObject(){
        EndOfTournamentObject playerEndObject=new EndOfTournamentObject();
        UserMainObject userMainObject=Paper.book("user").read("mainInfo",new UserMainObject());
        playerEndObject.icon=allIconScope[userMainObject.userPic];
        playerEndObject.score= playerScore;
        playerEndObject.name=playerNameSaved;
        playerEndObject.type="player";

        EndOfTournamentObject bot1Object=new EndOfTournamentObject();
        bot1Object.icon=allIconScope[allPlayersObject.get(0).iconId];
        bot1Object.score= bot1Score;
        bot1Object.name=bot1NameSaved;
        bot1Object.type="bot";

        EndOfTournamentObject bot2Object=new EndOfTournamentObject();
        bot2Object.icon=allIconScope[allPlayersObject.get(1).iconId];
        bot2Object.score= bot2Score;
        bot2Object.name=bot2NameSaved;
        bot2Object.type="bot";

        EndOfTournamentObject bot3Object=new EndOfTournamentObject();
        bot3Object.icon=allIconScope[allPlayersObject.get(2).iconId];
        bot3Object.score= bot3Score;
        bot3Object.name=bot3NameSaved;
        bot3Object.type="bot";

        // Заполняем массив объектами игроков
        EndOfTournamentObject allPlayers[]=new EndOfTournamentObject[4];
        allPlayers[0]=playerEndObject;
        allPlayers[1]=bot1Object;
        allPlayers[2]=bot2Object;
        allPlayers[3]=bot3Object;

        // Сортируем по кол-ву очков
        for (int i=0;i<allPlayers.length-1;i++){
            for (int j=i+1;j<allPlayers.length;j++){
                if(allPlayers[i].score<allPlayers[j].score){
                    EndOfTournamentObject sortVar=allPlayers[i];
                    allPlayers[i]=allPlayers[j];
                    allPlayers[j]=sortVar;
                }
            }
        }

        // Сохраняем достижения всех игроков
        Paper.book().write("tournamentEnd",allPlayers);
    }

    // Кликабельность кнопок ответов
    public void setClickable(boolean clickable){
        for (int i=0;i<allAnswersBut.size();i++){
            allAnswersBut.get(i).setClickable(clickable);
        }
    }

    // Видимость кнопок ответов
    public void setVisibility(int visibility){
        for (int i=0;i<allAnswersBut.size();i++){
            allAnswersBut.get(i).setVisibility(visibility);
        }
        wordsFrame.setVisibility(View.GONE);
        answerField.setVisibility(View.GONE);
        btnDelete.setVisibility(View.GONE);
        for (int i=0;i<allGreenCircles.size();i++){
            allGreenCircles.get(i).setVisibility(View.GONE);
        }
        for (int i=0;i<allTrueAnswers.size();i++){
            allTrueAnswers.get(i).setVisibility(View.GONE);
        }
    }

    // Заполняем листы
    public void fillAllLists(){
        allAnswersBut.add(answerBut1); allAnswersBut.add(answerBut2);
        allAnswersBut.add(answerBut3); allAnswersBut.add(answerBut4);

        allGreenCircles.add(answerDoneImage1); allGreenCircles.add(answerDoneImage2);
        allGreenCircles.add(answerDoneImage3); allGreenCircles.add(answerDoneImage4);

        allTrueAnswers.add(trueAnswer1); allTrueAnswers.add(trueAnswer2);
        allTrueAnswers.add(trueAnswer3); allTrueAnswers.add(trueAnswer4);

    }

    // Возвращаем всем кнопкам стандартный цвет
    public void setBackground(){
        for (int i=0;i<allAnswersBut.size();i++){
            allAnswersBut.get(i).setBackgroundResource(R.drawable.tournament_but_idle);
        }
    }

    // Подбор времени ответа  и его правильности
    public void createAnswersTime(){

        // Определяем время ответа
        for (int i=0;i<3;i++){
            int timeLimit=rand.nextInt(100);
            Log.d("Раунд", String.valueOf(roundType));
            if(roundType==1) {  // Для выбора ответа
                // 50% - от 12 до 19
                if (timeLimit < 50) {
                    answersTimeMas[i] = rand.nextInt(7) + 10;
                }
                // 35% - от 5 до 12
                if (timeLimit >= 50 && timeLimit <= 80) {
                    answersTimeMas[i] = rand.nextInt(7) + 5;
                }
                // 15% - от 2 до 9
                if (timeLimit > 80 && timeLimit <= 90) {
                    answersTimeMas[i] = rand.nextInt(9) + 2;
                }
                // 5% - от 1 до 2
                if (timeLimit > 90) {
                    answersTimeMas[i] = rand.nextInt(2) + 2;
                }
            }else{ // Для сбора слова
                // Определяем сложность задания
                int taskHard=8;

                // 50% - от 12 до 19
                if (timeLimit < 50) {
                    answersTimeMas[i] = rand.nextInt(9) + taskHard;
                }
                // 40% - от 8 до 12
                if (timeLimit >= 50 && timeLimit <= 90) {
                    answersTimeMas[i] = rand.nextInt(5) + taskHard;
                }
                // 10% - от 6 до 9
                if (timeLimit > 90) {
                    answersTimeMas[i] = rand.nextInt(2) + taskHard;
                }
            }
        }

        // Определяем правильность ответа
        for (int i=0;i<3;i++){
            switch (currentRound){
                case 0: if(rand.nextInt(100)<96){answerCorrect[i]=true;
                }else{answerCorrect[i]=false;}
                    break;
                case 1: if(rand.nextInt(100)<95){answerCorrect[i]=true;
                }else{answerCorrect[i]=false;}
                    break;
                case 2: if(rand.nextInt(100)<85){answerCorrect[i]=true;
                }else{answerCorrect[i]=false;answersTimeMas[i]=-1;}
                    break;
                case 3: if(rand.nextInt(100)<70){answerCorrect[i]=true;
                }else{answerCorrect[i]=false;}
                    break;
                case 4: if(rand.nextInt(100)<80){answerCorrect[i]=true;
                }else{
                    answerCorrect[i]=false; }
                    break;
                case 5: if(rand.nextInt(100)<75){answerCorrect[i]=true;
                }else{
                    answerCorrect[i]=false;answersTimeMas[i]=-1;}
                    break;
                case 6: if(rand.nextInt(100)<60){answerCorrect[i]=true;
                }else{answerCorrect[i]=false;}
                    break;
            }
        }
    }

    // Реализация подсказок 50-50
    public void hint50Click(View v){
        int hintCount=0;
        if(Boolean.valueOf(v.getTag().toString())){
            if(roundType==1){
                v.setBackgroundResource(R.drawable.hint50_but_icon_on);
                v.setTag(false);
                for (int i=0;i<allAnswersBut.size();i++){

                    if(hintCount!=2){
                        if(!allAnswersBut.get(i).getText().equals(questionText.getTag().toString())){
                            hintCount=hintCount+1;
                            allAnswersBut.get(i).setVisibility(View.GONE);
                        }
                    }
                }

            }else{
                createToast("Данная подсказка недоступна для текущего задания");
            }
        }else{
            createToast("Подсказка уже была использована");
        }
    }

    // Реализация подсказки открыть первую букву
    public void hintHelpClick(View v){
        if(Boolean.valueOf(v.getTag().toString())){
            if(roundType==2) {
                v.setBackgroundResource(R.drawable.hint_first_letter_on);
                v.setTag(false);

                // Узнаём правильно ли прописан текст и нужна ли подсказка
                boolean checkWords = true;

                if (answerField.getText().toString().length() <= taskCurrentAnswer.length()) {
                    for (int i = 0; i < answerField.getText().toString().length(); i++) {
                        Log.d("Тестик", String.valueOf(answerField.getText().toString().charAt(i)).toLowerCase()+" "+
                                String.valueOf(taskCurrentAnswer.charAt(i)).toLowerCase());
                        if (!String.valueOf(answerField.getText().toString().charAt(i)).toLowerCase()
                                .equals(String.valueOf(taskCurrentAnswer.charAt(i)).toLowerCase())) {
                            checkWords = false;
                        }
                    }
                } else {
                    checkWords = false;
                }

                if (!checkWords) {
                    // Возвращаем видимость всех кнопок и обнуляем ответ
                    for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                        wordsFrame.getChildAt(i).setVisibility(View.VISIBLE);
                    }
                    answerField.setText("");
                }

                // Получаем нужную букву
                String trueAnswer = taskCurrentAnswer;
                String trueCharacter = String.valueOf(trueAnswer.charAt(answerField.getText().length()));

                for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                    Button but = (Button) wordsFrame.getChildAt(i);

                    if (but.getText().toString().toLowerCase().equals(trueCharacter.toLowerCase())) {
                        but.callOnClick();
                        return;
                    }
                }
            }else{
                createToast("Данная подсказка недоступна для текущего задания");
            }
        }else{
            createToast("Подсказка уже была использована");
        }
    }

    // Звук выбранного ответа
    public void answerSoundStart(){
        if(soundOn) {
            if (!answerPlayer.isPlaying()) {
                answerPlayer.start();
            }
        }
    }

    // Звук окончания ответа и турнира
    public void endSoundStart(){
        if(soundOn) {
            endSound.start();
        }
    }

    // Проверка наличия интернет соединения
    public boolean checkInternetConnection(){
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    // Конструктор Тоаст сообщений
    public void createToast(String text){
        Toast.makeText(this, text,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        confirmExitDialog();
        confirmExitDialog();
    }

    // Диалог о подтверждении о выходе с текущего уровня
    public void confirmExitDialog(){

        String alertMessage="Вы хотите покинуть турнир?";

        // Выдаём диалог об окончании уровня
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Внимание!")
                .setMessage(alertMessage)
                .setIcon(R.drawable.main_screen_logo)
                .setCancelable(false)
                .setPositiveButton("Да",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                if(backgroundPlayer!=null&&backgroundPlayer.isPlaying()){
                                    backgroundPlayer.stop();
                                }
                                stopRunnable.set(true);
                                handler.removeCallbacks(postRound);

                                if(myTimer!=null){
                                    myTimer.cancel();
                                    myTimer.purge();
                                }
                                finish();
                            }
                        })
                .setNegativeButton("Нет",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onPause(){
        if(backgroundPlayer!=null&&backgroundPlayer.isPlaying()){
            backgroundPlayer.pause();
            backgroundPlayer.seekTo(0);
        }

        super.onPause();
    }

    public void onDestroy(){
        if(backgroundPlayer!=null&&backgroundPlayer.isPlaying()){
            backgroundPlayer.stop();
        }
        stopRunnable.set(true);
        handler.removeCallbacks(postRound);

        if(myTimer!=null){
            myTimer.cancel();
            myTimer.purge();
        }

        super.onDestroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
