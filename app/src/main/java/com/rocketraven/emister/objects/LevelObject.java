package com.rocketraven.emister.objects;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Darkmonk on 12.05.2016.
 */
public class LevelObject {

    @SerializedName("levelName")
    @Expose
    public String levelName;

    @SerializedName("levelNumber")
    @Expose
    public int levelNumber;

    @SerializedName("levelType")
    @Expose
    public String levelType;

    @SerializedName("levelWordsRus")
    @Expose
    public ArrayList<String> levelWordsRus=new ArrayList();

    @SerializedName("levelWordsEng")
    @Expose
    public ArrayList<String> levelWordsEng=new ArrayList();

    @SerializedName("levelRule")
    @Expose
    public String levelRuleSplit;

    @SerializedName("levelRuleAfterSplit")
    @Expose
    public String[] levelRule;

    @SerializedName("levelRuleTrain")
    @Expose
    public JsonObject levelRuleTrainJson;

    @SerializedName("levelRuleTrainString")
    @Expose
    public String levelRuleTrain;

    @SerializedName("levelHints")
    @Expose
    public String levelWordsHintSplit;

    public HashMap<String,String> levelWordsHint=new HashMap();
}
