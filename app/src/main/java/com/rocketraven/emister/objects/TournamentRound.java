package com.rocketraven.emister.objects;

import java.util.ArrayList;

/**
 * Created by Darkmonk on 28.06.2016.
 */
public class TournamentRound {

    public int roundId;
    public String roundQuestion="";
    public String roundAnswer="";
    public String roundAnswers[];
    public ArrayList<String> roundWords=new ArrayList();
}
