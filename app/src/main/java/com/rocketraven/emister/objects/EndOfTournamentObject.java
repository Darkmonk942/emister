package com.rocketraven.emister.objects;

/**
 * Created by DarKMonK on 01.07.2016.
 */
public class EndOfTournamentObject {
    public String type;
    public int icon;
    public String name;
    public int score;
    public int ratePoints;

}
