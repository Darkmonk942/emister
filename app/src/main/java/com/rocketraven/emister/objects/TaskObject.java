package com.rocketraven.emister.objects;

import java.util.ArrayList;

/**
 * Created by Darkmonk on 09.05.2016.
 */
public class TaskObject {

    private int taskType; // тип заданий
    private String question; // вопрос (слово или предложение)
    private ArrayList<String> answersVariety=new ArrayList<>(); // Варианты ответов
    private String answer; // правильный ответ
    private ArrayList<String> engWordsMas=new ArrayList(); // Лист новых английских слов
    private ArrayList<String> rusWordsMas=new ArrayList(); // Лист новых русских слов
    private String hint; // Объяснение к словам
    private String ruleHeader; // Название правила
    private ArrayList<String> ruleText=new ArrayList<>(); // Лист объяснений правила
    private ArrayList<String> ruleExample=new ArrayList<>(); // Лист примеров объяснений

    private final String emptyString="Пусто";

    // Get/Set методы для получения переменных
    // Тип Задания
    public int getTaskType(){
        return taskType;
    }

    public void setTaskType(int taskType){
        if(taskType>0 && taskType<16){
            this.taskType=taskType;
        }else{
            this.taskType=0;
        }
    }

    // Вопрос (слово или предложение)
    public String getQuestion(){
        if(question!=null){
        return question;
        }
        return emptyString;
    }

    public void setQuestion(String question){
        if(question!=null){
            this.question=question;
        }
    }

    // Варианты Ответов
    public ArrayList<String> getAnswersVariety(){
            return answersVariety;
    }

    public void setAnswersVariety(ArrayList <String> answersVariety){
        this.answersVariety=answersVariety;
    }

    // Правильный ответ
    public String getAnswer(){
        if(answer!=null) {
            return answer;
        }
        return emptyString;
    }

    public void setAnswer(String answer){
        this.answer=answer;
    }

    // Лист новых английских слов
    public ArrayList<String> getEngWordsMas(){
        return engWordsMas;
    }

    public void setEngWordsMas(ArrayList <String> engWordsMas){
        this.engWordsMas=engWordsMas;
    }

    // Лист новых русских слов
    public ArrayList<String> getRusWordsMas(){
        return rusWordsMas;
    }

    public void setRusWordsMas(ArrayList <String> rusWordsMas){
        this.rusWordsMas=rusWordsMas;
    }

    // Объяснения к словам
    public String getHint(){
        if(hint!=null) {
            return hint;
        }
        return emptyString;
    }

    public void setHint(String hint){
        this.hint=hint;
    }

    // Название правила
    public String getRuleHeader(){
        if(ruleHeader!=null) {
            return ruleHeader;
        }
        return emptyString;
    }

    public void setRuleHeader(String ruleHeader){
        this.ruleHeader=ruleHeader;
    }

    //  Лист объяснений правил
    public ArrayList<String> getRuleText(){
        return ruleText;
    }

    public void setRuleText(ArrayList <String> ruleText){
        this.ruleText=ruleText;
    }

    // Лист примеров для объяснений
    public ArrayList<String> getRuleExample(){
        return ruleExample;
    }

    public void setRuleExample(ArrayList <String> ruleExample){
        this.ruleExample=ruleExample;
    }
}
