package com.rocketraven.emister;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.rocketraven.emister.ads.ShowAds;
import com.rocketraven.emister.objects.WorldObject;
import com.rocketraven.emister.optimization.Optimization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Darkmonk on 22.06.2016.
 */
public class EndOfLevelActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.content_frame) FrameLayout contentFrame;
    // ImageView
    @BindView(R.id.star_icon1) ImageView starIcon1;
    @BindView(R.id.star_icon2) ImageView starIcon2;
    @BindView(R.id.star_icon3) ImageView starIcon3;
    // TextView
    @BindView(R.id.mistakes_count) TextView mistakesCount;
    @BindView(R.id.hint_count) TextView hintCountText;
    @BindView(R.id.text_lvl_number) TextView lvlNumberText;
    // Button
    @BindView(R.id.btn_main_menu) Button mainMenuBut;
    @BindView(R.id.btn_next_lvl) Button nextLvlBut;
    @BindView(R.id.btn_restart) Button restartBut;
    Button lastButton=null;
    // Примитивы
    MediaPlayer mPlayerBack;
    boolean adsCount=true;
    boolean premiumUser;
    ShowAds showAds;
    ArrayList<ImageView> allStarsIcon=new ArrayList();
    Handler handler=new Handler();
    int bangCount=0;
    int levelRating=1;
    boolean soundOn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_of_level);
        ButterKnife.bind(this);

        // Оптимизация интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(mainFrame);

        mPlayerBack = MediaPlayer.create(EndOfLevelActivity.this, R.raw.sound_end_screen);

        // Определяем параметры звука
        soundOn=Paper.book("settings").read("sound",true);

        // Проверка на премиум
        premiumUser=Paper.book("inapp").read("premium",false);

            // Инициализация admob
            showAds = new ShowAds();
            showAds.loadInterstitial(this, getResources().getString(R.string.admob_interstitial_key));
            showAds.mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    lastButton.callOnClick();
                }
            });

        // Добавляем звёзды в Лист
        allStarsIcon.add(starIcon1); allStarsIcon.add(starIcon2); allStarsIcon.add(starIcon3);

        // Узнаём кол-во полученных звёзд за уровень
        int mistakes=getIntent().getIntExtra("rating",0);
         if(mistakes==0){
             levelRating=3;
         }
        if(mistakes==1||mistakes==2){
            levelRating=2;
        }

        // Получаем пройденный уровень
        if(getIntent().getStringExtra("lvlType").equals("common")) {
            lvlNumberText.setText(getIntent().getStringExtra("lvlName"));
        }

        // Выводим кол-во неправильных ответов
        mistakesCount.setText(mistakesCount.getText().toString()+String.valueOf(mistakes));

        // Взрыв нужного кол-ва звёздочек
        starBang();

        // Определяем выполнение ежедневных заданий
        EverydayTasksCheck taskCheck=new EverydayTasksCheck();

        // Сохраняем прогресс если это не тренировка слов
        if(getIntent().getStringExtra("lvlType").equals("train")) {
            // Задание на прохождение тренировки
            taskCheck.checkTask(2);
            lvlNumberText.setText("Тренировка");
            nextLvlBut.setVisibility(View.GONE);
        }else{
            // Задание на прохождение урока в приключении
            taskCheck.checkTask(1);
            saveLvlProgress();

            // Метрика на конец
            metricaEvent("Окончание уровня");
        }

        // Проверяем, последний ли это уровень для обычных уровней
        if(getIntent().getIntExtra("currentLvl", 1)==15){
            nextLvlBut.setVisibility(View.GONE);
        }

        if(getIntent().getIntExtra("currentLvl", 1)==11) {
            // Проверяем последний ли это уровень для тематических
            switch (getIntent().getStringExtra("currentWorld")) {
                case "world_food":
                    nextLvlBut.setVisibility(View.GONE);
                    break;
                case "world_travel":
                    nextLvlBut.setVisibility(View.GONE);
                    break;
                case "world_hotel":
                    nextLvlBut.setVisibility(View.GONE);
                    break;
                case "world_shopping":
                    nextLvlBut.setVisibility(View.GONE);
                    break;
                case "world9":
                    nextLvlBut.setVisibility(View.GONE);
                    break;
                case "world10":
                    nextLvlBut.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    }

    // Сохраняем результат пройденного уровня
    public void saveLvlProgress(){

        // Узнаём текущий мир и уровень
         Integer currentProgress[]= Paper.book(getIntent().getStringExtra("currentWorld"))
                .read("progress",new Integer[30]);

        // Сохраняем прогресс в ячейку с текущим уровнем
        if(currentProgress[getIntent().getIntExtra("currentLvl", 1) ]==null){

            // Сохраняем последний пройденный уровень
            Paper.book("mainProgress").write(getIntent().getStringExtra("currentWorld"),getIntent().getIntExtra("currentLvl", 1)+1);

            currentProgress[getIntent().getIntExtra("currentLvl", 1) ] = levelRating;
            // Добавляем 5 подсказок если пройден обычный уровень
            if(getIntent().getStringExtra("lvlType").equals("common")){

                int hintCount= Paper.book().read("hintCount",10);
                hintCountText.setVisibility(View.VISIBLE);
                if(hintCount+3<=30) {
                    Paper.book().write("hintCount", hintCount + 3);
                }
            }else{
                lvlNumberText.setText("Тренировка");
            }

        }else{
            if(currentProgress[getIntent().getIntExtra("currentLvl", 1) ]<levelRating){
                currentProgress[getIntent().getIntExtra("currentLvl", 1) ] = levelRating;
            }
        }

        Paper.book(getIntent().getStringExtra("currentWorld")).write("progress",currentProgress);

        // Сохраняем на сервере обнову
        UserProgress userProgress=new UserProgress(this);
        userProgress.getServerProgress();
    }

    // Перход на главный экран
    public void mainScreenClick(View v){
        if(showAds.mInterstitialAd.isLoaded()&&adsCount&&!premiumUser){
            showAds.showInterstitial();
            lastButton=mainMenuBut;
            adsCount=false;
        }else{
            startActivity(new Intent(this, MainActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK ));
        }
    }

    // Рестарт теущего уровня
    public void restartClick(View v){
        // Показываем полноэкранный банер
        if(showAds.mInterstitialAd.isLoaded()&&adsCount&&!premiumUser){
            showAds.showInterstitial();
            lastButton=restartBut;
            adsCount=false;
        }else {

            // Переход. Передаём текущий уровень и всю информацию
            switch (getIntent().getStringExtra("lvlType")) {

                case "common":
                    startActivity(new Intent(this, CommonLevel.class)
                            .putExtra("currentLvl", getIntent().getIntExtra("currentLvl", 0))
                            .putExtra("currentWorld", getIntent().getStringExtra("currentWorld")));
                    finish();
                    break;
                case "uncommon":
                    startActivity(new Intent(this, UnCommonLevel.class)
                            .putExtra("currentLvl", getIntent().getIntExtra("currentLvl", 0))
                            .putExtra("currentWorld", getIntent().getStringExtra("currentWorld")));
                    finish();
                    break;
                case "exam":
                    break;
                case "train":
                    finish();
            }
        }
    }

    // Переход на следующий уровень
    public void nextLvlClick(View v){
    // Показываем полноэкранный банер
        if(showAds.mInterstitialAd.isLoaded()&&adsCount&&!premiumUser){
            showAds.showInterstitial();
            lastButton=nextLvlBut;
            adsCount=false;
        }else {
            // Проверка на тип следующего уровня
            WorldObject currentWorld = Paper.book(getIntent().getStringExtra("currentWorld")).read("world");
            switch (currentWorld.allLevels.get(getIntent().getIntExtra("currentLvl", 0) + 1).levelType) {
                case "common":
                    startActivity(new Intent(this, CommonLevel.class)
                            .putExtra("currentLvl", getIntent().getIntExtra("currentLvl", 0) + 1)
                            .putExtra("currentWorld", getIntent().getStringExtra("currentWorld")));
                    finish();
                    break;
                case "uncommon":
                    startActivity(new Intent(this, UnCommonLevel.class)
                            .putExtra("currentLvl", getIntent().getIntExtra("currentLvl", 0) + 1)
                            .putExtra("currentWorld", getIntent().getStringExtra("currentWorld")));
                    finish();
                    break;
                case "exam":
                    break;
            }
        }
    }

    // Анимация взрыва звёзд
    public void starBang(){
        // Запускаем фоновую музыку
        if(soundOn) {
            mPlayerBack.start();
        }

        handler.postDelayed(new Runnable() {
            public void run() {
                allStarsIcon.get(bangCount).setImageResource(R.drawable.star_icon_orange);
                bangCount=bangCount+1;

                if(bangCount!=levelRating){
                    starBang();
                }

            }
        }, 50);
    }

    // Статистика Окончание уровня
    public void metricaEvent(String trainType){
        Map<String, Object> eventAttributes = new HashMap<String, Object>();
        eventAttributes.put("Adventure", trainType);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
