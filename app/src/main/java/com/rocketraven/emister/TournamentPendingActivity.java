package com.rocketraven.emister;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.rocketraven.emister.objects.AllIcons;
import com.rocketraven.emister.objects.PlayerObject;
import com.rocketraven.emister.objects.TournamentRound;
import com.rocketraven.emister.objects.TournamentTaskObject;
import com.rocketraven.emister.objects.UserMainObject;
import com.rocketraven.emister.optimization.Optimization;
import com.rocketraven.emister.retrofit.RetrofitRequestCallback;
import com.rocketraven.emister.retrofit.requests.GetTournamentBots;
import com.rocketraven.emister.retrofit.requests.GetTournamentTaskRequest;
import com.rocketraven.emister.retrofit.requests.RetrofitMainClass;
import com.rocketraven.emister.service.BackgroundMainTheme;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Darkmonk on 28.06.2016.
 */
public class TournamentPendingActivity extends AppCompatActivity {
    //FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.player_frame) FrameLayout playerFrame;
    @BindView(R.id.bot1_frame) FrameLayout bot1Frame;
    @BindView(R.id.bot2_frame) FrameLayout bot2Frame;
    @BindView(R.id.bot3_frame) FrameLayout bot3Frame;
    // ImageView
    @BindView(R.id.progress_circle) ImageView progressBar;
    @BindView(R.id.player_icon) ImageView playerIcon;
    @BindView(R.id.bot1_icon) ImageView bot1Icon;
    @BindView(R.id.bot2_icon) ImageView bot2Icon;
    @BindView(R.id.bot3_icon) ImageView bot3Icon;
    // TextView
    @BindView(R.id.search_text) TextView searchText;
    @BindView(R.id.player_nickname_text) TextView playerName;
    @BindView(R.id.bot1_text) TextView bot1Text;
    @BindView(R.id.bot2_text) TextView bot2Text;
    @BindView(R.id.bot3_text) TextView bot3Text;
    // AdView
    @BindView(R.id.adView) AdView smartBanner;
    // Примитивы
    RetrofitMainClass retrofitMainClass;
    GetTournamentTaskRequest getTournamentTaskRequest;
    GetTournamentBots getTournamentBots;
    MediaPlayer mPlayer;
    Integer allIconScope[];
    ArrayList<TournamentRound> roundsList=new ArrayList();
    ArrayList<PlayerObject> allPlayers=new ArrayList();
    ArrayList<String> previousPlayersScope=new ArrayList();
    int opponentsTime[]=new int[3];
    Random rand;
    Timer myTimer=null;
    TimerTask timerTask;
    Handler handler=new Handler();
    int timeCount=0;
    String myTableName = "tournament_tasks";
    boolean soundOn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament_pending);
        ButterKnife.bind(this);

        // Подгружаем SmartBanner
        if(!Paper.book("inapp").read("premium",false)) {
            smartBanner.setVisibility(View.VISIBLE);
            smartBanner.loadAd(new AdRequest.Builder()
                    .build());
        }

        // Подгружаем ретрофит
        retrofitMainClass=new RetrofitMainClass(this);

        // Запрос на получение заданий для турнира
        getTournamentTaskRequest=retrofitMainClass.retrofit.create(GetTournamentTaskRequest.class);
        // Запрос на получение 3 ботов-оппонентов
        getTournamentBots=retrofitMainClass.retrofit.create(GetTournamentBots.class);

        // Останавливаем фоновую музыку
        stopService(new Intent(this, BackgroundMainTheme.class));

        // Объявляем Random
        rand=new Random();

        // Оптимизация интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(mainFrame);
        optimization.Optimization(playerFrame);
        optimization.Optimization(bot1Frame);
        optimization.Optimization(bot2Frame);
        optimization.Optimization(bot3Frame);

        // Получаем все иконки аватарок
        AllIcons allIcons=new AllIcons();
        allIconScope=allIcons.getIcons();

        // Узнаём включена ли музыка
        soundOn=Paper.book("settings").read("sound",true);

        // Получаем текущего пользователя
        UserMainObject userMainObject=Paper.book("user").read("mainInfo",new UserMainObject());

        // Подгружаем иконку пользователя
        playerIcon.setImageResource(allIconScope[userMainObject.userPic]);
        String fullName=userMainObject.userName;
        if(fullName.length()>12){
           fullName= fullName.substring(0,10)+"...";
        }

        playerName.setText(fullName);

        // Определяем уровень сложности
        int userScore=Paper.book("account").read("score",0);
        checkTaskLvl(userScore);

        // Запускаем фоновую музыку
        mPlayer  = MediaPlayer.create(this, R.raw.tournament_pending);
        if (soundOn) {
            mPlayer.start();
        }

        // Запускам ProgressBar
        progressAnimation();

        // Получаем с сервера задания для текущего уровня
        getTasksFromServer();

    }

    // Анимация кручения ProgressBar
    public void progressAnimation(){
        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotation);
        rotation.setRepeatCount(Animation.INFINITE);
        progressBar.startAnimation(rotation);
    }

    // Выкачиваем с сервера рандомный набор заданий для турнира
    public void getTasksFromServer(){

        // Отправляем запрос на получение заданий в турнире
        Call<List<TournamentTaskObject>> getCurrentTournament=getTournamentTaskRequest.getTournamentInfo("tournament/"+myTableName);
        retrofitMainClass.createRequest(getCurrentTournament);

        retrofitMainClass.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object response) {
                Response<List<TournamentTaskObject>> currentResponse=(Response<List<TournamentTaskObject>>)response;

                if(currentResponse.code()==200){

                    List<TournamentTaskObject> allTasks=currentResponse.body();

                    if(allTasks==null){
                        createToast("Произошла ошибка");
                        return;
                    }

                    TournamentTaskObject currentTask=currentResponse.body().get(rand.nextInt(allTasks.size()));
                    TournamentRound round1=new TournamentRound();
                    TournamentRound round2=new TournamentRound();
                    TournamentRound round3=new TournamentRound();
                    TournamentRound round4=new TournamentRound();
                    TournamentRound round5=new TournamentRound();
                    TournamentRound round6=new TournamentRound();
                    TournamentRound round7=new TournamentRound();

                    // Первый раунд
                    round1.roundId=1;
                    round1.roundQuestion=currentTask.round1Object.getAsJsonObject("round").get("roundQuestion").getAsString();
                    round1.roundAnswer=currentTask.round1Object.getAsJsonObject("round").get("roundAnswer").getAsString();
                    round1.roundAnswers=currentTask.round1Object.getAsJsonObject("round").get("roundAnswers").getAsString().split("#");
                    roundsList.add(round1);

                    // Второй раунд
                    round2.roundId=2;
                    round2.roundQuestion=currentTask.round2Object.getAsJsonObject("round").get("roundQuestion").getAsString();
                    round2.roundAnswer=currentTask.round2Object.getAsJsonObject("round").get("roundAnswer").getAsString();
                    round2.roundAnswers=currentTask.round2Object.getAsJsonObject("round").get("roundAnswers").getAsString().split("#");
                    roundsList.add(round2);

                    // Третий раунд
                    round3.roundId=3;
                    round3.roundQuestion=currentTask.round3Object.getAsJsonObject("round").get("roundQuestion").getAsString();
                    round3.roundAnswer=currentTask.round3Object.getAsJsonObject("round").get("roundAnswer").getAsString();
                    roundsList.add(round3);

                    // Четвёртый раунд
                    round4.roundId=4;
                    round4.roundQuestion=currentTask.round4Object.getAsJsonObject("round").get("roundQuestion").getAsString();
                    round4.roundAnswer=currentTask.round4Object.getAsJsonObject("round").get("roundAnswer").getAsString();
                    round4.roundAnswers=currentTask.round4Object.getAsJsonObject("round").get("roundAnswers").getAsString().split("#");
                    roundsList.add(round4);

                    // Пятый раунд
                    round5.roundId=5;
                    round5.roundQuestion=currentTask.round5Object.getAsJsonObject("round").get("roundQuestion").getAsString();
                    round5.roundAnswer=currentTask.round5Object.getAsJsonObject("round").get("roundAnswer").getAsString();
                    round5.roundAnswers=currentTask.round5Object.getAsJsonObject("round").get("roundAnswers").getAsString().split("#");
                    roundsList.add(round5);

                    // Шестой раунд
                    round6.roundId=6;
                    round6.roundQuestion=currentTask.round6Object.getAsJsonObject("round").get("roundQuestion").getAsString();
                    round6.roundAnswer=currentTask.round6Object.getAsJsonObject("round").get("roundAnswer").getAsString();
                    roundsList.add(round6);

                    // Седьмой раунд
                    round7.roundId=7;
                    round7.roundQuestion=currentTask.round7Object.getAsJsonObject("round").get("roundQuestion").getAsString();
                    round7.roundAnswer=currentTask.round7Object.getAsJsonObject("round").get("roundAnswer").getAsString();
                    round7.roundAnswers=currentTask.round7Object.getAsJsonObject("round").get("roundAnswers").getAsString().split("#");
                    roundsList.add(round7);

                    // Сохраняем текущий лист заданий
                    Paper.book().write("taskList",roundsList);

                    // Получаем с сервера трёх противников
                    getOpponents();

                }else{
                    createToast("В данный момент турнир не доступен");
                    finish();
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                createToast("В данный момент турнир не доступен");
                finish();
            }
        });

    }

    // Получаем с сервера трёх противников
    public void getOpponents(){

        Call<List<PlayerObject>> getBots=getTournamentBots.getBots();

        retrofitMainClass.createRequest(getBots);
        retrofitMainClass.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object response) {
                Response<List<PlayerObject>> currentResponse
                        =(Response<List<PlayerObject>>)response;

                if(currentResponse.code()==200){
                    allPlayers=(ArrayList<PlayerObject>) currentResponse.body();

                    if(allPlayers==null){
                        createToast("В данный момент турнир не доступен");
                        finish();
                    }

                    // Сохраняем список игроков для следующей игры
                    Paper.book().write("players_scope",allPlayers);

                    // Включаем счётчик и подбор соперников
                    timeOpponents();

                }else{
                    createToast("В данный момент турнир не доступен");
                    finish();
                }
            }

            @Override
            public void onEventFailure(Throwable error) {

            }
        });

    }

    // Рандомим время загрузки противников
    public void timeOpponents(){

        // Рандомим поиск по оппонентам
        for (int i=0;i<opponentsTime.length;i++){
            switch (i){
                case 0:  opponentsTime[i]=rand.nextInt(11)+2;
                    break;
                case 1:  opponentsTime[i]=opponentsTime[i-1]+rand.nextInt(11)+2;
                    break;
                case 2: opponentsTime[i]=opponentsTime[i-1]+rand.nextInt(11)+2;
            }
        }

        // Запускаем таймер
        myTimer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        timeCount=timeCount+1;
                        Log.d("ВремяПендинг", String.valueOf(timeCount));
                        // Меняем кол-во точек в тексте поиска
                        if(timeCount<opponentsTime[2]) {
                            switch (timeCount % 4) {
                                case 0:
                                    searchText.setText("Идёт поиск соперников...");
                                    break;
                                case 3:
                                    searchText.setText("Идёт поиск соперников..");
                                    break;
                                case 2:
                                    searchText.setText("Идёт поиск соперников.");
                                    break;
                                case 1:
                                    searchText.setText("Идёт поиск соперников");
                                    break;
                            }
                        }

                        // Проверяем готовность соперников
                        if(timeCount==opponentsTime[0]){
                            bot1Icon.setImageResource(allIconScope[allPlayers.get(0).iconId]);
                            bot1Text.setText(allPlayers.get(0).name);
                            if(!checkInternet()){
                                createToast("Отсутствует интернет соединение");
                                finish();
                            }
                        }
                        if(timeCount==opponentsTime[1]){
                            bot2Icon.setImageResource(allIconScope[allPlayers.get(1).iconId]);
                            bot2Text.setText(allPlayers.get(1).name);
                            if(!checkInternet()){
                                createToast("Отсутствует интернет соединение");
                                finish();
                            }
                        }
                        if(timeCount==opponentsTime[2]){
                            bot3Icon.setImageResource(allIconScope[allPlayers.get(2).iconId]);
                            bot3Text.setText(allPlayers.get(2).name);
                            searchText.setText("Приготовьтесь к схватке");
                            if(!checkInternet()){
                                createToast("Отсутствует интернет соединение");
                                finish();
                            }
                        }
                        if(timeCount==opponentsTime[2]+5){
                            if(mPlayer.isPlaying()){
                                mPlayer.stop();
                            }
                            myTimer.cancel();
                            myTimer.purge();
                            startActivity(new Intent(TournamentPendingActivity.this,TournamentActivity.class)
                                    .putExtra("gameType",getIntent().getStringExtra("gameType")));
                            finish();
                        }
                    }
                });
            }};

        myTimer.schedule(timerTask, 0, 1000);
    }

    // Определяем ур-нь сложности
    public void checkTaskLvl(int userScore){
        if(userScore<100){
            myTableName="tournament_task1";
            return;
        }
        if(userScore<300){
            myTableName="tournament_task2";
            return;
        }
        if (userScore>=300){
            myTableName="tournament_task3";
            return;
        }
         myTableName="tournament_task2";
    }

    // Проверка интернет соединения
    public boolean checkInternet(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    // Создание тоастов
    public void createToast(String text){
        Toast.makeText(this, text,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if(myTimer!=null){
            myTimer.cancel();
            myTimer.purge();
        }
        if(mPlayer.isPlaying()){
            mPlayer.stop();
        }
        finish();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mPlayer.isPlaying()){
            mPlayer.stop();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(mPlayer!=null&&soundOn){
            mPlayer.start();
        }
    }

    public void onDestroy(){
        if(myTimer!=null){
            myTimer.cancel();
        }
        if(mPlayer.isPlaying()){
            mPlayer.stop();
        }
        super.onDestroy();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
