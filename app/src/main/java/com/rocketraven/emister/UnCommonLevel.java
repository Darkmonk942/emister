package com.rocketraven.emister;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.rocketraven.emister.ads.ShowAds;
import com.rocketraven.emister.objects.LevelObject;
import com.rocketraven.emister.objects.WorldObject;
import com.rocketraven.emister.optimization.Optimization;
import com.rocketraven.emister.trains.CreateLevelTasks;
import com.rocketraven.emister.trains.TrainTask10;
import com.rocketraven.emister.trains.TrainTask11;
import com.rocketraven.emister.trains.TrainTask12;
import com.rocketraven.emister.trains.TrainTask13;
import com.rocketraven.emister.trains.TrainTask14;
import com.rocketraven.emister.trains.TrainTask15;
import com.rocketraven.emister.trains.TrainTask4;
import com.rocketraven.emister.trains.TrainTask5;
import com.rocketraven.emister.trains.TrainTask6;
import com.rocketraven.emister.trains.TrainTask7;
import com.rocketraven.emister.trains.TrainTask8;
import com.rocketraven.emister.trains.TrainTask9;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Darkmonk on 25.06.2016.
 */
public class UnCommonLevel extends AppCompatActivity {
    // ScrollView
    @BindView(R.id.words_frame_scroll) ScrollView wordsScroll;
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.content_frame) FrameLayout contentFrame;
    @BindView(R.id.card_frame_center) FrameLayout cardFrameCenter;
    @BindView(R.id.words_frame) FrameLayout wordsFrame;
    @BindView(R.id.audio_buttons_frame) FrameLayout audioButtonsFrame;
    // LinearLayout
    @BindView(R.id.progress_linear) LinearLayout progresLinear;
    @BindView(R.id.left_linear_words) LinearLayout leftLinearWords;
    @BindView(R.id.right_linear_words) LinearLayout rightLinearWords;
    // TextView
    @BindView(R.id.time_count_text) TextView timeCountText;
    @BindView(R.id.task_description) TextView taskDescription;
    @BindView(R.id.text_eng_center) TextView textEngCenter;
    @BindView(R.id.text_rus_center) TextView textRusCenter;
    @BindView(R.id.lvl_name) TextView lvlName;
    @BindView(R.id.question_text) TextView questionText;
    @BindView(R.id.answer_field) TextView answerField;
    @BindView(R.id.lives_text) TextView livesText;
    @BindView(R.id.progress) TextView progressText;
    @BindView(R.id.hint_count_text) TextView hintCountText;
    @BindView(R.id.rule_true) TextView ruleTrue;
    // Button
    @BindView(R.id.help_but) Button helpButton;
    @BindView(R.id.next_button) Button nextButton;
    @BindView(R.id.answer1) Button answer1;
    @BindView(R.id.answer2) Button answer2;
    @BindView(R.id.answer3) Button answer3;
    @BindView(R.id.answer4) Button answer4;
    @BindView(R.id.answer_sound1) Button answerSound1;
    @BindView(R.id.answer_sound2) Button answerSound2;
    @BindView(R.id.answer_sound3) Button answerSound3;
    @BindView(R.id.answer_sound4) Button answerSound4;
    @BindView(R.id.question_sound) Button questionSound;Button lastSound=null;
    // ImageView
    @BindView(R.id.btn_delete) ImageView deleteSymbol;
    @BindView(R.id.sound_icon_center) ImageView soundIconCenter;
    // Примитивы
    TextToSpeech tts;
    MediaPlayer answerTruePlayer;
    MediaPlayer answerFalsePlayer;
    int gameTime=240;
    private RewardedVideoAd mAd;
    ShowAds showAds;
    public static String lvlType="uncommon";
    public static String currentIntentWorld;
    public static int currentIntentLvl;
    public static ArrayList<Integer> usedWords=new ArrayList();
    HashMap<String, String> myHashAlarm = new HashMap<String, String>();
    public static HashMap<String,Integer> wordsPackMap=new HashMap<String, Integer>();
    ArrayList<Integer> allTasks;
    ArrayList<ImageView> allHintCardIcon=new ArrayList();
    ArrayList<Button> allAnswers=new ArrayList();
    ArrayList<Button> allAnswersSound=new ArrayList();
    Timer myTimer;
    TimerTask timerTask;
    int maxLives=0;
    int currentTime=0;
    Handler handler=new Handler();
    Handler handlerTimer=new Handler();
    Handler handlerRound=new Handler();
    int progressCellWidth,fivePxWidth;
    int currentTask=0;
    LevelObject levelObject;
    public static boolean currentTaskAnswer=false;
    public static int lives=3;
    boolean showCenterFrame=false;
    public static int levelRating=0;
    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;
    boolean premiumUser;
    boolean soundOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uncommon_level);
        ButterKnife.bind(this);

        // Обнуляем все слова
        wordsPackMap.clear();
        usedWords.clear();

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        soundOn=Paper.book("settings").read("sound",true);
        answerFalsePlayer =MediaPlayer.create(this,R.raw.sound_answer_false);
        answerTruePlayer = MediaPlayer.create(this, R.raw.sound_answer_true);

        // Подгружаем Синтезатор голоса
        tts=EmisterApp.tts;

        // Проверка на премиум
        premiumUser=Paper.book("inapp").read("premium",false);

        // Initialize the Mobile Ads SDK.
        //mAd = MobileAds.getRewardedVideoAdInstance(this);
        //showAds = new ShowAds();
        //showAds.rewardedVideo(mAd, getResources().getString(R.string.admob_rewarded_video_key));

        showAds = new ShowAds();
        showAds.loadInterstitial(this, getResources().getString(R.string.admob_interstitial_key));
        showAds.mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
            }
        });


        levelRating=0;
        if(premiumUser) {
            // Плюшки премиум пользователя
            lives=5;
            livesText.setText("x5");
            gameTime=300;
        }else{
            lives=3;
        }

        // Оптимизация интерфейса
        optimizationUI();

        // Получаем Кол-во подсказок, доступных пользоателю
        hintCountText.setText(String.valueOf(Paper.book().read("hintCount",10)));

        // Определяем ширину прогресса
        int width=Paper.book().read("width");
        fivePxWidth=width/216;
        progressCellWidth=fivePxWidth*4;

        // Собираем в массивы элементы UI
        createUIList();

        // Цвета для взрывов жизней
        int colors[]={R.color.colorAccent,R.color.colorRed};

        // Анимации карточек
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);

        // Получем набор из 25 заданий
        CreateLevelTasks createLevelTasks=new CreateLevelTasks();
        allTasks= createLevelTasks.createUnCommonTasks();

        // Получаем все данные текущего мира
        WorldObject currentWorld= Paper.book(getIntent().getStringExtra("currentWorld")).read("world");
        currentIntentWorld=getIntent().getStringExtra("currentWorld");
        levelObject =currentWorld.allLevels.get(getIntent().getIntExtra("currentLvl",0));
        currentIntentLvl=getIntent().getIntExtra("currentLvl",0);
        lvlName.setText("Тренировка");

        // Заполняем HashMap текущим начальным набором слов
        addAllword();

        // Создаём стартовый интерфейс
        createUI(allTasks.get(0));

        // Контейнер параметров для TTS
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "utterance_id_1");

        // Запускаем таймер
        levelTimerStart();
    }

    // Вызов меню
    public void menuClick(View v){
        mainMenuDialog();
    }

    // Диалог бокового меню текущего экрана
    public void mainMenuDialog(){
        final Dialog openDialog = new Dialog(this);
        openDialog.setContentView(R.layout.custom_alert_main_menu);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        openDialog.setCancelable(true);
        openDialog.show();

        // Оптимизация диалога
        Optimization optimization=new Optimization();
        optimization.Optimization((FrameLayout)openDialog.findViewById(R.id.frame_alert_menu));
    }

    // Создаём интерфейс текущего уровня
    public void createUI(Integer taskType){
        switch (taskType){
            // Показываем для обучения 4 карточки
            case 1:

                break;
            // Показываем для обучения 2 карточки
            case 2:
            break;
            // Показываем правило для обучения
            case 3:
                break;
            // Определяем правильность вопроса с ответом ДА/НЕТ
            case 4:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Определи, правильный ли указан перевод");
                TrainTask4 trainTask4=new TrainTask4();
                trainTask4.trainTask4("uncommon",questionText,answer1,answer2,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // Выбрать правильный перевод с английского на русский (4 варианта)
            case 5:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Выбери правильный перевод");
                TrainTask5 trainTask5=new TrainTask5();
                trainTask5.trainTask5("uncommon",questionText,allAnswers,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // Выбрать правильный перевод с русского на английский (4 варианта)
            case 6:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Выбери правильный перевод");
                TrainTask6 trainTask6=new TrainTask6();
                trainTask6.trainTask6("uncommon",questionText,allAnswers,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // Собираем слово из английских букв (перевод с русского)
            case 7:
                helpButton.setBackgroundResource(R.drawable.hint_icon_green);
                helpButton.setClickable(true);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Напиши правильный перевод\n(без артиклей и частицы to)");
                TrainTask7 trainTask7=new TrainTask7();
                trainTask7.trainTask7("uncommon",questionText,wordsFrame,wordsScroll,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask,
                        answerField,deleteSymbol,this);
                break;
            // Озвучиваем слово на английском, требуется найти перевод (4 варианта)
            case 8:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Прослушай и выбери правильный перевод");
                TrainTask8 trainTask8=new TrainTask8();
                trainTask8.trainTask8("uncommon",questionSound,allAnswers,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // Озвучиваем слово, требуется его собрать из букв
            case 9:
                helpButton.setBackgroundResource(R.drawable.hint_icon_green);
                helpButton.setClickable(true);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Прослушай и запиши");
                TrainTask9 trainTask9=new TrainTask9();
                trainTask9.trainTask9("uncommon",questionSound,wordsFrame,wordsScroll,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask
                        ,answerField,deleteSymbol,this);
                break;
            // Дано слово на русском языке и 4 варианта-озвучки на английском
            case 10:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Прослушай варианты и выбери правильный перевод");
                TrainTask10 trainTask10=new TrainTask10();
                trainTask10.trainTask10("uncommon",questionText,allAnswersSound,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // 10 русских слов и 10 английских. Требуется найти соответствие
            case 11:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                taskDescription.setText("Найди соответствие между английскими и русскими словами");
                nextButton.setVisibility(View.GONE);
                TrainTask11 trainTask11=new TrainTask11();
                trainTask11.trainTask11("uncommon",livesText,levelObject.levelWordsEng,levelObject.levelWordsRus,
                        leftLinearWords,rightLinearWords,this,R.drawable.answers_shape_gray,
                        R.drawable.answer_shape_yellow,R.drawable.answers_shape_green,
                        R.drawable.answer_shape_red);
                break;
            // Вставить в готовое приложение правильный ответ
            case 12:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Выбери правильный вариант");
                TrainTask12 trainTask12=new TrainTask12();
                trainTask12.trainTask12(levelObject.levelRuleTrain,questionText,answerField,
                        allAnswers,lvlType,currentTask,false,ruleTrue);
                break;
            // Собрать из букв правильный ответ для вставки в приложение
            case 13:
                helpButton.setBackgroundResource(R.drawable.hint_icon_green);
                helpButton.setClickable(true);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Дополни предложение");
                TrainTask13 trainTask13=new TrainTask13();
                trainTask13.trainTask13("uncommon",levelObject.levelRuleTrain,questionText,wordsFrame,wordsScroll,
                        answerField,deleteSymbol,this,false,currentTask,ruleTrue);
                break;
            // Выбираем правильный вариант составления предложения
            case 14:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Выбери правильный перевод");
                TrainTask14 trainTask14=new TrainTask14();
                trainTask14.trainTsk14(levelObject.levelRuleTrain,questionText,allAnswers,lvlType,
                        false,currentTask,ruleTrue);
                break;
            // Собираем предложение из предложенных слов
            case 15:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Собери предложение из предложенных слов");
                TrainTask15 trainTask15=new TrainTask15();
                trainTask15.trainTask15("uncommon",currentTask,levelObject.levelRuleTrain,questionText,
                        wordsFrame,wordsScroll,answerField,deleteSymbol,this,false,ruleTrue);
                break;
        }
    }

    // Обработка кнопки Далее
    public void nextTaskClick(View v){
        String currentTrueAnswer="";
        // Изучение слов или правил
        if(allTasks.get(currentTask)==1||allTasks.get(currentTask)==2||
                allTasks.get(currentTask)==3||showCenterFrame==true){
            nextTaskMethod(0);
        }else{
            // Проверка заданий с набором слов
            switch (allTasks.get(currentTask)){
                case 7:
                    if(currentTaskAnswer){
                        singleWordsSound(answerField.getTag().toString());
                        nextTaskMethod(1200);
                    }else{
                        answerField.setTextColor(getResources().getColor(R.color.colorRed));
                        currentTrueAnswer=answerField.getTag().toString();
                        wrongAnswer(currentTrueAnswer);
                    }
                    break;
                case 9:
                    if(currentTaskAnswer){
                        singleWordsSound(questionSound.getTag().toString());
                        nextTaskMethod(1200);
                    }else{
                        answerField.setTextColor(getResources().getColor(R.color.colorRed));
                        currentTrueAnswer=questionSound.getTag().toString();
                        wrongAnswer(currentTrueAnswer);
                    }
                    break;
                case 10:
                    if(currentTaskAnswer){
                        nextTaskMethod(1200);
                    }else{

                        if(lastSound!=null){
                            lastSound.setBackgroundResource(R.drawable.volume_icon_red);
                        }
                        wrongAnswer(questionText.getTag().toString());
                    }
                    break;
                case 13:
                    if(currentTaskAnswer){
                        nextTaskMethod(1200);
                    }else{
                        answerField.setTextColor(getResources().getColor(R.color.colorRed));
                        nextTaskMethod(1200);
                    }
                    break;
                case 15:
                    if(currentTaskAnswer){
                        nextTaskMethod(1200);
                    }else{
                        answerField.setTextColor(getResources().getColor(R.color.colorRed));
                        nextTaskMethod(1200);
                    }
                    break;
            }
        }
    }

    // Переходим на следующе задание
    public void nextTaskMethod(int time){

        handler.postDelayed(new Runnable() {
            public void run() {
                // Анимация Кручения
                roundCentralFrame();

                // Красим кнопки ответов в серый цвет
                for(int i=0;i<allAnswers.size();i++){
                    allAnswers.get(i).setBackgroundResource(R.drawable.answers_shape_gray);
                }

                if(allTasks.get(currentTask)==1||allTasks.get(currentTask)==2||
                        allTasks.get(currentTask)==3||showCenterFrame==true){
                    currentTaskAnswer=true;
                }

                if(!currentTaskAnswer){
                    lives=lives-1;
                    if(lives==0){
                        showAlertLives();
                    }
                    livesText.setText("x"+String.valueOf(lives));
                    levelRating=levelRating+1;
                }else{
                    if(soundOn) {
                        answerSound(true);
                    }
                }

                showCenterFrame=false;
                currentTask=currentTask+1;
                currentTaskAnswer=false;
                cardFrameCenter.setVisibility(View.GONE);

                // Меняем кол-во вопросов Удлинняем шкалу прогресса
                progressText.setText(String.valueOf(currentTask+1)+" из 25");

                ImageView image =new ImageView(getApplicationContext());
                LinearLayout.LayoutParams imageParams=new LinearLayout.LayoutParams(progressCellWidth,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                image.setBackgroundResource(R.drawable.part_of_progress);
                progresLinear.addView(image,imageParams);

                // Скрываем весь UI
                hideUi();

                // Подгружаем новый вопрос и новый UI
                createUI(allTasks.get(currentTask));
            }
        }, time);
    }

    // Обработка кнопок Ответы
    public void answerClick(View v){
        Button but=(Button) v;
        boolean fastAnswer=true;
        Button probablyAnswer=null;
        String currentTrueAnswer="";

        // Убираем кликабельность вариантам ответов
        for (int i=0;i<allAnswers.size();i++){
            allAnswers.get(i).setClickable(false);
        }

        // Красим выбранный ответ в зелёный
        but.setBackgroundResource(R.drawable.answers_shape_green);

        Log.d("Тестим",String.valueOf(allTasks.get(currentTask)));
        switch (allTasks.get(currentTask)) {
            case 4:
                if (questionText.getTag().toString().equals(but.getText().toString())) {
                    currentTaskAnswer = true;
                } else {
                    currentTaskAnswer = false;

                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(questionText.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                            currentTrueAnswer=allAnswers.get(i).getTag().toString();
                        }
                    }
                }
                break;
            case 5:
                if(questionText.getTag().toString().equals(but.getText().toString())){
                    currentTaskAnswer = true;
                } else {
                    currentTaskAnswer = false;
                    currentTrueAnswer=questionText.getText().toString();
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(questionText.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                        }
                    }
                }
                break;
            case 6:
                if(questionText.getTag().toString().equals(but.getText().toString())){
                    currentTaskAnswer = true;
                } else {
                    currentTaskAnswer = false;
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(questionText.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                            currentTrueAnswer=allAnswers.get(i).getText().toString();
                        }
                    }
                }
                break;
            case 8:
                if (but.getText().toString().equals(but.getTag().toString())) {
                    currentTaskAnswer = true;
                } else {
                    currentTaskAnswer = false;
                    currentTrueAnswer=questionSound.getTag().toString();
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(allAnswers.get(i).getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                        }
                    }
                }
                break;
            case 12:
                fastAnswer=true;
                answerField.setText(answerField.getText().toString().replace("__",but.getText().toString()));
                if (but.getText().toString().equals(answerField.getTag().toString())) {
                    currentTaskAnswer = true;
                } else {
                    fastAnswer=false;
                    currentTaskAnswer = false;
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(answerField.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                        }
                    }
                }
                break;
            case 14:
                if(questionText.getTag().toString().equals(but.getText().toString())){
                    currentTaskAnswer = true;
                } else {
                    fastAnswer=false;
                    currentTaskAnswer = false;
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(questionText.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                        }
                    }
                }
                break;
        }

        // Проверка на быстрый ответ
        checkTrueAnswerAnimation(but,probablyAnswer,currentTrueAnswer,fastAnswer);

    }

    // Хендлер на проверку правильности ответа
    public void checkTrueAnswerAnimation(final Button correctAnswer, final Button probablyAnswer,
                                         final String currentTrueAnswer, final boolean fastAnswer){
        handlerRound.postDelayed(new Runnable() {
            public void run() {
                // Правильный ответ
                if(currentTaskAnswer) {
                    // Получаем звук в зависимости от задания
                    switch (allTasks.get(currentTask)){
                        case 4:
                            singleWordsSound(correctAnswer.getTag().toString());
                            break;
                        case 5:
                            singleWordsSound(questionText.getText().toString());
                            break;
                        case 6:
                            singleWordsSound(correctAnswer.getText().toString());
                            break;
                        case 7:
                            singleWordsSound(answerField.getTag().toString());
                            break;
                        case 8:
                            singleWordsSound(correctAnswer.getTag().toString());
                            break;
                        case 9:
                            singleWordsSound(answerField.getTag().toString());
                            break;
                        case 12:
                            break;
                        case 13:
                            break;
                        case 14:
                            singleWordsSound(questionText.getTag().toString());
                            break;
                        case 15:
                            break;
                    }

                    nextTaskMethod(1200);

                    // Неправильный ответ
                }else{
                    correctAnswer.setBackgroundResource(R.drawable.answer_shape_red); // Пометка неправильного ответа
                    probablyAnswer.setBackgroundResource(R.drawable.answers_shape_green); // Пометка правильного

                    // Выводим карточку с правильным ответом
                    if(fastAnswer) {
                        wrongAnswer(currentTrueAnswer);
                    }else{
                        lives=lives-1;
                        if(lives==0){
                            showAlertLives();
                        }
                        livesText.setText("x"+String.valueOf(lives));
                        nextTaskMethod(1200);
                        levelRating=levelRating+1;
                    }
                }
            }
        }, 300);
    }

    // При неверном ответе скрываем старый UI и показываем карточку правильного ответа
    public void wrongAnswer(final String answer){
        // Убираем видимость подсказки
        taskDescription.setText("Повторяем слово");

        // Звук неверного ответа
        if (soundOn) {
            answerFalsePlayer.start();
        }

        lives=lives-1;
        if(lives==0){
            showAlertLives();
        }
        livesText.setText("x"+String.valueOf(lives));
        levelRating=levelRating+1;

        handler.postDelayed(new Runnable() {
            public void run() {
                hideUi();
                cardFrameCenter.setVisibility(View.VISIBLE);
                showCenterFrame=true;
                nextButton.setVisibility(View.VISIBLE);

                // Меняем контент внутри карточки на правильный ответ
                ArrayList<String> checkList=new ArrayList();
                checkList.add("a "+answer);
                checkList.add("an "+answer);
                checkList.add("to "+answer);
                checkList.add(answer);

                // Меняем контент внутри карточки на правильный ответ
                for(int j=0;j<checkList.size();j++) {
                    for (int i = 0; i < levelObject.levelWordsEng.size(); i++) {
                        if (levelObject.levelWordsEng.get(i).equals(checkList.get(j))) {
                            textEngCenter.setText(levelObject.levelWordsEng.get(i));
                            textRusCenter.setText(levelObject.levelWordsRus.get(i));
                            soundIconCenter.setTag(levelObject.levelWordsEng.get(i));

                            singleWordsSound(checkList.get(j));
                        }
                    }
                }

                // Озвучиваем правильный ответ

            }
        }, 1000);
    }

    // Озвучивание переданного слова
    public void singleWordsSound(String word){
        WordsSoundManager.textToSpeech(word);
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(word, TextToSpeech.QUEUE_FLUSH, null,"utterence1");
        }else{
            tts.speak(word, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
        }*/
    }

    // Обработка TextToSpeach при одном звуковом варианте
    public void singleSoundQuestionClick(View v){
        if(v.getTag()!=null){
            WordsSoundManager.textToSpeech(v.getTag().toString());
        }

        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(v.getTag().toString(), TextToSpeech.QUEUE_FLUSH, null,"utterence1");
        }else{
            tts.speak(v.getTag().toString(), TextToSpeech.QUEUE_FLUSH, myHashAlarm);
        }*/
    }

    // Обработка TextToSpeach при нескольких звуковых вариантах
    public void soundQuestionClick(View v){

        WordsSoundManager.textToSpeech(v.getTag().toString());
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(v.getTag().toString(), TextToSpeech.QUEUE_FLUSH, null,"utterence1");
        }else{
            tts.speak(v.getTag().toString(), TextToSpeech.QUEUE_FLUSH, myHashAlarm);
        }*/

        if(questionText.getTag().toString().equals(v.getTag().toString())){
            currentTaskAnswer = true;
        } else {
            currentTaskAnswer = false;}

        // Все иконки звука возвращаем в серый
        for (int i=0;i<allAnswersSound.size();i++){
            allAnswersSound.get(i).setBackgroundResource(R.drawable.volume_icon_gray);
        }
        v.setBackgroundResource(R.drawable.volume_icon);
        lastSound=(Button)v;
    }

    // Собираем листы UI элементов
    public void createUIList(){
        allAnswers.add(answer1); allAnswers.add(answer2);
        allAnswers.add(answer3); allAnswers.add(answer4);

        allAnswersSound.add(answerSound1); allAnswersSound.add(answerSound2);
        allAnswersSound.add(answerSound3); allAnswersSound.add(answerSound4);

    }

    // Скрываем ненужные элементы интерфейса
    public void hideUi(){

        // Скрываем текст вопроса
        questionText.setVisibility(View.GONE);

        // Возвращаем цвет полю ответов
        answerField.setTextColor(getResources().getColor(R.color.colorAccent));

        // Скрываем поле для ответа и чистим от старых символов
        answerField.setVisibility(View.GONE);
        answerField.setText("");

        // Скрываем поля-варианты ответов
        for (int i=0;i<allAnswers.size();i++){
            allAnswers.get(i).setVisibility(View.GONE);
        }

        // Восстанавливаем кликабельность вариантам ответов
        for (int i=0;i<allAnswers.size();i++){
            allAnswers.get(i).setClickable(true);
        }

        // Скрываем поля-варианты ответов с озвучкой
        for (int i=0;i<allAnswersSound.size();i++){
            allAnswersSound.get(i).setVisibility(View.GONE);
        }

        // Все иконки звука возвращаем в серый
        for (int i=0;i<allAnswersSound.size();i++){
            allAnswersSound.get(i).setBackgroundResource(R.drawable.volume_icon_gray);
        }

        // Чистим и Скрываем фрейм для сбора слов и предложений
        wordsFrame.removeAllViews();
        wordsScroll.setVisibility(View.GONE);

        // Скрываем кнопку звукового вопроса
        questionSound.setVisibility(View.GONE);

        // Скрываем фрейм для изучения правил
        //ruleFrame.setVisibility(View.GONE);

        // Скрываем кнопку удаления букв
        deleteSymbol.setVisibility(View.GONE);
    }

    // Метод создания анимации
    public void createAnimation(boolean cardVisibility,CardView frontCard,CardView backCard){
        if (!cardVisibility) {
            mSetRightOut.setTarget(frontCard);
            mSetLeftIn.setTarget(backCard);
            mSetRightOut.start();
            mSetLeftIn.start();
        } else {
            mSetRightOut.setTarget(backCard);
            mSetLeftIn.setTarget(frontCard);
            mSetRightOut.start();
            mSetLeftIn.start();
        }
    }

    // Анимация главного центрального фрейма
    public void roundCentralFrame(){
        ObjectAnimator.ofFloat(contentFrame, "rotationY", 0f, 360f).setDuration(800).start();
        changeCameraDistanceMain();
    }

    // Отдаление камеры  главного фрейма
    private void changeCameraDistanceMain() {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        contentFrame.setCameraDistance(scale);
    }

    // оптимизация всех элементов
    public void optimizationUI(){
        Optimization optimization=new Optimization();
        optimization.Optimization(mainFrame);

        optimization.Optimization(cardFrameCenter);

        optimization.Optimization(audioButtonsFrame);
    }

    // Добавляем в словарь текущего уровня все слова
    public void addAllword(){
        for (int i=0;i<levelObject.levelWordsEng.size();i++){
            wordsPackMap.put(levelObject.levelWordsEng.get(i),0);
        }
    }

    // Обработка кнопки подсказок
    public void useHintBut(View v){

        // Проверка на наличие подсказок
        if(!hintCountText.getText().toString().equals("0")) {

            if(hintCountText.getText().toString().equals("1")) {
                // Показываем экран приобритения подсказок
                showAlertHint();
            }
            // Подсказка для Задания 7 и 9
            if(allTasks.get(currentTask)==7||allTasks.get(currentTask)==9) {
                // Узнаём вписан ли уже правильный ответ
                if (answerField.getText().toString().toLowerCase().equals(answerField.getTag().toString().toLowerCase())) {
                    hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) - 1));
                    Paper.book().write("hintCount",Integer.valueOf(hintCountText.getText().toString()));
                    nextButton.callOnClick();
                    return;
                }

                // Узнаём правильно ли прописан текст и нужна ли подсказка
                boolean checkWords = true;

                if (answerField.getText().toString().length() <= answerField.getTag().toString().length()) {
                    for (int i = 0; i < answerField.getText().toString().length(); i++) {

                        if (!String.valueOf(answerField.getText().toString().charAt(i)).toLowerCase()
                                .equals(String.valueOf(answerField.getTag().toString().charAt(i)).toLowerCase())) {
                            checkWords = false;
                        }
                    }
                } else {
                    checkWords = false;
                }

                if (!checkWords) {
                    // Возвращаем видимость всех кнопок и обнуляем ответ
                    for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                        wordsFrame.getChildAt(i).setVisibility(View.VISIBLE);
                    }
                    answerField.setText("");
                }

                // Получаем нужную букву
                String trueAnswer = answerField.getTag().toString();
                String trueCharacter = String.valueOf(trueAnswer.charAt(answerField.getText().length()));

                for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                    Button but = (Button) wordsFrame.getChildAt(i);

                    if (but.getText().toString().toLowerCase().equals(trueCharacter.toLowerCase())) {
                        but.callOnClick();
                        hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) - 1));
                        Paper.book().write("hintCount",Integer.valueOf(hintCountText.getText().toString()));
                        break;
                    }
                }
            }else{
                // Подсказка для задания 13
                // Узнаём вписан ли уже правильный ответ
                if (answerField.getTag().toString().toLowerCase().equals(wordsFrame.getTag().toString().toLowerCase())) {
                    hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) - 1));
                    Paper.book().write("hintCount",Integer.valueOf(hintCountText.getText().toString()));
                    nextButton.callOnClick();
                    return;
                }

                // Узнаём правильно ли прописан текст и нужна ли подсказка
                boolean checkWords = true;

                if (wordsFrame.getTag().toString().length() <= answerField.getTag().toString().length()) {
                    for (int i = 0; i < wordsFrame.getTag().toString().length(); i++) {

                        if (!String.valueOf(wordsFrame.getTag().toString().charAt(i)).toLowerCase()
                                .equals(String.valueOf(answerField.getTag().toString().charAt(i)).toLowerCase())) {
                            checkWords = false;
                        }
                    }
                } else {
                    checkWords = false;
                }

                if (!checkWords) {
                    // Возвращаем видимость всех кнопок и обнуляем ответ
                    for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                        wordsFrame.getChildAt(i).setVisibility(View.VISIBLE);
                    }
                    while (!wordsFrame.getTag().toString().equals("")){
                        answerField.callOnClick();
                    }
                }
                // Получаем нужную букву
                String trueAnswer = answerField.getTag().toString();
                String trueCharacter = String.valueOf(trueAnswer.charAt(wordsFrame.getTag().toString().length()));

                for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                    Button but = (Button) wordsFrame.getChildAt(i);

                    if (but.getText().toString().toLowerCase().equals(trueCharacter.toLowerCase())) {
                        but.callOnClick();
                        hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) - 1));
                        Paper.book().write("hintCount",Integer.valueOf(hintCountText.getText().toString()));
                        break;
                    }
                }
            }
        }else {
            // Подсказки закончились, предложить посмотреть видос
            showAlertHint();
        }
    }

    // Запускаем таймер
    public void levelTimerStart(){
        // Запускаем таймер, который отсчитывает 5 минут для прохождения текущего уровня
        myTimer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handlerTimer.post(new Runnable() {
                    public void run() {
                        currentTime=currentTime+1;

                        // Узнаём остаток времени и переводим время в обычный формат
                        int restTime=gameTime-currentTime;

                        // Проверка на окончание времени
                        if(gameTime-currentTime<=0){
                            timeCountText.setText("0:00");
                            // Время закончилось, уровень не пройден
                            myTimer.cancel();
                            showAlertTime();
                        }else {
                            String seconds = String.valueOf(restTime % 60);
                            if (seconds.length() == 1) {
                                seconds = "0" + seconds;
                            }
                            timeCountText.setText(String.valueOf(restTime / 60) + ":" + seconds);
                        }

                    }
                });
            }};

        myTimer.schedule(timerTask, 0, 1000);
    }

    // Прописываем появление AlertDialog
    // Обработка и появление диалога про окнчание жизней
    public void showAlertLives(){
        final Dialog openDialog = new Dialog(this);
        openDialog.setContentView(R.layout.custom_alertdialog_lives);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        openDialog.setCancelable(false);
        openDialog.show();
        myTimer.cancel();

        // Оптимизация диалога
        Optimization optimization=new Optimization();
        optimization.Optimization((FrameLayout)openDialog.findViewById(R.id.frame_alert_lives));

        // Получение жизни и показ рекламы
        ImageView btnGetLives = (ImageView) openDialog.findViewById(R.id.btn_get_live);
        btnGetLives.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Проверяем, не использовано ли максимальное кол-во жизней
                if(maxLives==2){
                    createToast("Вы использовали все дополнительные жизни");
                    return;
                }

                // Показываем рекламный ролик для получения жизни
                if(showAds.mInterstitialAd.isLoaded()){
                    showAds.mInterstitialAd.show();
                    openDialog.dismiss();
                    levelTimerStart();
                    lives = 1;
                    livesText.setText("x1");
                    maxLives=maxLives+1;
                }else{
                    createToast("В данный момент нет рекламы для просмотра");
                }

            }
        });
    }

    // Обработка и появления диалога про окончание подсказок
    public void showAlertHint(){
        final Dialog openDialog = new Dialog(this);
        openDialog.setContentView(R.layout.custom_alertdialog_hints);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        openDialog.setCancelable(false);
        openDialog.show();
        myTimer.cancel();

        // Оптимизация диалога
        Optimization optimization=new Optimization();
        optimization.Optimization((FrameLayout)openDialog.findViewById(R.id.frame_alert_hints));

        // Получение 3 подсказок и показ рекламы
        ImageView btnGetHints = (ImageView) openDialog.findViewById(R.id.btn_get_hint);
        btnGetHints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Показываем рекламный ролик для получения подсказок
                if(showAds.mInterstitialAd.isLoaded()){
                    showAds.mInterstitialAd.show();
                    openDialog.dismiss();
                    levelTimerStart();
                    hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) + 3));
                    Paper.book().write("hintCount",Integer.valueOf(hintCountText.getText().toString()));
                }else{
                    createToast("В данный момент нет рекламы для просмотра");
                }
            }
        });

        // Закрываем данный экран
        Button btnClose = (Button) openDialog.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.dismiss();
                levelTimerStart();
            }
        });
    }

    // Обработка появления диалога про окончание времени
    public void showAlertTime(){
        final Dialog openDialog = new Dialog(this);
        openDialog.setContentView(R.layout.custom_alertdialog_time);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        openDialog.setCancelable(false);
        openDialog.show();

        // Оптимизация диалога
        Optimization optimization=new Optimization();
        optimization.Optimization((FrameLayout)openDialog.findViewById(R.id.frame_alert_time));

        // Получение дополнительной минуты времени
        ImageView btnGetTime = (ImageView) openDialog.findViewById(R.id.btn_get_time);
        btnGetTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Показываем рекламный ролик для получения дополнительной минуты
                if(showAds.mInterstitialAd.isLoaded()){
                    showAds.mInterstitialAd.show();
                    openDialog.dismiss();
                    currentTime = currentTime - 60;
                    levelTimerStart();
                }else{
                    createToast("В данный момент нет рекламы для просмотра");
                }
            }
        });
    }

    // Переход на главный экран
    public void backToMainScreen(View v){
        confirmExitDialog("main");
    }

    // Перезапуск текущего уровня
    public void restartLevel(View v){
        confirmExitDialog("restart");
    }

    // Переопределяем кнопку назад
    @Override
    public void onBackPressed() {
        confirmExitDialog("back");
    }

    // Диалог о подтверждении о выходе с текущего уровня
    public void confirmExitDialog(final String type){

        String alertMessage="Вы хотите покинуть данный уровень?";
        if(type.equals("restart")){
            alertMessage="Вы хотите начать уровень заново?";
        }
        // Выдаём диалог об окончании уровня
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Внимание!")
                .setMessage(alertMessage)
                .setIcon(R.drawable.main_screen_logo)
                .setCancelable(false)
                .setPositiveButton("Да",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                switch (type){
                                    case "back":
                                        dialog.dismiss();
                                        finish();
                                        break;
                                    case "main":
                                        dialog.dismiss();
                                        startActivity(new Intent(UnCommonLevel.this,MainActivity.class)
                                                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                        finish();
                                        break;
                                    case "restart":
                                        dialog.dismiss();
                                        startActivity(new Intent(UnCommonLevel.this, UnCommonLevel.class)
                                                .putExtra("currentLvl",currentIntentLvl)
                                                .putExtra("currentWorld",currentIntentWorld));
                                        finish();
                                        break;

                                }
                            }
                        })
                .setNegativeButton("Нет",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    // Обработка звуков правильного / не правильного ответов
    public void answerSound(boolean isCorrect){
        if(soundOn) {

            if (isCorrect) {
                answerTruePlayer.start();
            }else{
                answerFalsePlayer.start();
            }
        }
    }

    // Конструктор Тоаст сообщений
    public void createToast(String text){
        Toast.makeText(this, text,
                Toast.LENGTH_SHORT).show();
    }

    public void onPause(){
        super.onPause();
    }

    public void onDestroy(){
        //clearStaticVar();
        handler.removeCallbacksAndMessages(null);
        handlerTimer.removeCallbacksAndMessages(null);
        handlerRound.removeCallbacksAndMessages(null);
        if(myTimer!=null){
            myTimer.cancel();
        }
        if(tts !=null){
            tts.stop();
            //EmisterApp.tts.shutdown();
        }
        super.onDestroy();
    }

    // Обнуляем статические переменные
    public void clearStaticVar(){
        currentIntentWorld=null;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
