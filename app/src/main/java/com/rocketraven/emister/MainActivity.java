package com.rocketraven.emister;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rocketraven.emister.objects.AllIcons;
import com.rocketraven.emister.objects.AuthObject;
import com.rocketraven.emister.objects.UserMainObject;
import com.rocketraven.emister.optimization.Optimization;
import com.rocketraven.emister.retrofit.RetrofitRequestCallback;
import com.rocketraven.emister.retrofit.requests.PostUpdateUserInfoRequest;
import com.rocketraven.emister.retrofit.requests.RetrofitMainClass;
import com.rocketraven.emister.service.BackgroundMainTheme;
import com.rocketraven.emister.tutorial.CustomPresentationPagerFragment;
import com.rocketraven.emister.util.IabHelper;
import com.rocketraven.emister.util.IabResult;
import com.rocketraven.emister.util.Inventory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static io.paperdb.Paper.book;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener,
        View.OnClickListener {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.container) FrameLayout tutorialContainer;
    @BindView(R.id.settings_frame) FrameLayout settingsFrame;
    @BindView(R.id.frame_everyday_tasks) FrameLayout everyDayTasksFrame;
    // LinearLayout
    @BindView(R.id.linear_task1) LinearLayout frameTask1;
    @BindView(R.id.linear_task2) LinearLayout frameTask2;
    @BindView(R.id.linear_task3) LinearLayout frameTask3;
    // ImageView
    @BindView(R.id.emister_sprite) ImageView emisterAnimation;
    @BindView(R.id.tournament_icon) ImageView tournamentIcon;
    @BindView(R.id.premium_check) ImageView premiumCheck;
    @BindView(R.id.image_task1) ImageView imgTask1;
    @BindView(R.id.image_task2) ImageView imgTask2;
    @BindView(R.id.image_task3) ImageView imgTask3;
    ImageView lastImage=null;
    // TextView
    @BindView(R.id.img_task_flag) TextView taskFlag;
    @BindView(R.id.tournament_name) TextView tournamentName;
    @BindView(R.id.exit_account_text) TextView exitAccountText;
    @BindView(R.id.info_disable) TextView infoDisableText;
    @BindView(R.id.premium_acc_text) TextView premiumAccountText;
    @BindView(R.id.text_task1) TextView textTask1;
    @BindView(R.id.text_task2) TextView textTask2;
    @BindView(R.id.text_task3) TextView textTask3;
    @BindView(R.id.btn_task1) TextView btnTask1;
    @BindView(R.id.btn_task2) TextView btnTask2;
    @BindView(R.id.btn_task3) TextView btnTask3;
    // Switch
    @BindView(R.id.switch_sound) SwitchCompat switchSound;
    @BindView(R.id.switch_push) SwitchCompat switchPush;
    // Примитивы
    RetrofitMainClass retrofitMainClass;
    PostUpdateUserInfoRequest postUpdateUserInfoRequest;
    String token;
    IabHelper mHelper;
    final String SKU_PREMIUM1 = "premium3";
    final String SKU_PREMIUM3 = "premium6";
    final String SKU_PREMIUM12 = "premium12";
    final String SKU_PROMO_CODE = "promocode";
    ArrayList<ImageView> allTaskImage=new ArrayList();
    ArrayList<TextView> allTextImage=new ArrayList();
    ArrayList<TextView> allDoneBtn=new ArrayList();
    List<String> additionalSkuList = new ArrayList();
    int currentEverydayTasks[];
    public static boolean backgroundSoundOn=false;
    public static final String TOURNAMENT_TOP_PLAYER="top_players";
    ArrayList<ImageView> allImage=new ArrayList();
    int wordsBDVersion = 1;
    Integer allIconScope[];
    int tenPxHeight,height,tenPxWidth,width;
    boolean premiumUser;
    boolean soundOn;
    boolean nextScreen=false;
    boolean newTasks=false;
    BroadcastReceiver mBroadcastReceiver;
    BroadcastReceiver mReceiver;
    String allWorldsName[]={"world1","world2","world3","world4","world5","world6","world7","world8",
            "world_food","world_travel","world_hotel","world_shopping"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

        height= book().read("height");
        tenPxHeight=height/192;
        width= book().read("width");
        tenPxWidth=width/108;

        // Подключаем ретрофит
        retrofitMainClass=new RetrofitMainClass(this);
        postUpdateUserInfoRequest=retrofitMainClass.retrofit.create(PostUpdateUserInfoRequest.class);

        // Получаем токен
        token=Paper.book().read("token","");

        // Оптимизация интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);
        optimization.OptimizationLinear(frameTask1);
        optimization.OptimizationLinear(frameTask2);
        optimization.OptimizationLinear(frameTask3);

        // Получаем все иконки аватарок
        AllIcons allIcons=new AllIcons();
        allIconScope=allIcons.getIcons();

        // Получаем инфу о юзере
        UserMainObject userMainObject=Paper.book("user").read("mainInfo", new UserMainObject());

        // Если у юзера есть премиум, то записываем это в базу и не проверяем премиум с гугл плея
        if(userMainObject.isPremium==1){
            Paper.book("inapp").write("premium",true);
            premiumUser=true;
            premiumCheck.setImageResource(R.drawable.ic_check_on);
            premiumCheck.setClickable(false);
        }else{
            // Проверка на наличие премиума из базы
            //Paper.book("inapp").write("premium",true);
            premiumUser= Paper.book("inapp").read("premium",false);
            if(premiumUser){
                premiumCheck.setImageResource(R.drawable.ic_check_on);
                premiumCheck.setClickable(false);
            }

            // Наличие премиума с гугл плей
            checkPremiumUser();
        }

        // Получаем имя и иконку пользователя
        tournamentName.setText(userMainObject.userName);
        tournamentIcon.setImageResource(allIconScope[userMainObject.userPic]);

        //Register receivers for push notifications
        registerReceivers();

        // Определяем параметры звука
        soundOn= book("settings").read("sound",true);

        // Запускаем сервис для фоновой музыки
        if(soundOn) {
            startService(new Intent(this, BackgroundMainTheme.class));
            backgroundSoundOn=true;
        }

        // Проверяем вкл пушей и звуков
        if(book("settings").read("sound",true)){
            switchSound.setChecked(true);
        }else{
            switchSound.setChecked(false);
        }

        if(book("settings").read("push",true)){
            switchPush.setChecked(true);
        }else{
            switchPush.setChecked(false);
        }

        // Проверяем наличие премиум аккаунта
        //premiumUser= book("inapp").read("premium",false);


        // Проверка на заполнение базы словами (Обернуть в проверку)
        AllWordsTrain allWordsTrain = new AllWordsTrain();
        allWordsTrain.putWordsBD();

        // Определяем текущий день
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(date.getTime());

        // Активация туториала припервом заходе в приложение
        if(Paper.book().read("firstLaunch",false)) {

            // Скрываем статус бар
            hideStatusBar();

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setBreadCrumbShortTitle("Далее");
            fragmentTransaction.replace(R.id.container, new CustomPresentationPagerFragment());
            fragmentTransaction.commit();
            Paper.book().write("firstLaunch", false);
            Paper.book().write("firstLaunchTime", calendar.getTimeInMillis());
        }

        // Добавляем премиуму 5 подсказок каждый день
        if (Paper.book("inapp").read("premium", false)) {
            int hintCount = Paper.book().read("hintCount", 10);
            if (!currentDate.equals(Paper.book().read("currentDay", "0"))) {
                if (hintCount + 5 <= 30) {
                    Paper.book().write("hintCount", hintCount + 5);
                }
            }
        }

        // Объеденяем части ежедневных заданий в лист
        allTaskImage.add(imgTask1); allTaskImage.add(imgTask2); allTaskImage.add(imgTask3);
        allTextImage.add(textTask1); allTextImage.add(textTask2); allTextImage.add(textTask3);
        allDoneBtn.add(btnTask1); allDoneBtn.add(btnTask2); allDoneBtn.add(btnTask3);

        // Проверяем новые задания
        if(!currentDate.equals(book().read("currentDay", "0"))){

            // Рандомим текущие задания на сегодня и помечаем иконкой алерта
            currentEverydayTasks=getEverydayTasks();
            taskFlag.setBackgroundResource(R.drawable.img_flag_active);
            newTasks=true;

            // Сохраняем в базу
            book("everyday").write("tasks",currentEverydayTasks);

            // Удаляём старые прохождения
            int defaultTasks[]={0,0,0};
            book("everyday").write("done",defaultTasks);

        }else{
            currentEverydayTasks= book("everyday").read("tasks",getEverydayTasks());
        }

        // Меняем UI в ежедневных заданиях
        changeEverydayUI();

        // Проверяем выполнение заданий
        checkEverydayTasks();

        // Обновляем рейтинговые игры
        if (!currentDate.equals(book().read("currentDay", "0"))) {
            book("tournament").write("rate_count", 3);
            book().write("currentDay", currentDate);
        }

        // Обработка Свича на звуки
        switchPush.setOnCheckedChangeListener(this);
        switchSound.setOnCheckedChangeListener(this);

        // Обновляем прогресс пользователя
        UserProgress userProgress=new UserProgress(this);
        userProgress.getServerProgress();

        // Событие - Начало сессии
        metricaEvent();

        // Проверка разрешений Facebook
        if(Paper.book().read("loginType","").equals("fb")){
            facebookPerms();
        }

    }

    // Переход на экран бота
    public void botScreenClick(View v){
        startActivity(new Intent(this,BotMainScreenActivity.class));
    }

    // Проврка разрешений фейсбук
    private void facebookPerms(){

    }

    // Регистрируем Броадкасты для пушей
    public void registerReceivers(){

    }

    public void unregisterReceivers(){
        //Unregister receivers on pause
        try
        {
            unregisterReceiver(mReceiver);
        }
        catch (Exception e){}
        try
        {
            unregisterReceiver(mBroadcastReceiver);
        }
        catch (Exception e){}
    }

    // Видимость фрейма ежедневных заданий
    public void everydayTasksClick(View v){
        if(everyDayTasksFrame.getVisibility()==View.GONE) {
            everyDayTasksFrame.setVisibility(View.VISIBLE);

            if(newTasks){
                taskFlag.setBackgroundResource(R.drawable.img_flag);
            }
        }else{
            everyDayTasksFrame.setVisibility(View.GONE);
        }
    }

    // Переход на экран выбора приключения
    public void startTest(View v) {
        startActivity(new Intent(this, TopicalListActivity.class)); // startActivity(new Intent(this, AdventureListActivity.class));
        nextScreen=true;
    }

    // Выход из текущего аккаунта
    public void accountExitClick(View v){
        Paper.book("inapp").delete("premium");
        Paper.book("user").delete("mainInfo");
        Paper.book().delete("trainWords");
        Paper.book().delete("saveGrama");
        Paper.book().delete("token");
        Paper.book().delete("loginType");
        Paper.book().delete("loginInfo");

        for (int i=0;i<allWorldsName.length;i++){
            Paper.book(allWorldsName[i]).delete("progress");
        }

        startActivity(new Intent(this,StartScreen.class));
        finish();
    }

    // Переход к вводу промокода
    public void promocodeEnterClick(View v){
        startActivity(new Intent(this,PromoCodeActivity.class));
        finish();
    }

    // Переход на экран турнира
    public void tournamentClick(View v) {

        // Проверка интернета
        if(checkInternet()) {
            startActivity(new Intent(this, TournamentMainScreen.class));
            nextScreen = true;
        }else {
            createToast("Отсутствует интернет соединение");
        }
    }

    // Переход на экран тренировок
    public void trainClick(View v)
    {
        startActivity(new Intent(this, TrainScreenActivity.class));
        nextScreen = true;
    }

    // Переход на экран покупок
    public void shopClick(View v) {
        startActivity(new Intent(this, ShopScreenActivity.class)
                .putExtra("rating", 0));
        nextScreen=true;
    }

    // Переход в магазин из настроек
    public void shopClickSettings(View v){
        startActivity(new Intent(this,ShopScreenActivity.class));
        nextScreen=true;
    }

    // Появления окна настроек
    public void settingsClick(View v) {
        if(settingsFrame.getVisibility()==View.GONE){
            settingsFrame.setVisibility(View.VISIBLE);
        }else{
            settingsFrame.setVisibility(View.GONE);
        }
    }

    // Обработка переключения свичей
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switch_sound:
                // Вкл/Откл звуки
                book("settings").write("sound",isChecked);
                if(isChecked){
                    startService(new Intent(this, BackgroundMainTheme.class));
                    backgroundSoundOn=true;
                    soundOn=true;
                }else{
                    stopService(new Intent(this, BackgroundMainTheme.class));
                    backgroundSoundOn=false;
                    soundOn=false;
                }
                break;
            case R.id.switch_push:
                // Вкл/откл пуши и нотификации
                book("settings").write("push",isChecked);
                if(isChecked){
                }else{
                }
                break;
        }
    }

    // Обработка изменения иконки аккаунта
    public void changeAccountIcon(View v){

        // Проверяем наличие интернет соединения
        if (checkInternetConnection()) {
            final Dialog openDialog = new Dialog(this);
            openDialog.setContentView(R.layout.custom_alertdialog_changeicon);
            openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            openDialog.setCancelable(true);
            openDialog.show();

            // Оптимизация диалога
            Optimization optimization = new Optimization();
            optimization.Optimization((FrameLayout) openDialog.findViewById(R.id.frame_alert_icons));

            // Добавляем иконки на диалог
            FrameLayout iconsFrame = (FrameLayout) openDialog.findViewById(R.id.icons_frame);
            createFaceIcons(iconsFrame);

            // Обработка клика при выборе нужной иконки
            Button nextButton = (Button) openDialog.findViewById(R.id.next_button);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeIconInBdD();
                    openDialog.dismiss();
                }
            });
        }else{
            createToast("Отсутствует соединение с интернетом");
        }
    }

    // Заполняем фрейм иконками для аккаунта
    public void createFaceIcons(FrameLayout iconsFrame){

        int table=0;
        int raw=0;
        int premium=0;

        // Расстояние между иконками
        int limit=(width-tenPxWidth*60)/5;

        for (int i=0;i<allIconScope.length;i++){

            ImageView icon=new ImageView(this);

            FrameLayout.LayoutParams iconParams=new FrameLayout.LayoutParams(tenPxWidth*15,tenPxWidth*15);
            iconParams.setMargins(limit+(tenPxWidth*15+limit)*table,
                    premium*tenPxHeight*5+tenPxHeight*2+(tenPxHeight*10+limit)*raw,0,0);
            icon.setBackgroundResource(allIconScope[i]);
            iconsFrame.addView(icon,iconParams);
            icon.setOnClickListener(this);
            icon.setTag(i);

            allImage.add(icon);

            table=table+1;
            if(table==4){
                raw=raw+1;
                table=0;
            }

            if(raw==5){
                premium=1;
                ImageView line=new ImageView(this);
                FrameLayout.LayoutParams lineParams=new FrameLayout.LayoutParams(width-width/10,1);
                lineParams.gravity= Gravity.CENTER_HORIZONTAL;
                lineParams.setMargins(0,(tenPxHeight*10+limit)*raw +tenPxHeight*2,0,0);
                line.setBackgroundColor(Color.parseColor("#ffffff"));
                iconsFrame.addView(line,lineParams);
            }
        }

        lastImage=allImage.get(0);
        lastImage.setScaleY(1.2f);
        lastImage.setScaleX(1.2f);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    // Клик при выборе новой иконки
    @Override
    public void onClick(View v) {
            lastImage.setScaleY(1);
            lastImage.setScaleX(1);

            v.setScaleY(1.2f);
            v.setScaleX(1.2f);
            Log.d("Таги",lastImage.getTag().toString()+" "+v.getTag().toString());
            lastImage = (ImageView) v;
    }

    // Отправляем на сервер изменения по новой иконке
    public void changeIconInBdD(){
        Call<AuthObject> changeIconRequest =
                postUpdateUserInfoRequest.updateUserIcon(token,Integer.valueOf(lastImage.getTag().toString()));

        retrofitMainClass.createRequest(changeIconRequest);
        retrofitMainClass.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object response) {
                Response<AuthObject> currentResponse=(Response<AuthObject>)response;

                if(currentResponse.code()==200){
                    AuthObject authObject=currentResponse.body();

                    if(authObject.userMainObject!=null){
                        Paper.book("user").write("mainInfo",authObject.userMainObject);
                        createToast("Иконка успешно обновлена");
                        tournamentIcon.setImageResource(allIconScope[Integer.valueOf(lastImage.getTag().toString())]);
                    }
                }
            }

            @Override
            public void onEventFailure(Throwable error) {

            }
        });
    }

    // Выбор случайного пакета заданий
    public int[] getEverydayTasks(){
        int tasks[]=new int[3];
        Random rand=new Random();

        switch (rand.nextInt(6)){
            case 0: tasks[0]=1; tasks[1]=2;tasks[2]=5;
                break;
            case 1: tasks[0]=6; tasks[1]=2;tasks[2]=1;
                break;
            case 2: tasks[0]=7; tasks[1]=1;tasks[2]=4;
                break;
            case 3: tasks[0]=1; tasks[1]=4;tasks[2]=6;
                break;
            case 4: tasks[0]=2; tasks[1]=1;tasks[2]=5;
                break;
            case 5: tasks[0]=1; tasks[1]=7;tasks[2]=2;
                break;
            case 6: tasks[0]=6; tasks[1]=1;tasks[2]=4;
                break;
        }

        return tasks;
    }

    // Меняе информацию во фрейме ежедневных заданий
    public void changeEverydayUI(){

        // Меняем иконки и тексты в зависимости от типа задания
        for (int i=0;i<currentEverydayTasks.length;i++){
            switch (currentEverydayTasks[i]){
                case 1:
                    allTaskImage.get(i).setImageResource(R.drawable.ic_tutorial_adventure);
                    allTextImage.get(i).setText("Изучи любой урок в приключениях");
                    break;
                case 2:
                    allTaskImage.get(i).setImageResource(R.drawable.ic_tutoriak_train);
                    allTextImage.get(i).setText("Пройди любую тренировку слов");
                    break;
                case 3:
                    allTaskImage.get(i).setImageResource(R.drawable.ic_tutoriak_train);
                    allTextImage.get(i).setText("Пройди тренировку грамматики");
                    break;
                case 4:
                    allTaskImage.get(i).setImageResource(R.drawable.ic_tutorial_tournament);
                    allTextImage.get(i).setText("Займи первое место в турнире");
                    break;
                case 5:
                    allTaskImage.get(i).setImageResource(R.drawable.ic_tutorial_tournament);
                    allTextImage.get(i).setText("Прими участие в любом турнире");
                    break;
                case 6:
                    allTaskImage.get(i).setImageResource(R.drawable.ic_tutorial_tournament);
                    allTextImage.get(i).setText("Прими участие в рейтинговом турнире");
                    break;
                case 7:
                    allTaskImage.get(i).setImageResource(R.drawable.ic_tutorial_tournament);
                    allTextImage.get(i).setText("Прими участие в простом турнире");
                    break;
            }
        }
    }

    // Проверка на пройденные ежедневные задания
    public void checkEverydayTasks(){
        int defaultTasks[]={0,0,0};
        int doneTasks[]= book("everyday").read("done",defaultTasks);

        for (int i=0;i<doneTasks.length;i++){
            if(doneTasks[i]==1){
                allDoneBtn.get(i).setBackgroundResource(R.drawable.shape_white_stroke_fill);
                allDoneBtn.get(i).setText("Выполнено");
                allDoneBtn.get(i).setTextColor(Color.parseColor("#0272BC"));
            }
        }
    }

    // Проверка интернет соединения
    public boolean checkInternet(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    // Подсказка при первом входе
    public void firstEnterHint(){

        // Показываем статус бар
        showStatusBar();

    }

    // Проверка наличия премиум-аккаунта
    public void checkPremiumUser(){
        mHelper = new IabHelper(this, getResources().getString(R.string.billing_key));

        // Формируем лист покупок
        additionalSkuList.add(SKU_PREMIUM1);
        additionalSkuList.add(SKU_PREMIUM3);
        additionalSkuList.add(SKU_PREMIUM12);
        additionalSkuList.add(SKU_PROMO_CODE);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(false);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d("Setup", "Problem setting up In-app Billing: " + result);
                }
                try {
                    mHelper.queryInventoryAsync(true,additionalSkuList,mGotInventoryListener);
                    Log.d("Setup", "No problem " + result);
                }catch (Exception e){}

            }
        });
    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // handle error here
            }
            else {
                // 3 месяца
                boolean isUserPremium=false;
                boolean premium1Buy = inventory.hasPurchase(SKU_PREMIUM1);
                if(premium1Buy){
                    isUserPremium=true;
                    Log.d("Премиум 1 месяц","КУПЛЕНА");
                    premiumCheck.setImageResource(R.drawable.ic_check_on);
                }else{
                    Log.d("Премиум 1 месяц","НЕ КУПЛЕНА");
                }

                // Вторая секция
                boolean premium2Buy = inventory.hasPurchase(SKU_PREMIUM3);
                if(premium2Buy){
                    isUserPremium=true;
                    Log.d("Премиум 3 месяца","КУПЛЕНА");
                    premiumCheck.setImageResource(R.drawable.ic_check_on);
                }else{
                    Log.d("Премиум 3 месяца","НЕ КУПЛЕНА");
                }

                // Третья секция
                boolean premium3Buy = inventory.hasPurchase(SKU_PREMIUM12);
                if(premium3Buy){
                    isUserPremium=true;
                    Log.d("Премиум 12 месяцев","КУПЛЕНА");
                    premiumCheck.setImageResource(R.drawable.ic_check_on);
                }else{
                    Log.d("Премиум 12 месяцев","НЕ КУПЛЕНА");
                }

                // Промо Код
                boolean promoCodeBuy = inventory.hasPurchase(SKU_PROMO_CODE);
                if(promoCodeBuy){
                    isUserPremium=true;
                    Log.d("Промо Код","КУПЛЕН");
                    premiumCheck.setImageResource(R.drawable.ic_check_on);
                }else{
                    Log.d("Промо Код","НЕ КУПЛЕН");
                }
                Paper.book("inapp").write("premium",isUserPremium);

            }
        }
    };

    // Создание тоастов
    public void createToast(String text){
        Toast.makeText(this, text,
                Toast.LENGTH_SHORT).show();
    }

    // Проверка наличия интернет соединения
    public boolean checkInternetConnection(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(!nextScreen&&soundOn){
            stopService(new Intent(this, BackgroundMainTheme.class));
            backgroundSoundOn=false;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(book("inapp").read("premium",false)){
            premiumCheck.setImageResource(R.drawable.ic_check_on);
        }
        nextScreen=false;
        checkEverydayTasks();
        if(!backgroundSoundOn&&soundOn) {
            startService(new Intent(this, BackgroundMainTheme.class));
            backgroundSoundOn=true;
        }
    }

    // Переопределяем кнопку назад
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(backgroundSoundOn&&soundOn){
            // Останавливаем фоновую музыку
            stopService(new Intent(this, BackgroundMainTheme.class));
            backgroundSoundOn=false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, BackgroundMainTheme.class));
    }

    public void hideStatusBar(){
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    public void showStatusBar(){
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            // Show Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }


    // Статистика - Начало сессии
    public void metricaEvent(){
    }

}
