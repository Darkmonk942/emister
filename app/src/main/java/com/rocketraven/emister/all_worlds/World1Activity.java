package com.rocketraven.emister.all_worlds;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rocketraven.emister.CommonLevel;
import com.rocketraven.emister.MainActivity;
import com.rocketraven.emister.R;
import com.rocketraven.emister.UnCommonLevel;
import com.rocketraven.emister.objects.WorldObject;
import com.rocketraven.emister.optimization.Optimization;
import com.rocketraven.emister.service.BackgroundMainTheme;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Darkmonk on 27.06.2016.
 */
public class World1Activity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // ImageView
    // TextView
    @BindView(R.id.lvl_1) TextView lvl1Text;
    @BindView(R.id.lvl_2) TextView lvl2Text;
    @BindView(R.id.lvl_3) TextView lvl3Text;
    @BindView(R.id.lvl_4) TextView lvl4Text;
    @BindView(R.id.lvl_5) TextView lvl5Text;
    @BindView(R.id.lvl_6) TextView lvl6Text;
    @BindView(R.id.lvl_7) TextView lvl7Text;
    @BindView(R.id.lvl_8) TextView lvl8Text;
    @BindView(R.id.lvl_9) TextView lvl9Text;
    @BindView(R.id.lvl_10) TextView lvl10Text;
    @BindView(R.id.lvl_11) TextView lvl11Text;
    @BindView(R.id.lvl_12) TextView lvl12Text;
    @BindView(R.id.lvl_13) TextView lvl13Text;
    @BindView(R.id.lvl_14) TextView lvl14Text;
    @BindView(R.id.lvl_15) TextView lvl15Text;
    @BindView(R.id.lvl_16) TextView lvl16Text;
    // Примитивы
    public static final String BASIC_LEVEL="world1";
    int currentLvl=0;
    boolean backPressed=false;
    boolean soundOn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world1);
        ButterKnife.bind(this);

        // Определяем параметры звука
        soundOn=Paper.book("settings").read("sound",true);

        // Оптимизация интерфейса
        Optimization optim=new Optimization();
        optim.Optimization(mainFrame);

        // Собираем все диалоги в массив
        TextView allLvlText[]={lvl1Text,lvl2Text,lvl3Text,lvl4Text,lvl5Text,lvl6Text,
                lvl7Text,lvl8Text,lvl9Text,lvl10Text,lvl11Text,lvl12Text,lvl13Text,
                lvl14Text,lvl15Text,lvl16Text};

        // Получаем текущий уровень
        checkCurrentProgress(allLvlText);

    }

    // Обработка кнопки назад
    public void backClick(View v){
        backPressed=true;
        finish();
    }

    // Определяем Текущий прогресс в данном приключении
    public void checkCurrentProgress(TextView masText[]) {
        Integer currentProgress[] = Paper.book(BASIC_LEVEL).read("progress", new Integer[30]);

        // Определяем прогресс всех предыдущих уровней
        for (int i = 0; i < currentProgress.length; i++) {
            if (currentProgress[i] != null) {
                currentLvl = currentLvl + 1;
                masText[i].setTextColor(getResources().getColor(R.color.colorBlue));

                switch (currentProgress[i]) {
                    case 1:
                        masText[i].setTextColor(getResources().getColor(R.color.colorTopicalBlue));
                        if((i+1)%4!=0) {
                            masText[i].setBackgroundResource(R.drawable.ic_topical_common_past1);
                        }else{
                            masText[i].setBackgroundResource(R.drawable.ic_topical_train_past1);
                        }
                        break;
                    case 2:
                        masText[i].setTextColor(getResources().getColor(R.color.colorTopicalBlue));
                        if((i+1)%4!=0) {
                            masText[i].setBackgroundResource(R.drawable.ic_topical_common_past2);
                        }else{
                            masText[i].setBackgroundResource(R.drawable.ic_topical_train_past2);
                        }
                        break;
                    case 3:
                        masText[i].setTextColor(getResources().getColor(R.color.colorTopicalBlue));
                        if((i+1)%4!=0) {
                            masText[i].setBackgroundResource(R.drawable.ic_topical_common_past3);
                        }else{
                            masText[i].setBackgroundResource(R.drawable.ic_topical_train_past3);
                        }
                        break;
                }
            }
        }

        // Проверка на прохождение всех уровней
        if(currentLvl>=16){
            return;
        }

        // Определяем текущий активный уровень
        if((currentLvl+1)%4!=0) {
            masText[currentLvl].setBackgroundResource(R.drawable.ic_topical_common_current1);
        }else{
            masText[currentLvl].setBackgroundResource(R.drawable.ic_topical_train_current);
        }
        masText[currentLvl].setTextColor(getResources().getColor(R.color.colorTopicalGreen));

    }

    // Определяем на какой уровень произойдёт переход
    public void levelClick(View v){

        // Проверка доступности уровня
        if(currentLvl>=Integer.valueOf(v.getTag().toString())) {
            // Останавливаем фоновую музыку
            stopService(new Intent(this, BackgroundMainTheme.class));

            // Метрика на начало учебного уровня
            metricaEvent("Старт уровня");

            // Проверка на тип уровня
            WorldObject worldObject=Paper.book(BASIC_LEVEL).read("world",new WorldObject());

            // Переход. Передаём текущий уровень и всю информацию
            switch (worldObject.allLevels.get(Integer.valueOf(v.getTag().toString())).levelType){

                case "common":
                    startActivity(new Intent(this, CommonLevel.class)
                            .putExtra("currentLvl", Integer.valueOf(v.getTag().toString()))
                            .putExtra("currentWorld",BASIC_LEVEL));
                    break;
                case "uncommon":
                    startActivity(new Intent(this, UnCommonLevel.class)
                            .putExtra("currentLvl", Integer.valueOf(v.getTag().toString()))
                            .putExtra("currentWorld",BASIC_LEVEL));
                    break;
                case "exam":
                    break;
            }

        }else{
            createToast("Данный уровень ещё недоступен");
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if(!MainActivity.backgroundSoundOn&&soundOn) {
            startService(new Intent(this, BackgroundMainTheme.class));
            MainActivity.backgroundSoundOn=true;
        }

        // Обнуляем текущий уровень
        currentLvl=0;

        // Собираем все диалоги в массив
        TextView allLvlText[]={lvl1Text,lvl2Text,lvl3Text,lvl4Text,lvl5Text,lvl6Text,
                lvl7Text,lvl8Text,lvl9Text,lvl10Text,lvl11Text,lvl12Text,lvl13Text,
                lvl14Text,lvl15Text,lvl16Text};

        // Получаем текущий уровень
        checkCurrentProgress(allLvlText);
    }

    // Переопределяем кнопку назад
    @Override
    public void onBackPressed() {
        backPressed=true;
        super.onBackPressed();

    }

    @Override
    public void onStop() {
        super.onStop();
        if(MainActivity.backgroundSoundOn&&!backPressed&&soundOn) {
            stopService(new Intent(this, BackgroundMainTheme.class));
            MainActivity.backgroundSoundOn=false;
        }
    }

    // Конструктор Тоаст сообщений
    public void createToast(String text){
        Toast.makeText(this, text,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    // Статистика начало прохождения уровня
    public void metricaEvent(String trainType){
        Map<String, Object> eventAttributes = new HashMap<String, Object>();
        eventAttributes.put("Adventure", trainType);
    }
}
