package com.rocketraven.emister;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.rocketraven.emister.ads.ShowAds;
import com.rocketraven.emister.objects.LevelObject;
import com.rocketraven.emister.objects.WorldObject;
import com.rocketraven.emister.optimization.Optimization;
import com.rocketraven.emister.trains.CreateLevelTasks;
import com.rocketraven.emister.trains.TrainTask1;
import com.rocketraven.emister.trains.TrainTask10;
import com.rocketraven.emister.trains.TrainTask11;
import com.rocketraven.emister.trains.TrainTask12;
import com.rocketraven.emister.trains.TrainTask13;
import com.rocketraven.emister.trains.TrainTask14;
import com.rocketraven.emister.trains.TrainTask15;
import com.rocketraven.emister.trains.TrainTask2;
import com.rocketraven.emister.trains.TrainTask3;
import com.rocketraven.emister.trains.TrainTask4;
import com.rocketraven.emister.trains.TrainTask5;
import com.rocketraven.emister.trains.TrainTask6;
import com.rocketraven.emister.trains.TrainTask7;
import com.rocketraven.emister.trains.TrainTask8;
import com.rocketraven.emister.trains.TrainTask9;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CommonLevel extends AppCompatActivity {
    // ScrollView
    @BindView(R.id.words_frame_scroll) ScrollView wordsScroll;
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.content_frame) FrameLayout contentFrame;
    @BindView(R.id.card_frame_center) FrameLayout cardFrameCenter;
    @BindView(R.id.card_frame1_back) CardView cardFrame1Back;
    @BindView(R.id.card_frame2_back) CardView cardFrame2Back;
    @BindView(R.id.card_frame3_back) CardView cardFrame3Back;
    @BindView(R.id.card_frame4_back) CardView cardFrame4Back;
    @BindView(R.id.card_frame1) CardView cardFrame1;
    @BindView(R.id.card_frame2) CardView cardFrame2;
    @BindView(R.id.card_frame3) CardView cardFrame3;
    @BindView(R.id.card_frame4) CardView cardFrame4;
    @BindView(R.id.rule_frame) FrameLayout ruleFrame;
    @BindView(R.id.words_frame) FrameLayout wordsFrame;
    @BindView(R.id.audio_buttons_frame) FrameLayout audioButtonsFrame;
    // LinearLayout
    @BindView(R.id.rule_inner_frame) LinearLayout ruleInnerFrame;
    @BindView(R.id.progress_linear) LinearLayout progresLinear;
    @BindView(R.id.left_linear_words) LinearLayout leftLinearWords;
    @BindView(R.id.right_linear_words) LinearLayout rightLinearWords;
    // TextView
    @BindView(R.id.task_description) TextView taskDescription;
    @BindView(R.id.text_eng_center) TextView textEngCenter;
    @BindView(R.id.text_rus_center) TextView textRusCenter;
    @BindView(R.id.text_eng1) TextView textEng1;
    @BindView(R.id.text_eng2) TextView textEng2;
    @BindView(R.id.text_eng3) TextView textEng3;
    @BindView(R.id.text_eng4) TextView textEng4;
    @BindView(R.id.text_rus1) TextView textRus1;
    @BindView(R.id.text_rus2) TextView textRus2;
    @BindView(R.id.text_rus3) TextView textRus3;
    @BindView(R.id.text_rus4) TextView textRus4;
    @BindView(R.id.hint_text1) TextView hintText1;
    @BindView(R.id.hint_text2) TextView hintText2;
    @BindView(R.id.hint_text3) TextView hintText3;
    @BindView(R.id.hint_text4) TextView hintText4;
    @BindView(R.id.lvl_name) TextView lvlName;
    @BindView(R.id.hint_text1_back) TextView hintText1Back;
    @BindView(R.id.hint_text2_back) TextView hintText2Back;
    @BindView(R.id.hint_text3_back) TextView hintText3Back;
    @BindView(R.id.hint_text4_back) TextView hintText4Back;
    @BindView(R.id.rule_header) TextView ruleHeader;
    @BindView(R.id.question_text) TextView questionText;
    @BindView(R.id.answer_field) TextView answerField;
    @BindView(R.id.lives_text) TextView livesText;
    @BindView(R.id.progress) TextView progressText;
    @BindView(R.id.hint_count_text) TextView hintCountText;
    @BindView(R.id.rule_true) TextView ruleTrue;
    // Button
    @BindView(R.id.help_but) Button helpButton;
    @BindView(R.id.next_button) Button nextButton;
    @BindView(R.id.answer1) Button answer1;
    @BindView(R.id.answer2) Button answer2;
    @BindView(R.id.answer3) Button answer3;
    @BindView(R.id.answer4) Button answer4;
    @BindView(R.id.answer_sound1) Button answerSound1;
    @BindView(R.id.answer_sound2) Button answerSound2;
    @BindView(R.id.answer_sound3) Button answerSound3;
    @BindView(R.id.answer_sound4) Button answerSound4;
    @BindView(R.id.question_sound) Button questionSound;
    Button lastSound=null;
    // ImageView
    @BindView(R.id.btn_delete) ImageView deleteSymbol;
    @BindView(R.id.sound_icon_center) ImageView soundIconCenter;
    @BindView(R.id.sound_icon1) ImageView soundIcon1;
    @BindView(R.id.sound_icon2) ImageView soundIcon2;
    @BindView(R.id.sound_icon3) ImageView soundIcon3;
    @BindView(R.id.sound_icon4) ImageView soundIcon4;
    @BindView(R.id.hint_icon1) ImageView hintIcon1;
    @BindView(R.id.hint_icon2) ImageView hintIcon2;
    @BindView(R.id.hint_icon3) ImageView hintIcon3;
    @BindView(R.id.hint_icon4) ImageView hintIcon4;
    @BindView(R.id.hint_icon1_back) ImageView hintIcon1Back;
    @BindView(R.id.hint_icon2_back) ImageView hintIcon2Back;
    @BindView(R.id.hint_icon3_back) ImageView hintIcon3Back;
    @BindView(R.id.hint_icon4_back) ImageView hintIcon4Back;
    // Примитивы
    TextToSpeech tts;
    MediaPlayer answerFalsePlayer;
    MediaPlayer answerTruePlayer;
    private RewardedVideoAd mAd;
    ShowAds showAds;
    public static String lvlType="common";
    public static String currentIntentWorld;
    public static int currentIntentLvl;
    public static String currentLvlName;
    int maxLives=0;
    HashMap<String, String> myHashAlarm = new HashMap<String, String>();
    public static HashMap<String,Integer> wordsPackMap=new HashMap<String, Integer>();
    ArrayList <Integer> allTasks;
    ArrayList<TextView> allEngCardText=new ArrayList();
    ArrayList<TextView> allRusCardText=new ArrayList();
    ArrayList<TextView> allHintCardText=new ArrayList();
    ArrayList<TextView> allHintBack=new ArrayList<>();
    ArrayList<ImageView> allHintCardIcon=new ArrayList();
    ArrayList<ImageView> allSoundIcon=new ArrayList();
    ArrayList<Button> allAnswers=new ArrayList();
    ArrayList<Button> allAnswersSound=new ArrayList();
    ArrayList<ImageView> allHintIconBack=new ArrayList();
    Handler handler=new Handler();
    int progressCellWidth,fivePxWidth;
    int currentTask=0;
    LevelObject levelObject;
    public static boolean currentTaskAnswer=false;
    public static int lives=3; // 3
    boolean showCenterFrame=false;
    public static int levelRating=0;
    private boolean cardVisible1 = false;
    private boolean cardVisible2 = false;
    private boolean cardVisible3 = false;
    private boolean cardVisible4 = false;
    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;
    boolean premiumUser;
    boolean endOfTrainGrama=false;
    boolean gramaType=false;
    boolean soundOn;
    boolean nextButtonAvailable=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_level);
        ButterKnife.bind(this);

        // Обнуляем все статические переменные
        wordsPackMap.clear();
        lvlType="common";
        levelRating=0;

        // Подгружаем Синтезатор голоса
        tts=EmisterApp.tts;

        // Определяем параметры звука
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        soundOn=Paper.book("settings").read("sound",true);
        answerFalsePlayer =MediaPlayer.create(this,R.raw.sound_answer_false);
        answerTruePlayer = MediaPlayer.create(this,R.raw.sound_answer_true);

        // Проверка на премиум
        premiumUser=Paper.book("inapp").read("premium",false);

            // Initialize the Mobile Ads SDK.
            //mAd = MobileAds.getRewardedVideoAdInstance(this);
            //showAds = new ShowAds();
            //showAds.rewardedVideo(mAd, getResources().getString(R.string.admob_rewarded_video_key));
        showAds = new ShowAds();
        showAds.loadInterstitial(this, getResources().getString(R.string.admob_video_key));
        showAds.mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {

            }
        });

        if(premiumUser){
            // Плюшки премиум пользователя
            lives=5;
            livesText.setText("x5");
        }else{
            lives=3;
        }

        // Получаем Кол-во подсказок, доступных пользоателю
        hintCountText.setText(String.valueOf(Paper.book().read("hintCount",10)));

        // Оптимизация интерфейса
        optimizationUI();

        // Определяем ширину прогресса
        int width=Paper.book().read("width");
        fivePxWidth=width/216;
        progressCellWidth=fivePxWidth*3;

        // Собираем в массивы элементы UI
        createUIList();

        // Цвета для взрывов жизней
        int colors[]={R.color.colorAccent,R.color.colorRed};

        // Анимации карточек
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);

        CreateLevelTasks createLevelTasks=new CreateLevelTasks();
        // Проверка на тренировку слов или приключение
        if(getIntent().getStringExtra("currentWorld").equals("train")){

            String currentTrainType;
            currentTrainType=getIntent().getStringExtra("trainType");

            if(currentTrainType==null){
                currentTrainType="words";
            }
            // Тренировка слов
            if(currentTrainType.equals("words")) {
                // Получаем набор из 20 заданий
                allTasks = createLevelTasks.createWordsTrainTasks();
                progressCellWidth=fivePxWidth*6;

                lvlName.setText("Тренировка слов");
                lvlType = "train";
                progressText.setText("1 из 15");
                LevelObject trainLvlObject = new LevelObject();
                trainLvlObject.levelType = "train";
                trainLvlObject.levelName = "Тренировка слов";
                trainLvlObject.levelWordsEng = getEngWords();
                trainLvlObject.levelWordsRus = getRusWords();

                levelObject = trainLvlObject;
            }else{
            // Тренировка грамматики
                allTasks = createLevelTasks.createGramaTrainTasks();
                progressCellWidth=fivePxWidth*11;
                gramaType=true;
                ruleTrue.setVisibility(View.VISIBLE);
                lvlName.setText("Тренировка грамматики");
                lvlType = "train";
                progressText.setText("1 из 8");
                levelObject=new LevelObject();
                levelObject.levelRuleTrain =getIntent().getStringExtra("json");
            }
            hideUi();
        }else {
            // Получем набор из 30 заданий
            allTasks= createLevelTasks.createTasks();

            // Получаем все данные текущего мира
            WorldObject currentWorld = Paper.book(getIntent().getStringExtra("currentWorld")).read("world");
            currentIntentWorld = getIntent().getStringExtra("currentWorld");
            levelObject = currentWorld.allLevels.get(getIntent().getIntExtra("currentLvl", 0));
            currentIntentLvl = getIntent().getIntExtra("currentLvl", 0);
            lvlName.setText(currentWorld.allLevels.get(getIntent().getIntExtra("currentLvl", 0)).levelName);
            currentLvlName=currentWorld.allLevels.get(getIntent().getIntExtra("currentLvl", 0)).levelName;
        }

        // Заполняем HashMap текущим начальным набором слов
        addAllword();

        // Создаём стартовый интерфейс
        createUI(allTasks.get(currentTask));

        // Контейнер параметров для TTS
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "utterance_id_1");

    }

    // Вызов меню
    public void menuClick(View v){
        mainMenuDialog();
    }

    // Создаём интерфейс текущего уровня
    public void createUI(Integer taskType){

        // Проверка на окончание тренировки слов
        if(lvlType.equals("train")) {
            if(getIntent().getStringExtra("trainType").equals("words")) {
                if (currentTask == 14) {
                    Intent myIntent = new Intent(this, EndOfLevelActivity.class)
                            .putExtra("rating", levelRating)
                            .putExtra("currentLvl", currentIntentLvl)
                            .putExtra("currentWorld", currentIntentWorld)
                            .putExtra("lvlType", "train");
                    startActivity(myIntent);
                    finish();
                }
            }else{
                if (currentTask == 9) {
                    Intent myIntent = new Intent(this, EndOfLevelActivity.class)
                            .putExtra("rating", levelRating)
                            .putExtra("currentLvl", currentIntentLvl)
                            .putExtra("currentWorld", currentIntentWorld)
                            .putExtra("lvlType", "train");
                    startActivity(myIntent);
                    finish();
                }
            }
        }

        switch (taskType){
            // Показываем для обучения 3 карточки
            case 1:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Изучаем новые слова");
                TrainTask1.trainTask1(currentTask,levelObject.levelWordsEng,levelObject.levelWordsRus,allHintCardIcon,
                        levelObject.levelWordsHint,allEngCardText,allRusCardText,
                        allSoundIcon,cardFrame1,cardFrame2,cardFrame3,cardFrame4,
                        cardFrame1Back,cardFrame2Back,cardFrame3Back,cardFrame4Back,allHintBack,
                        allHintIconBack);
                break;
            // Показываем для обучения 2 карточки
            case 2:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Изучаем новые слова");
                cardFrame1.setVisibility(View.VISIBLE);
                cardFrame2.setVisibility(View.VISIBLE);
                TrainTask2.trainTask2(levelObject.levelWordsEng,levelObject.levelWordsRus,
                        levelObject.levelWordsHint,allEngCardText,allRusCardText,allHintBack,
                        allSoundIcon,cardFrame1,cardFrame2,cardFrame1Back,cardFrame2Back,
                        allHintCardIcon,allHintIconBack);
                break;
            // Показываем правило для обучения
            case 3:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Грамматика");
                ruleFrame.setVisibility(View.VISIBLE);
                TrainTask3.trainTask3(ruleHeader,ruleInnerFrame,levelObject.levelRule,this);
                break;
            // Определяем правильность вопроса с ответом ДА/НЕТ
            case 4:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Определи, правильный ли указан перевод");
                TrainTask4 trainTask4=new TrainTask4();
                trainTask4.trainTask4(lvlType,questionText,answer1,answer2,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // Выбрать правильный перевод с английского на русский (4 варианта)
            case 5:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Выбери правильный перевод");
                TrainTask5 trainTask5=new TrainTask5();
                trainTask5.trainTask5(lvlType,questionText,allAnswers,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // Выбрать правильный перевод с русского на английский (4 варианта)
            case 6:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Выбери правильный перевод");
                TrainTask6 trainTask6=new TrainTask6();
                trainTask6.trainTask6(lvlType,questionText,allAnswers,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // Собираем слово из английских букв (перевод с русского)
            case 7:
                helpButton.setBackgroundResource(R.drawable.hint_icon_green);
                helpButton.setClickable(true);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Напиши правильный перевод\n(без артиклей и частицы to)");
                TrainTask7 trainTask7=new TrainTask7();
                trainTask7.trainTask7(lvlType,questionText,wordsFrame,wordsScroll,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask,
                        answerField,deleteSymbol,this);
                break;
            // Озвучиваем слово на английском, требуется найти перевод (4 варианта)
            case 8:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Прослушай и выбери правильный перевод");
                TrainTask8 trainTask8=new TrainTask8();
                trainTask8.trainTask8(lvlType,questionSound,allAnswers,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // Озвучиваем слово, требуется его собрать из букв
            case 9:
                helpButton.setBackgroundResource(R.drawable.hint_icon_green);
                helpButton.setClickable(true);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Прослушай и запиши");
                TrainTask9 trainTask9=new TrainTask9();
                trainTask9.trainTask9(lvlType,questionSound,wordsFrame,wordsScroll,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask
                        ,answerField,deleteSymbol,this);
                break;
            // Дано слово на русском языке и 4 варианта-озвучки на английском
            case 10:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Прослушай варианты и выбери правильный перевод");
                TrainTask10 trainTask10=new TrainTask10();
                trainTask10.trainTask10(lvlType,questionText,allAnswersSound,
                        levelObject.levelWordsEng,levelObject.levelWordsRus,currentTask);
                break;
            // 10 русских слов и 10 английских. Требуется найти соответствие
            case 11:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                taskDescription.setText("Найди соответствие между английскими и русскими словами");
                nextButton.setVisibility(View.GONE);
                TrainTask11 trainTask11=new TrainTask11();
                trainTask11.trainTask11(lvlType,livesText,levelObject.levelWordsEng,levelObject.levelWordsRus,
                        leftLinearWords,rightLinearWords,this,R.drawable.answers_shape_gray,
                        R.drawable.answer_shape_yellow,R.drawable.answers_shape_green,
                        R.drawable.answer_shape_red);
                break;
            // Вставить в готовое приложение правильный ответ
            case 12:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Выбери правильный вариант");
                TrainTask12 trainTask12=new TrainTask12();
                trainTask12.trainTask12(levelObject.levelRuleTrain,questionText,answerField,allAnswers,lvlType,
                        currentTask,gramaType,ruleTrue);
                break;
            // Собрать из букв правильный ответ для вставки в приложение
            case 13:
                helpButton.setBackgroundResource(R.drawable.hint_icon_green);
                helpButton.setClickable(true);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Дополни предложение");
                TrainTask13 trainTask13=new TrainTask13();
                trainTask13.trainTask13(lvlType,levelObject.levelRuleTrain,questionText,wordsFrame,wordsScroll,
                        answerField,deleteSymbol,this,gramaType,currentTask,ruleTrue);
                break;
            // Выбираем правильный вариант составления предложения
            case 14:
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.GONE);
                taskDescription.setText("Выбери правильный перевод");
                TrainTask14 trainTask14=new TrainTask14();
                trainTask14.trainTsk14(levelObject.levelRuleTrain,questionText,allAnswers,lvlType,
                        gramaType,currentTask,ruleTrue);
                break;
            // Собираем предложение из предложенных слов
            case 15:
                Log.d("Чекер", String.valueOf(currentTask));
                if(currentTask==7){
                    endOfTrainGrama=true;
                }
                helpButton.setBackgroundResource(R.drawable.hint_icon_gray);
                helpButton.setClickable(false);
                nextButton.setVisibility(View.VISIBLE);
                taskDescription.setText("Собери предложение из предложенных слов");
                TrainTask15 trainTask15=new TrainTask15();
                trainTask15.trainTask15(lvlType,currentTask,levelObject.levelRuleTrain,questionText,
                        wordsFrame,wordsScroll,answerField,deleteSymbol,this,gramaType,ruleTrue);
                break;
        }
    }

    // Обработка кнопки Далее
    public void nextTaskClick(View v){
        if(!nextButtonAvailable){
            showAlertLives();
            return;
        }
        String currentTrueAnswer="";
        // Изучение слов или правил
        if(allTasks.get(currentTask)==1||allTasks.get(currentTask)==2||
                allTasks.get(currentTask)==3||showCenterFrame==true){
            nextTaskMethod(0);
        }else{
            // Проверка заданий с набором слов
            switch (allTasks.get(currentTask)){
                case 7:
                    if(currentTaskAnswer){
                        singleWordsSound(answerField.getTag().toString());
                        nextTaskMethod(1200);
                    }else{
                        answerField.setTextColor(getResources().getColor(R.color.colorRed));
                        currentTrueAnswer=answerField.getTag().toString();
                        wrongAnswer(currentTrueAnswer);
                    }
                    break;
                case 9:
                    if(currentTaskAnswer){
                        singleWordsSound(questionSound.getTag().toString());
                        nextTaskMethod(1200);
                    }else{
                        answerField.setTextColor(getResources().getColor(R.color.colorRed));
                        currentTrueAnswer=questionSound.getTag().toString();
                        wrongAnswer(currentTrueAnswer);
                    }
                    break;
                case 10:
                    if(currentTaskAnswer){
                        nextTaskMethod(1200);
                    }else{
                        if(lastSound!=null){
                            lastSound.setBackgroundResource(R.drawable.volume_icon_red);
                        }
                        wrongAnswer(questionText.getTag().toString());
                    }
                    break;
                case 13:
                    if(currentTaskAnswer){
                        nextTaskMethod(1200);
                    }else{
                        answerField.setTextColor(getResources().getColor(R.color.colorRed));
                        wrongGrammaAnswer();
                    }
                    break;
                case 15:
                    if(currentTaskAnswer){
                        nextTaskMethod(1200);
                    }else{
                        answerField.setTextColor(getResources().getColor(R.color.colorRed));
                        wrongGrammaAnswer();
                    }
                    break;
            }
        }
    }

    // Переходим на следующе задание
    public void nextTaskMethod(int time){

        // Проверка на окончание грамматической треннировки
        if(endOfTrainGrama){
            Intent myIntent = new Intent(this, EndOfLevelActivity.class)
                    .putExtra("rating", CommonLevel.levelRating)
                    .putExtra("currentLvl",CommonLevel.currentIntentLvl)
                    .putExtra("currentWorld",CommonLevel.currentIntentWorld)
                    .putExtra("lvlType","train");
            startActivity(myIntent);
            finish();
            return;
        }

        handler.postDelayed(new Runnable() {
            public void run() {
                // Анимация Кручения
                roundCentralFrame();

                // Звук ответа
                boolean soundOn=true;

                // Красим кнопки ответов в серый цвет
                for(int i=0;i<allAnswers.size();i++){
                    allAnswers.get(i).setBackgroundResource(R.drawable.answers_shape_gray);
                }

                if(allTasks.get(currentTask)==1||allTasks.get(currentTask)==2||
                        allTasks.get(currentTask)==3||showCenterFrame==true){
                    currentTaskAnswer=true;
                    soundOn=false;
                }

                if(!currentTaskAnswer){
                    lives=lives-1;
                    if(lives==0){
                        //showAlertLives();
                        nextButtonAvailable=false;
                    }
                    livesText.setText("x"+String.valueOf(lives));
                    levelRating=levelRating+1;
                }else{
                    if(soundOn) {
                        answerSound(true);
                    }
                }

                showCenterFrame=false;
                currentTask=currentTask+1;
                currentTaskAnswer=false;
                cardFrameCenter.setVisibility(View.GONE);

                // Меняем кол-во вопросов Удлинняем шкалу прогресса
                if(lvlType.equals("common")) {
                    progressText.setText(String.valueOf(currentTask + 1) + " из 30");
                }else{
                    if(getIntent().getStringExtra("trainType").equals("words")) {
                        progressText.setText(String.valueOf(currentTask + 1) + " из 15");
                    }else{
                        progressText.setText(String.valueOf(currentTask + 1) + " из 8");
                    }
                }

                ImageView image =new ImageView(getApplicationContext());
                LinearLayout.LayoutParams imageParams=new LinearLayout.LayoutParams(progressCellWidth,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                image.setBackgroundResource(R.drawable.part_of_progress);
                progresLinear.addView(image,imageParams);

                // Скрываем весь UI
                hideUi();

                // Подгружаем новый вопрос и новый UI
                createUI(allTasks.get(currentTask));
            }
        }, time);
    }

    // Обработка кнопок Ответы
    public void answerClick(View v){
        Button but=(Button) v;
        boolean fastAnswer=true;
        Button probablyAnswer=null;
        String currentTrueAnswer="";

        // Красим выбранный ответ в зелёный
        but.setBackgroundResource(R.drawable.answers_shape_green);

        // Убираем кликабельность с кнопок
        for (int i=0;i<allAnswers.size();i++){
            allAnswers.get(i).setClickable(false);
        }

        switch (allTasks.get(currentTask)) {
            case 4:
                if (questionText.getTag().toString().equals(but.getText().toString())) {
                    currentTaskAnswer = true;
                } else {
                    currentTaskAnswer = false;
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(questionText.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                            currentTrueAnswer=allAnswers.get(i).getTag().toString();
                        }
                    }
                }
                break;
            case 5:
                if(questionText.getTag().toString().equals(but.getText().toString())){
                    currentTaskAnswer = true;
                } else {
                    currentTaskAnswer = false;
                    currentTrueAnswer=questionText.getText().toString();
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(questionText.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                        }
                    }
                }
                break;
            case 6:
                if(questionText.getTag().toString().equals(but.getText().toString())){
                    currentTaskAnswer = true;
                } else {
                    currentTaskAnswer = false;
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(questionText.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                            currentTrueAnswer=allAnswers.get(i).getText().toString();
                        }
                    }
                }
                break;
            case 8:
                if (but.getText().toString().equals(but.getTag().toString())) {
                    currentTaskAnswer = true;
                } else {
                    currentTaskAnswer = false;
                    currentTrueAnswer=questionSound.getTag().toString();
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(allAnswers.get(i).getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                        }
                    }
                }
                break;
            case 12:
                fastAnswer=true;
                answerField.setText(answerField.getText().toString().replace("__",but.getText().toString()));
                if (but.getText().toString().equals(answerField.getTag().toString())) {
                    currentTaskAnswer = true;
                } else {
                    fastAnswer=false;
                    currentTaskAnswer = false;
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(answerField.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                        }
                    }
                }
                break;
            case 14:
                if(questionText.getTag().toString().equals(but.getText().toString())){
                    currentTaskAnswer = true;
                } else {
                    fastAnswer=false;
                    currentTaskAnswer = false;
                    // Ищем правильный ответ
                    for (int i=0;i<allAnswers.size();i++){
                        if(allAnswers.get(i).getText().toString().equals(questionText.getTag().toString())){
                            probablyAnswer=allAnswers.get(i);
                        }
                    }
                }
                break;
        }

        // Проверка на быстрый ответ
            checkTrueAnswerAnimation(but,probablyAnswer,currentTrueAnswer,fastAnswer);

    }

    // Хендлер на проверку правильности ответа
    public void checkTrueAnswerAnimation(final Button correctAnswer, final Button probablyAnswer,
                                         final String currentTrueAnswer, final boolean fastAnswer){
        handler.postDelayed(new Runnable() {
            public void run() {
                // Правильный ответ
                if(currentTaskAnswer) {
                    // Получаем звук в зависимости от задания
                    switch (allTasks.get(currentTask)){
                        case 4:
                            singleWordsSound(correctAnswer.getTag().toString());
                            break;
                        case 5:
                            singleWordsSound(questionText.getText().toString());
                            break;
                        case 6:
                            singleWordsSound(correctAnswer.getText().toString());
                            break;
                        case 7:
                            singleWordsSound(answerField.getTag().toString());
                            break;
                        case 8:
                            singleWordsSound(correctAnswer.getTag().toString());
                            break;
                        case 9:
                            singleWordsSound(answerField.getTag().toString());
                            break;
                        case 12:
                            break;
                        case 13:
                            break;
                        case 14:
                            singleWordsSound(questionText.getTag().toString());
                            break;
                        case 15:
                            break;
                    }

                    nextTaskMethod(1200);

                    // Неправильный ответ
                }else{
                    correctAnswer.setBackgroundResource(R.drawable.answer_shape_red); // Пометка неправильного ответа
                    probablyAnswer.setBackgroundResource(R.drawable.answers_shape_green); // Пометка правильного

                    // Выводим карточку с правильным ответом
                    if(fastAnswer) {
                        if(allTasks.get(currentTask)==12||allTasks.get(currentTask)==13||
                                allTasks.get(currentTask)==14||allTasks.get(currentTask)==15){
                            wrongGrammaAnswer();
                        }else {
                            wrongAnswer(currentTrueAnswer);
                        }
                    }else{
                        if(allTasks.get(currentTask)==12||allTasks.get(currentTask)==13||
                                allTasks.get(currentTask)==14||allTasks.get(currentTask)==15){
                            wrongGrammaAnswer();
                            return;
                        }
                        lives=lives-1;
                        if(lives==0){
                            //showAlertLives();
                            nextButtonAvailable=false;
                        }
                        livesText.setText("x"+String.valueOf(lives));
                        nextTaskMethod(1200);
                        levelRating=levelRating+1;
                    }
                }
            }
        }, 300);
    }

    // При неверном ответе скрываем старый UI и показываем карточку правильного ответа
    public void wrongAnswer(final String answer){
        // Убираем видимость подсказки
        taskDescription.setText("Повторяем слово");

        // Звук неверного ответа
        if (soundOn) {
            answerFalsePlayer.start();
        }

        lives=lives-1;
        if(lives==0){
            //showAlertLives();
            nextButtonAvailable=false;
        }
        livesText.setText("x"+String.valueOf(lives));
        levelRating=levelRating+1;

            handler.postDelayed(new Runnable() {
                public void run() {
                    hideUi();
                    cardFrameCenter.setVisibility(View.VISIBLE);
                    showCenterFrame = true;
                    nextButton.setVisibility(View.VISIBLE);

                    // Меняем контент внутри карточки на правильный ответ
                    ArrayList<String> checkList=new ArrayList();
                    checkList.add("a "+answer);
                    checkList.add("an "+answer);
                    checkList.add("to "+answer);
                    checkList.add(answer);
                    for(int j=0;j<checkList.size();j++) {
                        for (int i = 0; i < levelObject.levelWordsEng.size(); i++) {
                            if (levelObject.levelWordsEng.get(i).equals(checkList.get(j))) {
                                textEngCenter.setText(levelObject.levelWordsEng.get(i));
                                textRusCenter.setText(levelObject.levelWordsRus.get(i));
                                soundIconCenter.setTag(levelObject.levelWordsEng.get(i));
                                singleWordsSound(checkList.get(j));
                            }
                        }
                    }
                    // Озвучиваем правильный ответ
                }
            }, 1000);
    }

    // При неверном ответе на правило
    // При неверном ответе скрываем старый UI и показываем карточку правильного ответа
    public void wrongGrammaAnswer(){
        // Убираем видимость подсказки
        taskDescription.setText("Повторяем грамматику");

        // Звук неверного ответа
        if (soundOn) {
            answerFalsePlayer.start();
        }

        lives=lives-1;
        if(lives==0){
            //showAlertLives();
            nextButtonAvailable=false;
        }
        livesText.setText("x"+String.valueOf(lives));
        levelRating=levelRating+1;

        handler.postDelayed(new Runnable() {
            public void run() {
                hideUi();
                showCenterFrame=true;
                ruleFrame.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.VISIBLE);

            }
        }, 1000);
    }

    // Озвучивание переданного слова
    public void singleWordsSound(final String word){

        WordsSoundManager.textToSpeech(word);
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(word, TextToSpeech.QUEUE_FLUSH, null,"utterence1");
        }else{
            tts.speak(word, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
        }*/
    }

    // Обработка TextToSpeach при одном звуковом варианте
    public void singleSoundQuestionClick(View v){
        if(v.getTag()!=null){
            WordsSoundManager.textToSpeech(v.getTag().toString());
        }

        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(v.getTag().toString(), TextToSpeech.QUEUE_FLUSH, null,"utterence1");
        }else{
            tts.speak(v.getTag().toString(), TextToSpeech.QUEUE_FLUSH, myHashAlarm);
        }*/
    }

    // Обработка TextToSpeach при нескольких звуковых вариантах
    public void soundQuestionClick(View v){
        WordsSoundManager.textToSpeech(v.getTag().toString());
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(v.getTag().toString(), TextToSpeech.QUEUE_FLUSH, null,"utterence1");
        }else{
            tts.speak(v.getTag().toString(), TextToSpeech.QUEUE_FLUSH, myHashAlarm);
        }*/

            if(questionText.getTag().toString().equals(v.getTag().toString())){
                currentTaskAnswer = true;
            } else {
                currentTaskAnswer = false;}

        // Все иконки звука возвращаем в серый
        for (int i=0;i<allAnswersSound.size();i++){
            allAnswersSound.get(i).setBackgroundResource(R.drawable.volume_icon_gray);
        }
        v.setBackgroundResource(R.drawable.volume_icon);
        lastSound=(Button)v;
    }

    // Собираем листы UI элементов
    public void createUIList(){
        allEngCardText.add(textEng1);  allEngCardText.add(textEng2);
        allEngCardText.add(textEng3);  allEngCardText.add(textEng4);

        allRusCardText.add(textRus1);  allRusCardText.add(textRus2);
        allRusCardText.add(textRus3);  allRusCardText.add(textRus4);

        allHintCardText.add(hintText1); allHintCardText.add(hintText2);
        allHintCardText.add(hintText3); allHintCardText.add(hintText4);

        allSoundIcon.add(soundIcon1); allSoundIcon.add(soundIcon2);
        allSoundIcon.add(soundIcon3); allSoundIcon.add(soundIcon4);

        allAnswers.add(answer1); allAnswers.add(answer2);
        allAnswers.add(answer3); allAnswers.add(answer4);

        allAnswersSound.add(answerSound1); allAnswersSound.add(answerSound2);
        allAnswersSound.add(answerSound3); allAnswersSound.add(answerSound4);

        allHintCardIcon.add(hintIcon1); allHintCardIcon.add(hintIcon2);
        allHintCardIcon.add(hintIcon3); allHintCardIcon.add(hintIcon4);

        allHintBack.add(hintText1Back); allHintBack.add(hintText2Back);
        allHintBack.add(hintText3Back); allHintBack.add(hintText4Back);

        allHintIconBack.add(hintIcon1Back); allHintIconBack.add(hintIcon2Back);
        allHintIconBack.add(hintIcon3Back); allHintIconBack.add(hintIcon4Back);
    }

    // Скрываем ненужные элементы интерфейса
    public void hideUi(){
        // Проверяем есть ли перевёрнутые карточки. Если есть - переварачиваем их назад
        if(cardVisible1){
            cardFrame1.setRotationY(0f);
            cardFrame1.setAlpha(1);
            cardFrame1Back.setRotationY(-180f);
            cardVisible1=false;
        }
        if(cardVisible2){
            cardFrame2.setRotationY(0f);
            cardFrame2.setAlpha(1);
            cardFrame2Back.setRotationY(-180f);
            cardVisible2=false;
        }
        if(cardVisible3){
            cardFrame3.setRotationY(0f);
            cardFrame3.setAlpha(1);
            cardFrame3Back.setRotationY(-180f);
            cardVisible3=false;
        }

        // Скрываем карточки со словами
        cardFrame1.setVisibility(View.GONE); cardFrame2.setVisibility(View.GONE);
        cardFrame3.setVisibility(View.GONE); cardFrame4.setVisibility(View.GONE);

        // Скрываем карточки с подсказками
        cardFrame1Back.setVisibility(View.GONE); cardFrame2Back.setVisibility(View.GONE);
        cardFrame3Back.setVisibility(View.GONE); cardFrame4Back.setVisibility(View.GONE);

        // Скрываем текст вопроса
        questionText.setVisibility(View.GONE);

        // Возвращаем цвет полю ответов
        answerField.setTextColor(getResources().getColor(R.color.colorAccent));

        // Скрываем поле для ответа и чистим от старых символов
        answerField.setVisibility(View.GONE);
        answerField.setText("");

        // Скрываем поля-варианты ответов
        for (int i=0;i<allAnswers.size();i++){
            allAnswers.get(i).setVisibility(View.GONE);
        }

        // Восстанавливаем кликабельность вариантам ответов
        for (int i=0;i<allAnswers.size();i++){
            allAnswers.get(i).setClickable(true);
        }

        // Скрываем поля-варианты ответов с озвучкой
        for (int i=0;i<allAnswersSound.size();i++){
            allAnswersSound.get(i).setVisibility(View.GONE);
        }

        // Скрываем иконки подсказок для слов
        for (int i=0;i<allHintCardIcon.size();i++){
            allHintCardIcon.get(i).setVisibility(View.GONE);
        }

        // Все иконки звука возвращаем в серый
        for (int i=0;i<allAnswersSound.size();i++){
            allAnswersSound.get(i).setBackgroundResource(R.drawable.volume_icon_gray);
        }

        // Чистим и Скрываем фрейм для сбора слов и предложений
        wordsFrame.removeAllViews();
        wordsScroll.setVisibility(View.GONE);

        // Скрываем кнопку звукового вопроса
        questionSound.setVisibility(View.GONE);

        // Скрываем фрейм для изучения правил
        ruleFrame.setVisibility(View.GONE);

        // Скрываем кнопку удаления букв
        deleteSymbol.setVisibility(View.GONE);
    }

    // Анимации карточек слов в зависимости от номера карточки
    public void roundCardAnimation(View v){
        if(!mSetLeftIn.isRunning()) { // Проверка на анимацию другой карточки
            switch (v.getTag().toString()) {

                case "topLeft":
                    createAnimation(cardVisible1, cardFrame1, cardFrame1Back);

                    if (!cardVisible1) {
                        cardVisible1 = true;
                    } else {
                        cardVisible1 = false;
                    }

                    break;
                case "topRight":
                    createAnimation(cardVisible2, cardFrame2, cardFrame2Back);

                    if (!cardVisible2) {
                        cardVisible2 = true;
                    } else {
                        cardVisible2 = false;
                    }

                    break;
                case "bottomLeft":
                    createAnimation(cardVisible3, cardFrame3, cardFrame3Back);

                    if (!cardVisible3) {
                        cardVisible3 = true;
                    } else {
                        cardVisible3 = false;
                    }

                    break;
                case "bottomRight":
                    createAnimation(cardVisible4, cardFrame4, cardFrame4Back);

                    if (!cardVisible4) {
                        cardVisible4 = true;
                    } else {
                        cardVisible4 = false;
                    }

                    break;
            }
        }
    }

    // Метод создания анимации
    public void createAnimation(boolean cardVisibility,CardView frontCard,CardView backCard){
            if (!cardVisibility) {
                frontCard.setRotationY(180f);
                backCard.setRotationY(0f);
                frontCard.setAlpha(0f);
                backCard.setAlpha(1f);
//                mSetRightOut.setTarget(frontCard);
//                mSetLeftIn.setTarget(backCard);
//                mSetRightOut.start();
//                mSetLeftIn.start();
            } else {
                frontCard.setRotationY(0f);
                backCard.setRotationY(180f);
                frontCard.setAlpha(1f);
                backCard.setAlpha(0f);
//                mSetRightOut.setTarget(backCard);
//                mSetLeftIn.setTarget(frontCard);
//                mSetRightOut.start();
//                mSetLeftIn.start();
            }
    }

    // Анимация главного центрального фрейма
    public void roundCentralFrame(){
        ObjectAnimator.ofFloat(contentFrame, "rotationY", 0f, 360f).setDuration(800).start();
        changeCameraDistanceMain();
    }

    // Отдаление камеры  главного фрейма
    private void changeCameraDistanceMain() {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        contentFrame.setCameraDistance(scale);
    }

    // оптимизация всех элементов
    public void optimizationUI(){
        Optimization optimization=new Optimization();
        optimization.Optimization(mainFrame);
        optimization.Optimization(cardFrame1);
        optimization.Optimization(cardFrame2);
        optimization.Optimization(cardFrame3);

        optimization.Optimization(cardFrameCenter);
        optimization.Optimization(cardFrame1Back);
        optimization.Optimization(cardFrame2Back);
        optimization.Optimization(cardFrame3Back);

        optimization.Optimization(ruleFrame);
        optimization.Optimization(audioButtonsFrame);

    }

    // Добавляем в словарь текущего уровня все слова
    public void addAllword(){
        for (int i=0;i<levelObject.levelWordsEng.size();i++){
            wordsPackMap.put(levelObject.levelWordsEng.get(i),0);
        }
    }

    // Обработка кнопки подсказок
    public void useHintBut(View v){

        // Проверка на наличие подсказок
        if(!hintCountText.getText().toString().equals("0")) {

            if(hintCountText.getText().toString().equals("1")){
                showAlertHint();
            }
            // Подсказка для Задания 7 и 9
            if(allTasks.get(currentTask)==7||allTasks.get(currentTask)==9) {
                // Узнаём вписан ли уже правильный ответ
                if (answerField.getText().toString().toLowerCase().equals(answerField.getTag().toString().toLowerCase())) {
                    hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) - 1));
                    Paper.book().write("hintCount", Integer.valueOf(hintCountText.getText().toString()));
                    nextButton.callOnClick();
                    return;
                }

                // Узнаём правильно ли прописан текст и нужна ли подсказка
                boolean checkWords = true;

                if (answerField.getText().toString().length() <= answerField.getTag().toString().length()) {
                    for (int i = 0; i < answerField.getText().toString().length(); i++) {

                        if (!String.valueOf(answerField.getText().toString().charAt(i)).toLowerCase()
                                .equals(String.valueOf(answerField.getTag().toString().charAt(i)).toLowerCase())) {
                            checkWords = false;
                        }
                    }
                } else {
                    checkWords = false;
                }

                if (!checkWords) {
                    // Возвращаем видимость всех кнопок и обнуляем ответ
                    for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                        wordsFrame.getChildAt(i).setVisibility(View.VISIBLE);
                    }
                    answerField.setText("");
                }

                // Получаем нужную букву
                String trueAnswer = answerField.getTag().toString();
                String trueCharacter = String.valueOf(trueAnswer.charAt(answerField.getText().length()));

                for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                    Button but = (Button) wordsFrame.getChildAt(i);

                    if (but.getText().toString().toLowerCase().equals(trueCharacter.toLowerCase())) {
                        but.callOnClick();
                        hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) - 1));
                        Paper.book().write("hintCount", Integer.valueOf(hintCountText.getText().toString()));
                        break;
                    }
                }
            }else{
                // Подсказка для задания 13
                // Узнаём вписан ли уже правильный ответ
                if (answerField.getTag().toString().toLowerCase().equals(wordsFrame.getTag().toString().toLowerCase())) {
                    hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) - 1));
                    Paper.book().write("hintCount", Integer.valueOf(hintCountText.getText().toString()));
                    nextButton.callOnClick();
                    return;
                }

                // Узнаём правильно ли прописан текст и нужна ли подсказка
                boolean checkWords = true;

                if (wordsFrame.getTag().toString().length() <= answerField.getTag().toString().length()) {
                    for (int i = 0; i < wordsFrame.getTag().toString().length(); i++) {

                        if (!String.valueOf(wordsFrame.getTag().toString().charAt(i)).toLowerCase()
                                .equals(String.valueOf(answerField.getTag().toString().charAt(i)).toLowerCase())) {
                            checkWords = false;
                        }
                    }
                } else {
                    checkWords = false;
                }

                if (!checkWords) {
                    // Возвращаем видимость всех кнопок и обнуляем ответ
                    for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                        wordsFrame.getChildAt(i).setVisibility(View.VISIBLE);
                    }
                    while (!wordsFrame.getTag().toString().equals("")){
                        answerField.callOnClick();
                    }
                }
                // Получаем нужную букву
                String trueAnswer = answerField.getTag().toString();
                String trueCharacter = String.valueOf(trueAnswer.charAt(wordsFrame.getTag().toString().length()));

                for (int i = 0; i < wordsFrame.getChildCount(); i++) {
                    Button but = (Button) wordsFrame.getChildAt(i);

                    if (but.getText().toString().toLowerCase().equals(trueCharacter.toLowerCase())) {
                        but.callOnClick();
                        hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) - 1));
                        Paper.book().write("hintCount", Integer.valueOf(hintCountText.getText().toString()));
                        break;
                    }
                }
            }

        }else {
            // Подсказки закончились, предложить посмотреть видос
            showAlertHint();
        }
    }

    // Прописываем появление AlertDialog
    // Обработка и появление диалога про окончание жизней
    public void showAlertLives(){
        final Dialog openDialog = new Dialog(this);
        openDialog.setContentView(R.layout.custom_alertdialog_lives);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        openDialog.setCancelable(false);
        openDialog.show();

        // Оптимизация диалога
        Optimization optimization=new Optimization();
        optimization.Optimization((FrameLayout)openDialog.findViewById(R.id.frame_alert_lives));

        // Получение жизни и показ рекламы
        ImageView btnGetLives = (ImageView) openDialog.findViewById(R.id.btn_get_live);
        btnGetLives.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Проверяем, не использовано ли максимальное кол-во жизней
                if(maxLives==2){
                    createToast("Вы использовали все дополнительные жизни");
                    return;
                }

                // Показываем рекламный ролик для получения жизни
                if(showAds.mInterstitialAd.isLoaded()){
                    showAds.mInterstitialAd.show();
                    openDialog.dismiss();
                    lives = 1;
                    livesText.setText("x1");
                    maxLives=maxLives+1;
                    nextButtonAvailable=true;
                }else{
                    createToast("В данный момент нет видео для просмотра");
                }
            }
        });
    }

    // Обработка и появления диалога про окончание подсказок
    public void showAlertHint(){
        final Dialog openDialog = new Dialog(this);
        openDialog.setContentView(R.layout.custom_alertdialog_hints);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        openDialog.setCancelable(false);
        openDialog.show();

        // Оптимизация диалога
        Optimization optimization=new Optimization();
        optimization.Optimization((FrameLayout)openDialog.findViewById(R.id.frame_alert_hints));

        // Получение 3 подсказок и показ рекламы
        ImageView btnGetHints = (ImageView) openDialog.findViewById(R.id.btn_get_hint);
        btnGetHints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Показываем рекламный ролик для получения подсказок
                if(showAds.mInterstitialAd.isLoaded()){
                    showAds.mInterstitialAd.show();
                    openDialog.dismiss();
                    hintCountText.setText(String.valueOf(Integer.valueOf(hintCountText.getText().toString()) + 3));
                    Paper.book().write("hintCount",Integer.valueOf(hintCountText.getText().toString()));
                }else{
                    createToast("В данный момент нет рекламы для просмотра");
                }
            }
        });

        // Закрываем данный экран
        Button btnClose = (Button) openDialog.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.dismiss();
            }
        });
    }

    // Диалог бокового меню текущего экрана
    public void mainMenuDialog(){
        final Dialog openDialog = new Dialog(this);
        openDialog.setContentView(R.layout.custom_alert_main_menu);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        openDialog.setCancelable(true);
        openDialog.show();

        // Оптимизация диалога
        Optimization optimization=new Optimization();
        optimization.Optimization((FrameLayout)openDialog.findViewById(R.id.frame_alert_menu));
    }

    // Переход на главный экран
    public void backToMainScreen(View v){
        confirmExitDialog("main");
    }

    // Перезапуск текущего уровня
    public void restartLevel(View v){
        confirmExitDialog("restart");
    }

    // Диалог о подтверждении о выходе с текущего уровня
    public void confirmExitDialog(final String type){

        String alertMessage="Вы хотите покинуть данный уровень?";
        if(type.equals("restart")){
            alertMessage="Вы хотите начать уровень заново?";
        }
        // Выдаём диалог об окончании уровня
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Внимание!")
                .setMessage(alertMessage)
                .setIcon(R.drawable.main_screen_logo)
                .setCancelable(false)
                .setPositiveButton("Да",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                switch (type){
                                    case "back":
                                        dialog.cancel();
                                        finish();
                                        break;
                                    case "main":
                                        dialog.cancel();
                                        startActivity(new Intent(CommonLevel.this,MainActivity.class)
                                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                        finish();
                                        break;
                                    case "restart":
                                        dialog.cancel();
                                        if(lvlType.equals("train")){
                                            startActivity(new Intent(CommonLevel.this, CommonLevel.class)
                                                    .putExtra("currentWorld", "train"));
                                            finish();
                                        }else {
                                            startActivity(new Intent(CommonLevel.this, CommonLevel.class)
                                                    .putExtra("currentLvl", currentIntentLvl)
                                                    .putExtra("currentWorld", currentIntentWorld));
                                            finish();
                                        }

                                        break;

                                }
                            }
                        })
                .setNegativeButton("Нет",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    // Подбор слов для тренировки
    public ArrayList<String> getRusWords(){
        return getIntent().getStringArrayListExtra("rusMas");
    }

    // Подбор слов для тренировки
    public ArrayList<String> getEngWords(){
        return getIntent().getStringArrayListExtra("engMas");
    }

    // Обработка звуков правильного / не правильного ответов
    public void answerSound(boolean isCorrect){
        if(soundOn) {
            if (isCorrect) {
                answerTruePlayer.start();
            }else{
                answerFalsePlayer.start();
            }
        }
    }

    // Переопределяем кнопку назад
    @Override
    public void onBackPressed() {
        confirmExitDialog("back");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onPause(){
        super.onPause();
    }

    public void onDestroy(){
        if(tts !=null){
            tts.stop();
            //tts.shutdown();
        }
        Log.d("Adshow","Destroy");
        super.onDestroy();
    }

    // Конструктор Тоаст сообщений
    public void createToast(String text){
        Toast.makeText(this, text,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
