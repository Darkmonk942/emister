package com.rocketraven.emister.optimization;

import android.util.Log;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.paperdb.Paper;

/**
 * Created by Darkmonk on 20.05.2016.
 */

    public class Optimization {
    int height,width;

        public void Optimization(FrameLayout layout){
            // Получаем из базы параметры экрана текущего устройства
            getScreenParams();

            for (int i=0;i<layout.getChildCount();i++) {

                if(layout.getChildAt(i) instanceof FrameLayout){

                    FrameLayout frame=(FrameLayout)layout.getChildAt(i);

                    totalOptimization(layout, i);

                    for (int j=0;j<frame.getChildCount();j++){
                        totalOptimization(frame,j);
                    }

                }else{
                    totalOptimization(layout, i);
                }
            }
        }

        public void OptimizationLinear(LinearLayout layout){
            // Получаем из базы параметры экрана текущего устройства
            getScreenParams();

            for (int i=0;i<layout.getChildCount();i++) {
                totalOptimizationLinear(layout,i);
            }
        }

        public void totalOptimization(FrameLayout layout,int i){
            FrameLayout.LayoutParams vlp = (FrameLayout.LayoutParams) layout.getChildAt(i).getLayoutParams();

            // Вычисляем новые параметры
            int newHeight = (vlp.height * height) / 1920;
            int newWidth = (vlp.width * width) / 1080;
            int leftMargin = (vlp.leftMargin * width) / 1080;
            int rightMargin = (vlp.rightMargin * width) / 1080;
            int topMargin = (vlp.topMargin * height) / 1920;
            int bottomMargin = (vlp.bottomMargin * height) / 1920;

            if(vlp.height==vlp.width){
                newWidth=newHeight;
            }

            if (vlp.width == FrameLayout.LayoutParams.MATCH_PARENT) {
                newWidth = width;
            }

            if (vlp.width == FrameLayout.LayoutParams.WRAP_CONTENT) {
                newWidth = FrameLayout.LayoutParams.WRAP_CONTENT;
            }

            if (vlp.height == FrameLayout.LayoutParams.MATCH_PARENT) {
                newHeight = height;
            }

            if (vlp.height == FrameLayout.LayoutParams.WRAP_CONTENT) {
                newHeight = FrameLayout.LayoutParams.WRAP_CONTENT;
            }

            if(vlp.width==1){
                newWidth=1;
            }

            if(vlp.height==1){
                newHeight=1;
            }

            // Меняем параметры View
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(newWidth, newHeight); // устанавливаем параметры для View
            params.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
            params.gravity = vlp.gravity;
            layout.getChildAt(i).setLayoutParams(params);

            if (layout.getChildAt(i) instanceof TextView) {
                TextView text = (TextView) layout.getChildAt(i);
                int newTextSize = (Math.round(text.getTextSize()) * width) / 1080;
                text.setTextSize(TypedValue.COMPLEX_UNIT_PX, newTextSize);
                // Отступы
                int leftPadding = (text.getPaddingLeft() * width) / 1080;
                int rightPadding = (text.getPaddingRight() * width) / 1080;
                int topPadding = (text.getPaddingTop() * height) / 1920;
                int bottomPadding = (text.getPaddingBottom() * height) / 1920;

                text.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);
                text.setGravity(text.getGravity());
            }

            if (layout.getChildAt(i) instanceof Button) {
                Button but = (Button) layout.getChildAt(i);

                but.setGravity(but.getGravity());
            }
        }

        public void totalOptimizationLinear(LinearLayout layout,int i) {
            Log.d("Кол-во",String.valueOf(layout.getChildCount()) );
            if (layout.getChildCount() != 0) {
                LinearLayout.LayoutParams vlp = (LinearLayout.LayoutParams) layout.getChildAt(i).getLayoutParams();
                // Вычисляем новые параметры
                int newHeight = (vlp.height * height) / 1920;
                int newWidth = (vlp.width * width) / 1080;
                int leftMargin = (vlp.leftMargin * width) / 1080;
                int rightMargin = (vlp.rightMargin * width) / 1080;
                int topMargin = (vlp.topMargin * height) / 1920;
                int bottomMargin = (vlp.bottomMargin * height) / 1920;

                if (vlp.width == LinearLayout.LayoutParams.MATCH_PARENT) {
                    newWidth = width;
                }

                if (vlp.width == LinearLayout.LayoutParams.WRAP_CONTENT) {
                    newWidth = LinearLayout.LayoutParams.WRAP_CONTENT;
                }

                if (vlp.height == LinearLayout.LayoutParams.MATCH_PARENT) {
                    newHeight = height;
                }

                if (vlp.height == LinearLayout.LayoutParams.WRAP_CONTENT) {
                    newHeight = LinearLayout.LayoutParams.WRAP_CONTENT;
                }

                if(vlp.width==1){
                    newWidth=1;
                }

                if(vlp.height==1){
                    newHeight=1;
                }

                // Меняем параметры View
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(newWidth, newHeight); // устанавливаем параметры для View
                params.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
                params.weight=vlp.weight;
                params.gravity = vlp.gravity;
                layout.getChildAt(i).setLayoutParams(params);


                if (layout.getChildAt(i) instanceof TextView) {
                    TextView text = (TextView) layout.getChildAt(i);
                    int newTextSize = (Math.round(text.getTextSize()) * width) / 1080;
                    text.setTextSize(TypedValue.COMPLEX_UNIT_PX, newTextSize);
                    // Отступы
                    int leftPadding = (text.getPaddingLeft() * width) / 1080;
                    int rightPadding = (text.getPaddingRight() * width) / 1080;
                    int topPadding = (text.getPaddingTop() * height) / 1920;
                    int bottomPadding = (text.getPaddingBottom() * height) / 1920;

                    text.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);
                    text.setGravity(text.getGravity());
                }

                if (layout.getChildAt(i) instanceof Button) {
                    Button but = (Button) layout.getChildAt(i);

                    but.setGravity(but.getGravity());
                }


                if(layout.getChildAt(i) instanceof FrameLayout){
                    FrameLayout frame=(FrameLayout)layout.getChildAt(i);
                    for (int j=0;j<frame.getChildCount();j++){
                        totalOptimization(frame,j);
                    }
                }
            }
        }

    // Оптимизация самого фрейма
    public void totalOptimizationRecycler(FrameLayout layout,int i){
        FrameLayout.LayoutParams vlp = (FrameLayout.LayoutParams) layout.getChildAt(i).getLayoutParams();

        // Вычисляем новые параметры
        int newHeight = (vlp.height * height) / 1920;
        int newWidth = (vlp.width * width) / 1080;
        int leftMargin = (vlp.leftMargin * width) / 1080;
        int rightMargin = (vlp.rightMargin * width) / 1080;
        int topMargin = (vlp.topMargin * height) / 1920;
        int bottomMargin = (vlp.bottomMargin * height) / 1920;

        if(vlp.height==vlp.width){
            newWidth=newHeight;
        }

        if (vlp.width == FrameLayout.LayoutParams.MATCH_PARENT) {
            newWidth = width;
        }

        if (vlp.width == FrameLayout.LayoutParams.WRAP_CONTENT) {
            newWidth = FrameLayout.LayoutParams.WRAP_CONTENT;
        }

        if (vlp.height == FrameLayout.LayoutParams.MATCH_PARENT) {
            newHeight = height;
        }

        if (vlp.height == FrameLayout.LayoutParams.WRAP_CONTENT) {
            newHeight = FrameLayout.LayoutParams.WRAP_CONTENT;
        }

        if(vlp.width==1){
            newWidth=1;
        }

        if(vlp.height==1){
            newHeight=1;
        }

        // Меняем параметры View
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(newWidth, newHeight); // устанавливаем параметры для View
        params.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
        layout.getChildAt(i).setLayoutParams(params);

        if (layout.getChildAt(i) instanceof TextView) {
            TextView text = (TextView) layout.getChildAt(i);
            int newTextSize = (Math.round(text.getTextSize()) * width) / 1080;
            text.setTextSize(TypedValue.COMPLEX_UNIT_PX, newTextSize);
            // Отступы
            int leftPadding = (text.getPaddingLeft() * width) / 1080;
            int rightPadding = (text.getPaddingRight() * width) / 1080;
            int topPadding = (text.getPaddingTop() * height) / 1920;
            int bottomPadding = (text.getPaddingBottom() * height) / 1920;

            text.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);
            text.setGravity(text.getGravity());
        }

        if (layout.getChildAt(i) instanceof Button) {
            Button but = (Button) layout.getChildAt(i);

            but.setGravity(but.getGravity());
        }
    }

    // Плучаем размеры экрана устройства
    public void getScreenParams(){
        height= Paper.book().read("height", 0);
        width=Paper.book().read("width",0);
    }
    }

