package com.rocketraven.emister.trains;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rocketraven.emister.CommonLevel;
import com.rocketraven.emister.UnCommonLevel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Darkmonk on 14.05.2016.
 */
public class TrainTask4 {
    Random rand;
    String lvlType;
    public void trainTask4(String lvlType,TextView questionText, Button  answer1, Button answer2,
                           ArrayList<String> engMas, ArrayList<String> rusMas, int currentTask
                           ){
        // Текущий тип уовня
        this.lvlType=lvlType;

        // Меняем answer1 и answer2 Для текущего заданий
        questionText.setVisibility(View.VISIBLE);
        answer1.setVisibility(View.VISIBLE);
        answer2.setVisibility(View.VISIBLE);
        answer1.setText("Да");
        answer2.setText("Нет");

        // Проверяем кол-во слов для вопроса, в зависимости от текущего вопроса
        int wordsLimit=8;
        if(!lvlType.equals("train")) {
            if (currentTask < 19) { // первые 5 слов
                wordsLimit = 5;
            }
            if (currentTask < 10) { // первые 3 слова
                wordsLimit = 3;
            }
        }

        // Подбираем вопрос для задания
        rand=new Random();

        // Определяем, правильный будет ответ или нет
        if(rand.nextBoolean()){
            questionText.setTag("Да");
            int wordNumber=getPossibleWord(wordsLimit,engMas,rusMas);
            questionText.setText(engMas.get(wordNumber)+" - "+rusMas.get(wordNumber));
            answer1.setTag(engMas.get(wordNumber));
        }else {
            questionText.setTag("Нет");
            int engNumber=getPossibleWord(wordsLimit,engMas,rusMas);
            int rusNumber=engNumber;

            while (rusNumber==engNumber){
                rusNumber=rand.nextInt(wordsLimit);
            }
            questionText.setText(engMas.get(engNumber)+" - "+rusMas.get(rusNumber));
            answer2.setTag(engMas.get(engNumber));
        }

    }

    // Получаем доступное английское слово по алгоритму
    public int getPossibleWord(int wordsLimit,
                               ArrayList<String> engMas,ArrayList<String> rusMas){

        if(lvlType.equals("common")||lvlType.equals("train")) {
            HashMap<String, Integer> wordsPackMap = CommonLevel.wordsPackMap;

            int possibleWord = rand.nextInt(wordsLimit);

            // Получаем нужное слово в зависимости от его повторения ранее
            while ((wordsPackMap.get(engMas.get(possibleWord)) == 3)) {
                possibleWord = rand.nextInt(wordsLimit);
            }

            wordsPackMap.put(engMas.get(possibleWord), wordsPackMap.get(engMas.get(possibleWord)) + 1);
            CommonLevel.wordsPackMap = wordsPackMap;

            return possibleWord;
        }else{
            ArrayList<Integer> usersWords = UnCommonLevel.usedWords;

            int possibleWord = rand.nextInt(engMas.size());

            // Получаем нужное слово в зависимости от его повторения ранее
            while (usersWords.contains(possibleWord)) {
                possibleWord = rand.nextInt(engMas.size());
            }

            usersWords.add(possibleWord);
            UnCommonLevel.usedWords = usersWords;

            return possibleWord;
        }
    }
}
