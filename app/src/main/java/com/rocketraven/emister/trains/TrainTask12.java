package com.rocketraven.emister.trains;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.paperdb.Paper;

/**
 * Created by Darkmonk on 19.05.2016.
 */
public class TrainTask12 {

    public void trainTask12(String taskJson, TextView questionText,
                                   TextView answerField,ArrayList<Button> allAnswers,String lvlType,
                                   int currentTaskNumber,boolean gramaType,TextView ruleTrue
                                   ){

        // Возвращяем видимость для кнопок ответов и других текстовых полей
        for (int i=0;i<allAnswers.size();i++){
            allAnswers.get(i).setVisibility(View.VISIBLE);
        }

        questionText.setVisibility(View.VISIBLE);
        answerField.setVisibility(View.VISIBLE);

        // Разбираем json на отдельные элементы
        try {
            JSONObject jsObject=new JSONObject(taskJson);
            JSONArray taskArray=jsObject.getJSONArray("results");
            JSONObject currentTask=null;
            // Тренировка правил
            if(gramaType){
                currentTask = (JSONObject) taskArray.get(currentTaskNumber);
            }else {
                currentTask = (JSONObject) taskArray.get(0);

                // Сохраняем изученные материалы грамматики
                ArrayList<String> saveGrama= Paper.book().read("saveGrama",new ArrayList<String>());
                String saveJson=taskArray.get(0).toString()+","+
                        taskArray.get(1).toString()+","+
                        taskArray.get(2).toString()+"," +
                        taskArray.get(3).toString();

                if (!saveGrama.contains(saveJson)){
                    saveGrama.add(saveJson);
                    Paper.book().write("saveGrama",saveGrama);
                }
            }

            // Добавляем вопрос
            questionText.setText(currentTask.getString("question"));

            // Добавляем часть ответа в поле ответ и правильный ответ в таг
            answerField.setText(currentTask.getString("sentence"));
            answerField.setTag(currentTask.getString("answer"));

            ruleTrue.setText(currentTask.getString("sentence").replaceAll("__",currentTask.getString("answer")));

            // Добавляем варианты ответов на кнопки
            String[] answersMas=currentTask.getString("answers").split("#");

            for (int i=0;i<answersMas.length;i++){
                allAnswers.get(i).setText(answersMas[i]);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
