package com.rocketraven.emister.trains;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Darkmonk on 14.05.2016.
 */
public class TrainTask2 {
    public static final int CARDS_COUNT=2;

    public static void trainTask2(ArrayList<String> engMas, ArrayList<String> rusMas,
                                  HashMap<String,String> wordsHint, ArrayList<TextView> allEngCardText,
                                  ArrayList<TextView> allRusCardText, ArrayList<TextView> allHintCardText,
                                  ArrayList<ImageView> allSoundIcon,
                                  FrameLayout cardFrame1,FrameLayout cardFrame2,
                                  FrameLayout cardFrame1Back,FrameLayout cardFrame2Back,
                                  ArrayList<ImageView> allHintIcons,ArrayList<ImageView> allHintIconBack){

        // Видимость карточек
        cardFrame1.setVisibility(View.VISIBLE);
        cardFrame2.setVisibility(View.VISIBLE);

        // Видимость подсказок для карточек
        cardFrame1Back.setVisibility(View.VISIBLE);
        cardFrame2Back.setVisibility(View.VISIBLE);

        // Меняем английский текст на карточках
        for (int i=0;i<CARDS_COUNT;i++) {
            allEngCardText.get(i).setText(engMas.get(i+3));

            // Меняем русский текст на карточках
            allRusCardText.get(i).setText(rusMas.get(i+3));

            // Меняем Таг в иконках озвучки
            allSoundIcon.get(i).setTag(engMas.get(i+3));
        }

        // Добавляем объяснения к словам
        for (Map.Entry<String, String> entry : wordsHint.entrySet()) {
            for(int i=0;i<CARDS_COUNT;i++){

                if(entry.getKey().toString().equals(engMas.get(i+3))){
                    Log.d("Проверочка",entry.getKey().toString()+"!"+entry.getValue().toString());
                    allHintCardText.get(i).setText(entry.getValue().toString());
                    allHintIcons.get(i).setVisibility(View.VISIBLE);
                    allHintIconBack.get(i).setVisibility(View.VISIBLE);
                }else{/*
                    allHintCardText.get(i).setText("");
                    allHintIcons.get(i).setVisibility(View.GONE);
                    allHintIconBack.get(i).setVisibility(View.GONE);*/
                }
            }

        }

    }
}
