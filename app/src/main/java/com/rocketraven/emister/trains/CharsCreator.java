package com.rocketraven.emister.trains;

import java.util.Random;

/**
 * Created by Darkmonk on 17.05.2016.
 */
public class CharsCreator {

    public static char[] charsCreator(int count){

        // Базовые буквы
        char[] basicLetters={'a','b','c','d','e','f','g','h','k','m','n','o','p','q',
        'r','s','t','x','y','z'};

        char[] additionalLetters=new char[count];
        Random rand=new Random();

        for (int i=0;i<additionalLetters.length;i++){
            additionalLetters[i]=basicLetters[rand.nextInt(basicLetters.length)];
        }

        return additionalLetters;
    }
}
