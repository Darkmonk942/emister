package com.rocketraven.emister.trains;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rocketraven.emister.CommonLevel;
import com.rocketraven.emister.R;
import com.rocketraven.emister.UnCommonLevel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.paperdb.Paper;

/**
 * Created by Darkmonk on 19.05.2016.
 */
public class TrainTask15 implements View.OnClickListener {
    String[] answersMas;
    TextView answerField;
    FrameLayout wordsFrame;
    ImageView deleteSymbol;
    String lvlType;
    public void trainTask15(String lvlType, int currentTaskQuestion, String taskJson, TextView questionText,
                            FrameLayout wordsFrame, ScrollView wordsScroll, TextView answerField,
                            ImageView deleteSymbol,Context context,boolean gramaType,TextView ruleTrue){
        // Текущий тип уовня
        this.lvlType=lvlType;
        this.deleteSymbol=deleteSymbol;

        // Получаем высоту и ширину экрана
        int height= Paper.book().read("height");
        int width= Paper.book().read("width");
        int tenPx= height/192;

        // Возвращаем видимость кнопкам
        questionText.setVisibility(View.VISIBLE);
        wordsScroll.setVisibility(View.VISIBLE);
        answerField.setVisibility(View.VISIBLE);
        deleteSymbol.setVisibility(View.VISIBLE);

        deleteSymbol.setOnClickListener(this);

        try {
            JSONObject jsObject=new JSONObject(taskJson);
            JSONArray taskArray=jsObject.getJSONArray("results");
            JSONObject currentTask=null;
            // Тренировка правил
            if(gramaType){
                currentTask = (JSONObject) taskArray.get(currentTaskQuestion);
            }else {
                currentTask = (JSONObject) taskArray.get(3);
            }

            // Добавляем вопрос и ответ в таг
            questionText.setText(currentTask.getString("question"));
            answerField.setTag(currentTask.getString("answer"));

            // Добавляем правильный ответ в повторения
            ruleTrue.setText(currentTask.getString("answer"));

            // Получаем все слова для сбора предложения
            answersMas=currentTask.getString("answers").split("#");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.answerField=answerField;
        this.wordsFrame=wordsFrame;

        // Создаём кнопки для составления предложения
        int coloumn=0;

        for (int i=0;i<answersMas.length;i++){

            Button but = new Button(context);
            FrameLayout.LayoutParams butParams = new FrameLayout.LayoutParams(width/3, tenPx*12);
            if(i%2==0){
                butParams.setMargins(tenPx*3,coloumn*(tenPx*15),0,0);
            }else{
                butParams.gravity= Gravity.RIGHT;
                butParams.setMargins(0,coloumn*(tenPx*15),tenPx*3,0);

                // Добавяе ещё одну строку в таблицу
                coloumn=coloumn+1;
            }

            but.setText(answersMas[i]);
            but.setTag(answersMas[i].toLowerCase());
            but.setTextColor(Color.parseColor("#ffffff"));
            but.setTextSize(TypedValue.COMPLEX_UNIT_PX,tenPx*4);
            but.setBackgroundResource(R.drawable.shape_round_button);
            but.setAllCaps(false);
            but.setIncludeFontPadding(false);
            but.setOnClickListener(this);

            wordsFrame.addView(but,butParams);
        }
    }

    @Override
    public void onClick(View v) {

        if(v instanceof Button) {
            Button but = (Button) v;

            // Проверка на первое сово в ответе
            if (answerField.getText().toString().equals("")) {
                answerField.setText(but.getText().toString());
            } else {
                answerField.setText(answerField.getText().toString() + " " + but.getText().toString());
            }

            // Убираем слово с фрейма
            but.setVisibility(View.GONE);

            // Проверка на правильность составления предложения
            if (answerField.getText().toString().equals(answerField.getTag().toString())) {
                if(lvlType.equals("common")||lvlType.equals("train")) {
                    CommonLevel.currentTaskAnswer = true;
                }else {
                    UnCommonLevel.currentTaskAnswer = true;}
            } else {
                if(lvlType.equals("common")||lvlType.equals("train")) {
                    CommonLevel.currentTaskAnswer = false;
                }else {UnCommonLevel.currentTaskAnswer = false;}
            }
        }else{
            // Получаем массив слов в текущем поле ответа
            String wordsMas[]=answerField.getText().toString().split(" ");

            for (int i=0;i<wordsFrame.getChildCount();i++){
                if(wordsMas[wordsMas.length-1].toLowerCase().equals(wordsFrame.getChildAt(i).getTag().toString())){
                    wordsFrame.getChildAt(i).setVisibility(View.VISIBLE);
                    if(wordsMas.length>1){
                        for (int j=0;j<wordsMas.length-1;j++){
                            if(j==0){
                                answerField.setText(wordsMas[0]);
                            }else{
                                answerField.setText(answerField.getText().toString()+" "+wordsMas[j]);
                            }
                        }
                    }else{
                        answerField.setText("");
                    }
                }
            }

            // Проверка на правильность составления предложения
            if (answerField.getText().toString().equals(answerField.getTag().toString())) {
                if(lvlType.equals("common")||lvlType.equals("train")) {
                    CommonLevel.currentTaskAnswer = true;
                }else {
                    UnCommonLevel.currentTaskAnswer = true;}
            } else {
                if(lvlType.equals("common")||lvlType.equals("train")) {
                    CommonLevel.currentTaskAnswer = false;
                }else {UnCommonLevel.currentTaskAnswer = false;}
            }

        }
    }
}
