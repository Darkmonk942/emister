package com.rocketraven.emister.trains;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rocketraven.emister.CommonLevel;
import com.rocketraven.emister.R;
import com.rocketraven.emister.UnCommonLevel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import io.paperdb.Paper;

/**
 * Created by Darkmonk on 18.05.2016.
 */
public class TrainTask9 implements View.OnClickListener {
    TextView answerField;
    Button questionSound;
    FrameLayout wordsFrame;
    ImageView deleteSymbol;
    Handler handler=new Handler();
    Random rand;
    String lvlType;
    public void trainTask9(String lvlType,final Button questionSound,FrameLayout wordsFrame,ScrollView wordsScroll,
                           ArrayList<String> engMas, ArrayList<String> rusMas, int currentTask,
                           TextView answerField,ImageView deleteSymbol, Context context){
        // Текущий тип уовня
        this.lvlType=lvlType;

        answerField.setVisibility(View.VISIBLE);
        deleteSymbol.setVisibility(View.VISIBLE);
        this.answerField=answerField;
        this.questionSound=questionSound;
        this.wordsFrame=wordsFrame;
        this.deleteSymbol=deleteSymbol;

        deleteSymbol.setOnClickListener(this);

        // Получаем высоту и ширину экрана
        int height= Paper.book().read("height");
        int width= Paper.book().read("width");
        int tenPx= height/192;

        // Возвращаем видимость фрейма для букв
        wordsScroll.setVisibility(View.VISIBLE);
        questionSound.setVisibility(View.VISIBLE);

        // Проверяем кол-во слов для вопроса, в зависимости от текущего вопроса
        int wordsLimit=8;
        if(!lvlType.equals("train")) {
            if (currentTask < 19) { // первые 5 слов
                wordsLimit = 5;
            }
            if (currentTask < 10) { // первые 3 слова
                wordsLimit = 3;
            }
        }

        // Подбираем вопрос для задания
        rand=new Random();

        // Рандомим вопрос и в таг добавляем ответ
        int randomQuestion=getPossibleWord(wordsLimit,engMas);

        // Удаляем частицы a,an,to
        String clearWord=engMas.get(randomQuestion);

        if(clearWord.length()>=4) {
            if (clearWord.substring(0, 2).equals("a ")) {
                clearWord = clearWord.substring(2, clearWord.length());
            }
            if (clearWord.substring(0, 3).equals("an ")) {
                clearWord = clearWord.substring(3, clearWord.length());
            }
            if (clearWord.substring(0, 3).equals("to ")) {
                clearWord = clearWord.substring(3, clearWord.length());
            }
        }

        answerField.setTag(clearWord);
        this.questionSound.setTag(clearWord);

        // Разбиваем правильный ответ на буквы
        char[] lettersMas=clearWord.toCharArray();

        // Определяем кол-во букв для сбора слова
        int wordsStartCount=0;
        if(lettersMas.length<15){
            wordsStartCount=15;
        }else{
            wordsStartCount=23;
        }
        char[] allLettersMas=new char[wordsStartCount];

        if(lettersMas.length<15){
            char[] additionalLetters=CharsCreator.charsCreator(15-lettersMas.length);
            System.arraycopy(lettersMas, 0, allLettersMas, 0, lettersMas.length);
            System.arraycopy(additionalLetters, 0, allLettersMas, lettersMas.length, additionalLetters.length);
        }else{
            char[] additionalLetters=CharsCreator.charsCreator(23-lettersMas.length);
            System.arraycopy(lettersMas, 0, allLettersMas, 0, lettersMas.length);
            System.arraycopy(additionalLetters, 0, allLettersMas, lettersMas.length, additionalLetters.length);
        }

        // Получаем Ширину Скрола
        FrameLayout.LayoutParams scrollParams=(FrameLayout.LayoutParams)wordsScroll.getLayoutParams();

        // Получаем размеры для кнопок-букв
        int letterWidth=tenPx*11;
        int letterMargin=(scrollParams.width-letterWidth*6)/7;
        int raw=0;
        int coloumn=0;

        // Перемешиваем Массив готовый массив букв
        for (int i=0;i<allLettersMas.length;i++){
            char shuffle;
            int randNumber=rand.nextInt(allLettersMas.length);
            shuffle=allLettersMas[i];
            allLettersMas[i]=allLettersMas[randNumber];
            allLettersMas[randNumber]=shuffle;
        }

        for (int i=0;i<allLettersMas.length;i++) {
            Button but = new Button(context);
            FrameLayout.LayoutParams butParams = new FrameLayout.LayoutParams(letterWidth, letterWidth);
            if(i==6||i==12||i==18){
                raw=0;
                coloumn=coloumn+1;
            }
            butParams.setMargins(letterMargin+raw*(letterMargin+letterWidth),coloumn*(letterMargin+letterWidth),0,0);
            wordsFrame.addView(but, butParams);
            but.setOnClickListener(this);
            but.setText(String.valueOf(allLettersMas[i]));
            but.setTextColor(Color.parseColor("#ffffff"));
            but.setTextSize(TypedValue.COMPLEX_UNIT_PX,tenPx*5);
            but.setBackgroundResource(R.drawable.shape_round_button);
            but.setPadding(0,0,0,0);
            but.setIncludeFontPadding(false);
            but.setAllCaps(false);
            raw=raw+1;
        }

        // Озвучиваем вопрос
        handler.postDelayed(new Runnable() {
            public void run() {
                questionSound.callOnClick();
            }
        }, 1000);


    }

    @Override
    public void onClick(View v) {
        if(v instanceof Button) {
            Button but = (Button) v;
            but.setVisibility(View.GONE);

            answerField.setText(answerField.getText().toString() + but.getText().toString());

            if (answerField.getText().toString().equals(questionSound.getTag().toString())) {
                if(lvlType.equals("common")||lvlType.equals("train")) {
                    CommonLevel.currentTaskAnswer = true;
                }else {UnCommonLevel.currentTaskAnswer = true;}
            } else {
                if(lvlType.equals("common")||lvlType.equals("train")) {
                    CommonLevel.currentTaskAnswer = false;
                }else {UnCommonLevel.currentTaskAnswer = false;}
            }
        }else{

            // Удаляем символ с поля ответов
            if(!answerField.getText().toString().equals("")){
                String oldText=answerField.getText().toString();
                String lastSymbol=oldText.substring(oldText.length()-1);
                oldText=oldText.substring(0,oldText.length()-1);
                answerField.setText(oldText);

                // Добавляем символ назад в поле с кнопками
                for (int i=0;i<wordsFrame.getChildCount();i++){
                    Button but=(Button)wordsFrame.getChildAt(i);
                    if(but.getVisibility()==View.GONE &&
                            but.getText().toString().toLowerCase().equals(lastSymbol.toLowerCase())){
                        but.setVisibility(View.VISIBLE);
                        return;
                    }
                }
            }
        }
    }

    // Получаем доступное английское слово по алгоритму
    public int getPossibleWord(int wordsLimit,
                               ArrayList<String> engMas){

        if(lvlType.equals("common")||lvlType.equals("train")) {
            HashMap<String, Integer> wordsPackMap = CommonLevel.wordsPackMap;

            int possibleWord = rand.nextInt(wordsLimit);

            // Получаем нужное слово в зависимости от его повторения ранее
            while ((wordsPackMap.get(engMas.get(possibleWord)) == 3)) {
                possibleWord = rand.nextInt(wordsLimit);
            }

            wordsPackMap.put(engMas.get(possibleWord), wordsPackMap.get(engMas.get(possibleWord)) + 1);
            CommonLevel.wordsPackMap = wordsPackMap;

            return possibleWord;
        }else{
            ArrayList<Integer> usersWords = UnCommonLevel.usedWords;

            int possibleWord = rand.nextInt(engMas.size());

            // Получаем нужное слово в зависимости от его повторения ранее
            while (usersWords.contains(possibleWord)) {
                possibleWord = rand.nextInt(engMas.size());
            }

            usersWords.add(possibleWord);
            UnCommonLevel.usedWords = usersWords;

            return possibleWord;
        }
    }
}
