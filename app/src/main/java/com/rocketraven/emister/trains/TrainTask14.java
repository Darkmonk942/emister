package com.rocketraven.emister.trains;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Darkmonk on 19.05.2016.
 */
public class TrainTask14 {

    public void trainTsk14(String taskJson, TextView questionText,
                                  ArrayList <Button> allAnswers,String lvlType,boolean gramaType,
                                  int currentTaskNumber,TextView ruleTrue){

        // Возвращяем видимость для кнопок ответов и других текстовых полей
        for (int i=0;i<allAnswers.size();i++){
            allAnswers.get(i).setVisibility(View.VISIBLE);
        }

        questionText.setVisibility(View.VISIBLE);

        try {
            JSONObject jsObject=new JSONObject(taskJson);
            JSONArray taskArray=jsObject.getJSONArray("results");
            JSONObject currentTask=null;
            // Тренировка правил
            if(gramaType){
                currentTask = (JSONObject) taskArray.get(currentTaskNumber);
            }else {
                currentTask = (JSONObject) taskArray.get(2);
            }

            // Добавляем вопрос и ответ в таг
            questionText.setText(currentTask.getString("question"));
            questionText.setTag(currentTask.getString("answer"));

            ruleTrue.setText(currentTask.getString("answer"));

            // Добавляем варианты ответов на кнопки
            String[] answersMas=currentTask.getString("answers").split("#");

            for (int i=0;i<answersMas.length;i++){
                allAnswers.get(i).setText(answersMas[i]);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
