package com.rocketraven.emister.trains;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rocketraven.emister.CommonLevel;
import com.rocketraven.emister.UnCommonLevel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by Darkmonk on 17.05.2016.
 */
public class TrainTask6 {
    Random rand;
    String lvlType;
    public void trainTask6(String lvlType,TextView questionText, ArrayList<Button> allAnswersButton,
                                  ArrayList<String> engMas, ArrayList<String> rusMas, int currentTask){
        // Текущий тип уовня
        this.lvlType=lvlType;

        // Возвращяем видимость для кнопок ответов
        for (int i=0;i<allAnswersButton.size();i++){
            allAnswersButton.get(i).setVisibility(View.VISIBLE);
        }

        questionText.setVisibility(View.VISIBLE);

        // Проверяем кол-во слов для вопроса, в зависимости от текущего вопроса
        int wordsLimit=8;
        if(!lvlType.equals("train")) {
            if (currentTask < 19) { // первые 5 слов
                wordsLimit = 5;
            }
            if (currentTask < 10) { // первые 3 слова
                wordsLimit = 3;
            }
        }

        // Подбираем вопрос для задания
        rand=new Random();

        // Рандомим вопрос и в таг добавляем ответ
        int randomQuestion=getPossibleWord(wordsLimit,engMas);
        questionText.setText(rusMas.get(randomQuestion));
        questionText.setTag(engMas.get(randomQuestion));

        // Рандомим варианты ответов
        HashSet<String> allAnswers=new HashSet();
        allAnswers.add(engMas.get(randomQuestion));

        while (allAnswers.size()!=4){
            allAnswers.add(engMas.get(rand.nextInt(engMas.size())));
        }

        // Создаём лист кнопок, мешаем и заполняем текстом
        Collections.shuffle(allAnswersButton);

        Iterator iteratorAnswers = allAnswers.iterator();
        Iterator iteratorButton = allAnswersButton.iterator();

        while (iteratorAnswers.hasNext()){
            Button but=(Button)iteratorButton.next();
            but.setText(iteratorAnswers.next().toString());
        }
    }

    // Получаем доступное английское слово по алгоритму
    public int getPossibleWord(int wordsLimit,
                               ArrayList<String> engMas){

        if(lvlType.equals("common")||lvlType.equals("train")) {
            HashMap<String, Integer> wordsPackMap = CommonLevel.wordsPackMap;

            int possibleWord = rand.nextInt(wordsLimit);

            // Получаем нужное слово в зависимости от его повторения ранее
            while ((wordsPackMap.get(engMas.get(possibleWord)) == 3)) {
                possibleWord = rand.nextInt(wordsLimit);
            }

            wordsPackMap.put(engMas.get(possibleWord), wordsPackMap.get(engMas.get(possibleWord)) + 1);
            CommonLevel.wordsPackMap = wordsPackMap;

            return possibleWord;
        }else{

            ArrayList<Integer> usersWords = UnCommonLevel.usedWords;

            int possibleWord = rand.nextInt(engMas.size());

            // Получаем нужное слово в зависимости от его повторения ранее
            while (usersWords.contains(possibleWord)) {
                possibleWord = rand.nextInt(engMas.size());
            }

            usersWords.add(possibleWord);
            UnCommonLevel.usedWords = usersWords;

            return possibleWord;
        }
    }
}
