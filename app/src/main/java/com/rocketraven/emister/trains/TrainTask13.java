package com.rocketraven.emister.trains;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rocketraven.emister.CommonLevel;
import com.rocketraven.emister.R;
import com.rocketraven.emister.UnCommonLevel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import io.paperdb.Paper;

/**
 * Created by Darkmonk on 19.05.2016.
 */
public class TrainTask13 implements View.OnClickListener{
    String answerText="";
    String emptyAnswer="";
    String startQuestion="";
    TextView answerField;
    FrameLayout wordsFrame;
    ImageView deleteSymbol;
    String lvlType;
    public void trainTask13(String lvlType,String taskJson, TextView questionText, FrameLayout wordsFrame,
                            ScrollView wordsScroll, TextView answerField,ImageView deleteSymbol,
                            Context context,boolean gramaType,int currentTaskNumber,TextView ruleTrue){
        // Текущий тип уовня
        this.lvlType=lvlType;

        // Возвращяем видимость для кнопок ответов и других текстовых полей
        questionText.setVisibility(View.VISIBLE);
        answerField.setVisibility(View.VISIBLE);
        wordsScroll.setVisibility(View.VISIBLE);
        deleteSymbol.setVisibility(View.VISIBLE);

        this.wordsFrame=wordsFrame;
        this.answerField=answerField;
        this.deleteSymbol=deleteSymbol;

        deleteSymbol.setOnClickListener(this);

        // Получаем высоту и ширину экрана
        int height= Paper.book().read("height");
        int width= Paper.book().read("width");
        int tenPx= height/192;

        // Разбираем json на отдельные элементы
        try {
            JSONObject jsObject=new JSONObject(taskJson);
            JSONArray taskArray=jsObject.getJSONArray("results");

            JSONObject currentTask=null;
            // Тренировка правил
            if(gramaType){
                currentTask = (JSONObject) taskArray.get(currentTaskNumber);
            }else {
                currentTask = (JSONObject) taskArray.get(1);
            }

            // Добавляем вопрос
            questionText.setText(currentTask.getString("question"));
            startQuestion=currentTask.getString("sentence");

            // Добавляем часть ответа в поле ответ и правильный ответ в таг
            answerField.setText(currentTask.getString("sentence"));
            emptyAnswer=currentTask.getString("sentence");
            answerField.setTag(currentTask.getString("answer"));

            // Добавляем правильный ответ в поле повторения
            ruleTrue.setText(currentTask.getString("sentence").replaceAll("__",currentTask.getString("answer")));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Подбираем вопрос для задания
        Random rand=new Random();

        // Разбиваем правильный ответ на буквы
        char[] lettersMas=answerField.getTag().toString().toCharArray();

        // Определяем кол-во букв для сбора слова
        char[] allLettersMas=new char[15];

        if(lettersMas.length<15){
            char[] additionalLetters=CharsCreator.charsCreator(15-lettersMas.length);
            System.arraycopy(lettersMas, 0, allLettersMas, 0, lettersMas.length);
            System.arraycopy(additionalLetters, 0, allLettersMas, lettersMas.length, additionalLetters.length);
        }

        // Получаем Ширину Скрола
        FrameLayout.LayoutParams scrollParams=(FrameLayout.LayoutParams)wordsScroll.getLayoutParams();

        // Получаем размеры для кнопок-букв
        int letterWidth=tenPx*11;
        int letterMargin=(scrollParams.width-letterWidth*6)/7;
        int raw=0;
        int coloumn=0;

        // Перемешиваем Массив готовый массив букв
        for (int i=0;i<allLettersMas.length;i++){
            char shuffle;
            int randNumber=rand.nextInt(allLettersMas.length);
            shuffle=allLettersMas[i];
            allLettersMas[i]=allLettersMas[randNumber];
            allLettersMas[randNumber]=shuffle;
        }

        for (int i=0;i<allLettersMas.length;i++) {
            Button but = new Button(context);
            FrameLayout.LayoutParams butParams = new FrameLayout.LayoutParams(letterWidth, letterWidth);
            if(i==6||i==12||i==18){
                raw=0;
                coloumn=coloumn+1;
            }
            butParams.setMargins(letterMargin+raw*(letterMargin+letterWidth),coloumn*(letterMargin+letterWidth),0,0);
            but.setOnClickListener(this);
            but.setText(String.valueOf(allLettersMas[i]));
            but.setTextColor(Color.parseColor("#ffffff"));
            but.setTextSize(TypedValue.COMPLEX_UNIT_PX,tenPx*5);
            but.setBackgroundResource(R.drawable.shape_round_button);
            but.setIncludeFontPadding(false);
            but.setPadding(0,0,0,0);
            but.setAllCaps(false);
            raw=raw+1;
            wordsFrame.addView(but, butParams);
        }
    }

    @Override
    public void onClick(View v) {
        Log.d("Удаление","Клик");
        if(v instanceof Button) {
            Button but = (Button) v;
            but.setVisibility(View.GONE);

            answerText = answerText + but.getText().toString();
            String newText=emptyAnswer.replace("__",answerText);

            answerField.setText(newText);
            wordsFrame.setTag(answerText);

            if (answerText.toLowerCase().equals(answerField.getTag().toString().toLowerCase())) {

                if(lvlType.equals("common")||lvlType.equals("train")) {
                    Log.d("Проверочка1",answerText.toLowerCase() +" - "+answerField.getTag().toString().toLowerCase());
                    CommonLevel.currentTaskAnswer = true;
                }else {
                    UnCommonLevel.currentTaskAnswer = true;}
            } else {
                if(lvlType.equals("common")||lvlType.equals("train")) {
                    Log.d("Проверочка2",answerText.toLowerCase() +" - "+answerField.getTag().toString().toLowerCase());
                    CommonLevel.currentTaskAnswer = false;
                }else {UnCommonLevel.currentTaskAnswer = false;}
            }
        }else{
            // Удаление последнего введённого символа
            if(!answerText.equals("")) {

                // Добавляем символ назад в поле с кнопками
                String lastSymbol=answerText.substring(answerText.length()-1);
                Log.d("Удаление",lastSymbol);
                for (int i=0;i<wordsFrame.getChildCount();i++){
                    Button but=(Button)wordsFrame.getChildAt(i);
                    if(but.getVisibility()==View.GONE &&
                            but.getText().toString().toLowerCase().equals(lastSymbol.toLowerCase())){
                        but.setVisibility(View.VISIBLE);
                        break;
                    }
                }
                answerText = answerText.substring(0, answerText.length()-1);
                String newText=emptyAnswer.replace("__",answerText);

                answerField.setText(newText);
                wordsFrame.setTag(answerText);
            }
            if(answerText.equals("")){
                answerField.setText(startQuestion);
            }
        }
    }
}
