package com.rocketraven.emister.trains;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rocketraven.emister.EmisterApp;

import io.paperdb.Paper;

/**
 * Created by Darkmonk on 14.05.2016.
 */
public class TrainTask3 {

    public static void trainTask3(TextView ruleHeader, LinearLayout ruleInnerFrame,
                                  String[] levelRule, Context context){

        // Меняем название правила
        ruleHeader.setText(levelRule[0]);
        int height= Paper.book().read("height");
        int width= Paper.book().read("width");
        int tenPxHeight=height/192;

        // Добавляем на фрейм список текстов и примеров
        for (int i=1;i<levelRule.length;i=i+2){
            try {
                // Создаём текст правила и пример
                TextView ruleText = new TextView(context);
                TextView ruleExample = new TextView(context);
                ImageView line = new ImageView(context);

                // Параметры текста правила
                LinearLayout.LayoutParams ruleTextParams = new LinearLayout.LayoutParams(width - tenPxHeight * 30,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                ruleTextParams.setMargins(0, tenPxHeight * 3, 0, 0);
                ruleTextParams.gravity = Gravity.CENTER_HORIZONTAL;
                ruleText.setGravity(Gravity.CENTER);
                ruleText.setText(Html.fromHtml(levelRule[i]));
                ruleText.setTypeface(EmisterApp.robotoCondensed);
                ruleText.setPadding(tenPxHeight * 4, 0, tenPxHeight * 4, 0);
                ruleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, tenPxHeight * 5);

                // Параметры текста примера
                LinearLayout.LayoutParams ruleExampleParams = new LinearLayout.LayoutParams(width - tenPxHeight * 30,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                ruleExampleParams.setMargins(0, tenPxHeight * 3, 0, 0);
                ruleExampleParams.gravity = Gravity.CENTER_HORIZONTAL;
                ruleExample.setText(Html.fromHtml(levelRule[i + 1]));
                ruleExample.setGravity(Gravity.CENTER);
                ruleExample.setTypeface(EmisterApp.robotoLight);
                ruleExample.setTextSize(TypedValue.COMPLEX_UNIT_PX, tenPxHeight * 5);
                ruleExample.setPadding(tenPxHeight * 2, 0, tenPxHeight * 2, 0);

                // Разделительная линия
                LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams((width / 3) * 2, 1);
                lineParams.setMargins(0, tenPxHeight, 0, 0);
                lineParams.gravity = Gravity.CENTER_HORIZONTAL;
                line.setBackgroundColor(Color.parseColor("#009688"));

                // Добавляем на фрейм элементы
                ruleInnerFrame.addView(ruleText, ruleTextParams);
                ruleInnerFrame.addView(ruleExample, ruleExampleParams);
                ruleInnerFrame.addView(line, lineParams);
            }catch (Exception e){}
        }
    }
}
