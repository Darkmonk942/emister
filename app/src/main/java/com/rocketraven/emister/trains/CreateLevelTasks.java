package com.rocketraven.emister.trains;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Darkmonk on 13.05.2016.
 */
public class CreateLevelTasks {

    // Создаём набор заданий для обычного уровня
    public ArrayList<Integer> createTasks(){
        ArrayList<Integer> tasks=new ArrayList();
        Random rand=new Random();
        // Создаём набор заданий для обычного уровня
        for (int i=1;i<31;i++){
            int currentTask=0;
            if(i!=1){
                do{
                    currentTask=rand.nextInt(7)+4;
                } while (currentTask==tasks.get(tasks.size()-1));
            }

            switch (i){
                case 1: currentTask=1;
                    break;
                case 2: currentTask=4; //4
                    break;
                case 10: currentTask=2;
                    break;
                case 13: currentTask=3;
                    break;
                case 14: currentTask=12;
                    break;
                case 16: currentTask=13;
                    break;
                case 18: currentTask=14;
                    break;
                case 19: currentTask=1;
                    break;
                case 25: currentTask=15;
                    break;
                case 30: currentTask=11;
                    break;
            }
            tasks.add(currentTask);
            Log.d("Задание", String.valueOf(currentTask));
        }
        return tasks;
    }

    // Создаём набор заданий для уровня на время
    public ArrayList<Integer> createUnCommonTasks(){
        ArrayList<Integer> tasks=new ArrayList();
        Random rand=new Random();
        // Создаём набор заданий для обычного уровня
        for (int i=1;i<26;i++){
            int currentTask=0;
            if(i!=1){
                do{
                    currentTask=rand.nextInt(7)+4;
                } while (currentTask==tasks.get(tasks.size()-1));
            }else{
                currentTask=rand.nextInt(7)+4;
            }

            switch (i){
                case 1: currentTask=4; //4
                    break;
                case 5: currentTask=12;
                    break;
                case 10: currentTask=13;
                    break;
                case 15: currentTask=14;
                    break;
                case 20: currentTask=15;
                    break;
                case 25: currentTask=11;
                    break;
            }
            tasks.add(currentTask);
            Log.d("Задание", String.valueOf(currentTask));
        }
        return tasks;
    }

    // Создаём набор заданий для тренировок слов
    public ArrayList<Integer> createWordsTrainTasks(){
        ArrayList<Integer> tasks=new ArrayList();
        Random rand=new Random();
        // Создаём набор заданий для обычного уровня
        for (int i=1;i<21;i++){
            int currentTask=0;
            if(i!=1){
                do{
                    currentTask=rand.nextInt(7)+4;
                } while (currentTask==tasks.get(tasks.size()-1));
            }else{
                currentTask=rand.nextInt(7)+4;
            }

            tasks.add(currentTask);
            Log.d("Задание", String.valueOf(currentTask));
        }
        return tasks;
    }

    // Создаём набор заданий для тренировок грамматики
    public ArrayList<Integer> createGramaTrainTasks(){
        ArrayList<Integer> tasks=new ArrayList();

        // Создаём набор заданий для обычного уровня
        tasks.add(12); tasks.add(13); tasks.add(14); tasks.add(15); tasks.add(12);
        tasks.add(13); tasks.add(14); tasks.add(15); tasks.add(12); tasks.add(13);

        return tasks;
    }
}
