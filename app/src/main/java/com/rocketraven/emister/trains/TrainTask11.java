package com.rocketraven.emister.trains;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rocketraven.emister.CommonLevel;
import com.rocketraven.emister.EmisterApp;
import com.rocketraven.emister.EndOfLevelActivity;
import com.rocketraven.emister.UnCommonLevel;

import java.util.ArrayList;
import java.util.Collections;

import io.paperdb.Paper;

/**
 * Created by Darkmonk on 18.05.2016.
 */

public class TrainTask11 implements View.OnClickListener {
    Context context;
    Handler handler=new Handler();
    int answers=0;
    int buttonDraw;
    int buttonSelect;
    int buttonTrue;
    int buttonFalse;
    boolean choser=false;
    Button prevButton;
    String lvlType;
    ArrayList<Button> allButtons=new ArrayList();
    ArrayList<String> engMas=new ArrayList();
    ArrayList<String> rusMas=new ArrayList();
    TextView livesText;
    int levelLimit=0;
    public void trainTask11(String lvlType, TextView livesText,ArrayList<String> engMas, ArrayList<String> rusMas,
                            LinearLayout leftLinear, LinearLayout rightLinear,
                            Context context,
                            int buttonDraw, int buttonSelect, int buttonTrue, int buttonFalse){
        // Текущий тип уовня
        this.lvlType=lvlType;

        // Получаем шейпы и анимацию
        this.buttonDraw=buttonDraw;
        this.buttonSelect=buttonSelect;
        this.buttonTrue=buttonTrue;
        this.buttonFalse=buttonFalse;
        this.engMas=engMas;
        this.rusMas=rusMas;
        levelLimit=engMas.size();
        // Глобальный контекст и Активти
        this.context=context;
        this.livesText=livesText;

        // Получаем параметры экрана
        int tenPxHeight= Paper.book().read("height",0)/192;

        // Возвращаем видимость Linear-ов
        leftLinear.setVisibility(View.VISIBLE);
        rightLinear.setVisibility(View.VISIBLE);

        ArrayList<Button> rusButList=new ArrayList();
        ArrayList<Button> engButList=new ArrayList();

        // Если уровень на время, то выбираем 10 оставшихся неиспользованных слов
        if(lvlType.equals("uncommon")){

            ArrayList<Integer> usersWords = UnCommonLevel.usedWords;
            ArrayList<String> newEngMas=new ArrayList();
            ArrayList<String> newRusMas=new ArrayList();

            // Составляем новые массивы из неиспользованных слов
            for (int i=0;i<engMas.size();i++){
                boolean addNewWord=true;
                for (int j=0;j<usersWords.size();j++){
                    if(i==usersWords.get(j)){
                        addNewWord=false;
                    }
                }
                if(addNewWord){
                    newEngMas.add(engMas.get(i));
                    newRusMas.add(rusMas.get(i));
                }
            }

            engMas=newEngMas;
            rusMas=newRusMas;

            levelLimit=engMas.size();
        }

        // Заполняем лайауты
        for (int i=0;i<engMas.size();i++){

            // Создаём кнопки
            Button engWord=new Button(context);
            Button rusWord=new Button(context);

            // Добавляем переносы в русский текст
            String rusText=rusMas.get(i);
            if(rusText.contains("(")){
               rusText=rusText.replace("(","<br/>(");
            }

            // Добавляем английские слова
            LinearLayout.LayoutParams engWordParams=new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,tenPxHeight*11);
            engWordParams.setMargins(0,(tenPxHeight*5)/2,0,0);
            engWord.setLayoutParams(engWordParams);
            engWord.setText(engMas.get(i));
            engWord.setTag(Html.fromHtml(rusText));
            engWord.setGravity(Gravity.CENTER);
            engWord.setTypeface(EmisterApp.robotoLight);
            engWord.setTextSize(TypedValue.COMPLEX_UNIT_PX,tenPxHeight*4);
            engWord.setIncludeFontPadding(false);
            engWord.setPadding((tenPxHeight*3)/2,tenPxHeight/2,(tenPxHeight*3)/2,tenPxHeight/2);
            engWord.setTextColor(Color.WHITE);
            engWord.setBackgroundResource(buttonDraw);
            engWord.setOnClickListener(this);
            engWord.setAllCaps(false);
            engButList.add(engWord);

            allButtons.add(engWord);

            // Добавляем русские кнопки
            LinearLayout.LayoutParams rusWordParams=new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,tenPxHeight*11);
            rusWordParams.setMargins(0,(tenPxHeight*5)/2,0,0);
            rusWord.setLayoutParams(rusWordParams);
            rusWord.setText(Html.fromHtml(rusText));
            rusWord.setTag(engMas.get(i));
            rusWord.setGravity(Gravity.CENTER);
            rusWord.setTypeface(EmisterApp.robotoLight);
            rusWord.setTextSize(TypedValue.COMPLEX_UNIT_PX,tenPxHeight*4);
            rusWord.setIncludeFontPadding(false);
            rusWord.setPadding((tenPxHeight*3)/2,tenPxHeight/2,(tenPxHeight*3)/2,tenPxHeight/2);
            rusWord.setTextColor(Color.WHITE);
            rusWord.setBackgroundResource(buttonDraw);
            rusWord.setOnClickListener(this);
            rusWord.setAllCaps(false);
            rusButList.add(rusWord);
            allButtons.add(rusWord);
        }

        // Перемешиваем кнопки
        Collections.shuffle(rusButList);
        Collections.shuffle(engButList);

        // Добавляем на экран
        for (int i=0;i<rusButList.size();i++){
            leftLinear.addView(engButList.get(i));
            rightLinear.addView(rusButList.get(i));
        }
    }

    @Override
    public void onClick(final View v) {
        if(choser) {
            choser = false;

            // Если нажали на ту же кнопку!
            if (v.getTag().toString().equals(prevButton.getTag().toString())) {
                v.setBackgroundResource(buttonDraw);
            } else{

                // Проверка на правильность ответа
                String firstWord= String.valueOf(Html.fromHtml(v.getTag().toString()));
                String secondWord=String.valueOf(Html.fromHtml(prevButton.getText().toString()));
                Log.d("ДваСлова",firstWord+" ! "+secondWord);
                if (firstWord.equals(secondWord)) {

                    v.setBackgroundResource(buttonTrue);
                    prevButton.setBackgroundResource(buttonTrue);

                    // Анимация правильного ответа
                    clickPosible(false);

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            v.setVisibility(View.GONE);
                            prevButton.setVisibility(View.GONE);
                            clickPosible(true);
                            answers=answers+1;
                            // Проверка на окончание уровня
                            if(answers==levelLimit) {
                                endOfTheLevel();
                            }
                        }
                    }, 500);
                } else {
                    v.setBackgroundResource(buttonFalse);
                    prevButton.setBackgroundResource(buttonFalse);
                    clickPosible(false);

                    // Отнимаем жизни
                    if(lvlType.equals("common")||lvlType.equals("train")){
                        int lives=CommonLevel.lives-1;
                        int levelRating=CommonLevel.levelRating+1;
                        livesText.setText("x"+String.valueOf(lives));
                        CommonLevel.lives=lives;
                        CommonLevel.levelRating=levelRating;
                        if(lives==0){
                            ((CommonLevel)context).showAlertLives();
                        }
                    }else{
                        int lives=UnCommonLevel.lives-1;
                        int levelRating=UnCommonLevel.levelRating+1;
                        livesText.setText("x"+String.valueOf(lives));
                        UnCommonLevel.lives=lives;
                        UnCommonLevel.levelRating=levelRating;
                        if(lives==0){
                            ((UnCommonLevel)context).showAlertLives();
                        }
                    }

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            v.setBackgroundResource(buttonDraw);
                            prevButton.setBackgroundResource(buttonDraw);
                            clickPosible(true);
                        }
                    }, 500);
                }
        }
        }else{
            v.setBackgroundResource(buttonSelect);
            prevButton=(Button)v;
            choser=true;
        }
    }

    // Вкл \ выкл клик на кнопки
    public void clickPosible(boolean clickable){

        for (int i=0;i<allButtons.size();i++){
            allButtons.get(i).setClickable(clickable);
        }
    }

    // Окончание уровня
    public void endOfTheLevel(){
                if(lvlType.equals("common")) {
                    saveLearnedWords();
                    Intent myIntent = new Intent(context, EndOfLevelActivity.class)
                            .putExtra("rating", CommonLevel.levelRating)
                            .putExtra("currentLvl",CommonLevel.currentIntentLvl)
                            .putExtra("currentWorld",CommonLevel.currentIntentWorld)
                            .putExtra("lvlType","common")
                            .putExtra("lvlName",CommonLevel.currentLvlName);
                    context.startActivity(myIntent);
                    ((Activity)context).finish();
                }else{
                    Intent myIntent = new Intent(context, EndOfLevelActivity.class)
                            .putExtra("rating", UnCommonLevel.levelRating)
                            .putExtra("currentLvl",UnCommonLevel.currentIntentLvl)
                            .putExtra("currentWorld",UnCommonLevel.currentIntentWorld)
                            .putExtra("lvlType","uncommon");
                    context.startActivity(myIntent);
                    ((Activity)context).finish();
                }

    }

    // Сохраняем изученные слова в базу освоенных слов
    public void saveLearnedWords(){
            // Создаём пары английских и русских слов
            ArrayList<Pair<String,String>> masPair=new ArrayList();

            for (int i=0;i<engMas.size();i++){
                Pair <String,String> newWordsPair=new Pair(engMas.get(i),rusMas.get(i));
                masPair.add(newWordsPair);
            }
            // Добавляем слова, изученные в этом уровне в базу усвоенных
             ArrayList<Pair<String,String>> learnedWordsBase=
                    Paper.book().read("learnedWordsBD",new ArrayList<Pair<String,String>>());
            learnedWordsBase.addAll(masPair);
            Paper.book().write("learnedWordsBD",learnedWordsBase);
            Log.d("Список слов", String.valueOf(learnedWordsBase.size()));
    }
}
