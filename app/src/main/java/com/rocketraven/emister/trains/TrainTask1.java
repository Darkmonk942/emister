package com.rocketraven.emister.trains;

import android.media.Image;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.rocketraven.emister.CommonLevel;
import com.rocketraven.emister.objects.WorldObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

/**
 * Created by Darkmonk on 13.05.2016.
 *
 */

// 4 карточки для изучения новых слов
public class TrainTask1 {

    public static final int CARDS_COUNT=3;

    public static void trainTask1(int currentTask, ArrayList<String> engMas, ArrayList<String> rusMas,
                                  ArrayList<ImageView> allHintIcons,
                                  HashMap<String,String> wordsHint, ArrayList<TextView> allEngCardText,
                                  ArrayList<TextView> allRusCardText,
                                  ArrayList<ImageView> allSoundIcon,
                                  FrameLayout cardFrame1, FrameLayout cardFrame2,
                                  FrameLayout cardFrame3, FrameLayout cardFrame4,
                                  FrameLayout cardFrame1Back, FrameLayout cardFrame2Back,
                                  FrameLayout cardFrame3Back, FrameLayout cardFrame4Back,
                                  ArrayList<TextView> allHintBack, ArrayList<ImageView> allHintIconBack){

        // Видимость фрейма карточек
        cardFrame1.setVisibility(View.VISIBLE);
        cardFrame2.setVisibility(View.VISIBLE);
        cardFrame3.setVisibility(View.VISIBLE);

        // Видимость фрейма подсказок для слов
        cardFrame1Back.setVisibility(View.VISIBLE);
        cardFrame2Back.setVisibility(View.VISIBLE);
        cardFrame3Back.setVisibility(View.VISIBLE);

        if(currentTask==0) {
            // Меняем английский текст на карточках для первых 4 слов
            for (int i = 0; i < CARDS_COUNT; i++) {
                allEngCardText.get(i).setText(engMas.get(i));

                // Меняем русский текст на карточках
                allRusCardText.get(i).setText(rusMas.get(i));

                // Меняем Таг в иконках озвучки
                allSoundIcon.get(i).setTag(engMas.get(i));
            }

            // Добавляем объяснения к словам
            for (Map.Entry<String, String> entry : wordsHint.entrySet()) {
                for (int i = 0; i < CARDS_COUNT; i++) {
                    if (entry.getKey().toString().equals(engMas.get(i))) {
                        allHintBack.get(i).setText(entry.getValue().toString());
                        allHintIcons.get(i).setVisibility(View.VISIBLE);
                        allHintIconBack.get(i).setVisibility(View.VISIBLE);
                    }
                }
            }
        }else{

            // Меняем английский текст на карточках для последних 4-х слов
            for (int i = 0; i < CARDS_COUNT; i++) {
                allEngCardText.get(i).setText(engMas.get(i+5));

                // Меняем русский текст на карточках
                allRusCardText.get(i).setText(rusMas.get(i+5));

                // Меняем Таг в иконках озвучки
                allSoundIcon.get(i).setTag(engMas.get(i+5));
            }

            // Добавляем объяснения к словам
            for (Map.Entry<String, String> entry : wordsHint.entrySet()) {
                for (int i = 0; i < CARDS_COUNT; i++) {
                    if (entry.getKey().toString().equals(engMas.get(i+5))) {
                        allHintBack.get(i).setText(entry.getValue().toString());
                        allHintIcons.get(i).setVisibility(View.VISIBLE);
                        allHintIconBack.get(i).setVisibility(View.VISIBLE);
                    }
                }

            }

        }
    }
}
