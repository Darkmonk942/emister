package com.rocketraven.emister;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.rocketraven.emister.all_worlds.World1Activity;
import com.rocketraven.emister.all_worlds.World2Activity;
import com.rocketraven.emister.all_worlds.World3Activity;
import com.rocketraven.emister.all_worlds.World4Activity;
import com.rocketraven.emister.all_worlds.World5Activity;
import com.rocketraven.emister.all_worlds.World6Activity;
import com.rocketraven.emister.all_worlds.World7Activity;
import com.rocketraven.emister.all_worlds.World8Activity;
import com.rocketraven.emister.objects.LevelObject;
import com.rocketraven.emister.objects.WorldObject;
import com.rocketraven.emister.optimization.Optimization;
import com.rocketraven.emister.retrofit.RetrofitRequestCallback;
import com.rocketraven.emister.retrofit.requests.GetWorldRequest;
import com.rocketraven.emister.retrofit.requests.RetrofitMainClass;
import com.rocketraven.emister.service.BackgroundMainTheme;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Darkmonk on 09.05.2016.
 */
public class AdventureListActivity extends AppCompatActivity {
    // LinearLayout
    @BindView(R.id.main_linear) LinearLayout mainLinear;
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // ProgressBar
    @BindView(R.id.progress_circle) ProgressBar progressBar;
    // NativeExpressAdView
    @BindView(R.id.adView) NativeExpressAdView nativeAdsView;
    // TextView
    @BindView(R.id.progress1) TextView progress1;
    @BindView(R.id.progress2) TextView progress2;
    @BindView(R.id.progress3) TextView progress3;
    @BindView(R.id.progress4) TextView progress4;
    @BindView(R.id.progress5) TextView progress5;
    @BindView(R.id.progress6) TextView progress6;
    @BindView(R.id.progress7) TextView progress7;
    @BindView(R.id.progress8) TextView progress8;
    // Примитивы
    RetrofitMainClass retrofitMainClass;
    GetWorldRequest getWorldRequest;
    ArrayList<String> allWorldsName=new ArrayList();
    ArrayList<TextView> allProgressText=new ArrayList();
    final String BASIC_LEVEL="world1";
    final String EASY_LEVEL1="world2";
    final String EASY_LEVEL2="world3";
    final String MEDIUM_LEVEL1="world4";
    final String MEDIUM_LEVEL2="world5";
    final String HARD_LEVEL1="world6";
    final String HARD_LEVEL2="world7";
    final String HARD_LEVEL3="world8";
    boolean backPressed=false;
    boolean nextScreen=false;
    boolean soundOn;
    boolean worldClick=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adventure_list);
        ButterKnife.bind(this);

        // Подгружаем ретрофит
        retrofitMainClass=new RetrofitMainClass(this);

        // Запрос на получение всей инфы о выбранном приключении
        getWorldRequest=retrofitMainClass.retrofit.create(GetWorldRequest.class);

        // Определяем параметры звука
        soundOn=Paper.book("settings").read("sound",true);

        // Заполняем лист прогресса текста
        allProgressText.add(progress1); allProgressText.add(progress2); allProgressText.add(progress3);
        allProgressText.add(progress4); allProgressText.add(progress5); allProgressText.add(progress6);
        allProgressText.add(progress7); allProgressText.add(progress8);

        allWorldsName.add("world1"); allWorldsName.add("world2"); allWorldsName.add("world3");
        allWorldsName.add("world4"); allWorldsName.add("world5"); allWorldsName.add("world6");
        allWorldsName.add("world7"); allWorldsName.add("world8");

        // Оптимизация интерфейса
        Optimization optim=new Optimization();
        optim.OptimizationLinear(mainLinear);
        optim.Optimization(mainFrame);

        // Проверяем прогресс каждого уровня
        checkAllLvlProgress();

        // Подгружаем Нативную рекламу если нет премиума
        if (!Paper.book("inapp").read("premium",false)) {
            nativeAdsView.setVisibility(View.VISIBLE);
            nativeAdsView.loadAd(new AdRequest.Builder().build());
            nativeAdsView.setFocusable(false);
        }
    }

    // Переход на главный экран по кнопке назад
    public void backClick(View v){
        backPressed=true;
        finish();
    }

    // Определяем переход на выбранный уровень
    public void adventureClick(View v){
        if(!worldClick){
            return;
        }
        worldClick=false;

        nextScreen=true;
        switch (v.getTag().toString()){
            case "basicLevel":
                getAdventureBD(BASIC_LEVEL,World1Activity.class);
                metricaEvent("World 1");
                break;
            case "easyWorld1":
                getAdventureBD(EASY_LEVEL1,World2Activity.class);
                metricaEvent("World 2");
                break;
            case "easyWorld2":
                getAdventureBD(EASY_LEVEL2,World3Activity.class);
                metricaEvent("World 3");
                break;
            case "averageWorld1":
                getAdventureBD(MEDIUM_LEVEL1,World4Activity.class);
                metricaEvent("World 4");
                break;
            case "averageWorld2":
                getAdventureBD(MEDIUM_LEVEL2,World5Activity.class);
                metricaEvent("World 5");
                break;
            case "hardWorld1":
                getAdventureBD(HARD_LEVEL1,World6Activity.class);
                metricaEvent("World 6");
                break;
            case "hardWorld2":
                getAdventureBD(HARD_LEVEL2,World7Activity.class);
                metricaEvent("World 7");
                break;
            case "hardWorld3":
                getAdventureBD(HARD_LEVEL3,World8Activity.class);
                metricaEvent("World 8");
                break;
        }
    }

    // Выкачиваем с сервера всю информацию приключения
    public void getAdventureBD(final String myTableName,final Class className ){

        // Проверка на интернет
        if(!checkInternet()){
            if(Paper.book(myTableName).read("world",null)!=null){
                // Переходим на карту мира выбранного приключения
                startActivity(new Intent(AdventureListActivity.this, className));

            }else{
                createToast("Для загрузки текущего уровня отсутствует интернет соединение");
                worldClick=true;
            }
            return;
        }
        // Появляется прогресс бар и надпись "идёт загрузка"
        progressBar.setVisibility(View.VISIBLE);
        createToast("Идёт загрузка текущего приключения");

        Call<List<LevelObject>> getCurrentWorld=getWorldRequest.getWorldInfo("world/"+myTableName);
        retrofitMainClass.createRequest(getCurrentWorld);

        retrofitMainClass.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object response) {

                // Убираем прогресс бар
                progressBar.setVisibility(View.GONE);
                worldClick=true;

                // Обработка полученного ответа
                Response<List<LevelObject>> worldLevels = (Response<List<LevelObject>>)response;

                // 200 ответ при получении приключения
                if(worldLevels.code()==200){
                    List<LevelObject> allLevels= worldLevels.body();
                    WorldObject worldObject=new WorldObject();
                    worldObject.lvlCount=allLevels.size();

                    for (int i=0;i<allLevels.size();i++){
                        LevelObject levelObject=allLevels.get(i);

                        // Получаем массив данных для отображения правил
                        levelObject.levelRule=levelObject.levelRuleSplit.split("#");

                        // Собираем в пары подсказки для слов
                        String[] hintsPair=levelObject.levelWordsHintSplit.split("#");

                        for (int j=0;j<hintsPair.length;j=j+2){
                            levelObject.levelWordsHint.put(hintsPair[j],hintsPair[j+1]);
                        }

                        // Тренировки правил
                        levelObject.levelRuleTrain=levelObject.levelRuleTrainJson.toString();

                        worldObject.allLevels.add(levelObject);
                    }

                    // Сохраняем всё приключение в базу
                    worldObject.worldName=myTableName;
                    Paper.book(myTableName).write("world",worldObject);

                    // Переходим на карту мира выбранного приключения
                    startActivity(new Intent(AdventureListActivity.this, className));

                }else{
                    Log.d("загрузка", String.valueOf(worldLevels.code()));
                    // Если ответ не 200, то получаем с локальной базы сохранённую инфу
                    if(Paper.book(myTableName).read("world",null)!=null){
                        startActivity(new Intent(AdventureListActivity.this, className));
                    }else{
                        createToast("Произошла ошибка");
                    }
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                Log.d("загрузка",error.toString());
                progressBar.setVisibility(View.GONE);
                worldClick=true;
                Log.d("Проверка ошибки",error.toString());
                if(Paper.book(myTableName).read("world",null)!=null){
                    startActivity(new Intent(AdventureListActivity.this, className));
                }else{
                    createToast("Произошла ошибка");
                }
            }
        });
    }

    // Определяем прогресс каждого уровня
    public void checkAllLvlProgress(){

        for (int i=0;i<allWorldsName.size();i++){

            Integer currentProgress[] = Paper.book(allWorldsName.get(i)).read("progress",new Integer[30]);

            // Узнаём прогресс
            int worldProgress=0;

            for (int j=0;j<currentProgress.length;j++){
                if(currentProgress[j]!=null){
                    worldProgress=worldProgress+1;
                }
            }

            // Прогресс для коротких(12) уровней
            if(allWorldsName.get(i).equals("world9")||allWorldsName.get(i).equals("world10")){
                if(worldProgress==12){
                    allProgressText.get(i).setText("100%");
                }else {
                    long totalProgress = Math.round(worldProgress * 8.5);
                    allProgressText.get(i).setText(String.valueOf(totalProgress) + "%");
                }
                continue;
            }

            // Прогресс для больших(16) уровней
            if(worldProgress==16){
                allProgressText.get(i).setText("100%");
            }else {
                long totalProgress = Math.round(worldProgress * 6.5);
                allProgressText.get(i).setText(String.valueOf(totalProgress) + "%");
            }
        }
    }

    // Проверка интернет соединения
    public boolean checkInternet(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }

        return false;
    }

    // Конструктор Тоаст сообщений
    public void createToast(String text){
        Toast.makeText(this, text,
                Toast.LENGTH_SHORT).show();
    }

    // Переопределяем кнопку назад
    @Override
    public void onBackPressed() {
            backPressed=true;
            super.onBackPressed();

    }

    @Override
    public void onResume(){
        super.onResume();
        worldClick=true;
        nextScreen=false;
        checkAllLvlProgress();
        if(!MainActivity.backgroundSoundOn&&soundOn) {
            startService(new Intent(this, BackgroundMainTheme.class));
            MainActivity.backgroundSoundOn=true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(MainActivity.backgroundSoundOn&&!backPressed&&!nextScreen&&soundOn) {
            stopService(new Intent(this, BackgroundMainTheme.class));
            MainActivity.backgroundSoundOn=false;
        }

    }

    // Статистика Выбор мира
    public void metricaEvent(String worldType){
        Map<String, Object> eventAttributes = new HashMap<String, Object>();
        eventAttributes.put("Adventure", worldType);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
