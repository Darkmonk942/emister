package com.rocketraven.emister;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;

import com.rocketraven.emister.objects.AuthObject;
import com.rocketraven.emister.objects.UserMainObject;
import com.rocketraven.emister.retrofit.RetrofitRequestCallback;
import com.rocketraven.emister.retrofit.requests.PostRegistrationRequests;
import com.rocketraven.emister.retrofit.requests.RetrofitMainClass;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Response;

import static io.paperdb.Paper.book;

/**
 * Created by Darkmonk on 09.05.2016.
 */
public class SplashScreen extends AppCompatActivity {
    RetrofitMainClass retrofitMainClass;
    PostRegistrationRequests postRegistrationRequests;
    int premium=0; // Для Алло
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Подгружаем ретрофит
        retrofitMainClass=new RetrofitMainClass(this);

        // Набор запросов на регистрацию / авторизацию
        postRegistrationRequests=retrofitMainClass.retrofit.create(PostRegistrationRequests.class);

        if(!checkInternet()){
            mainScreenEnter();
            return;
        }

        // Проверка на автовход

        switch (Paper.book().read("loginType", "")){
            // Обычный автовход(логин+пароль), вк, фб
            case "common":
                Pair<String,String> userAuthPair= Paper.book().read("loginInfo",null);
                if(userAuthPair!=null){
                    Log.d("Вход","1");
                    autoAuth("common",userAuthPair);
                }else{
                    Log.d("Вход","2");
                    mainScreenEnter();
                }
                break;
            case "vk":
                String vkToken= book().read("vkToken","");
                vkLoginRequest(vkToken);
                break;
            case "fb":
                String fbToken= book().read("fbToken","");
                facebookLoginRequest(fbToken);
                break;
            case "":
                startActivity(new Intent(this,StartScreen.class));
                finish();
                break;
            default:
                mainScreenEnter();
                break;
        }


    }

    // Проверка на автовход
    // Запрос на логин в приложение
    private void autoAuth(String type,Pair<String, String> loginInfo){

        // Вход логин/пароль
        if(type.equals("common")){
            if(loginInfo==null){
                Intent intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                finish();
                return;
            }
        }

        // Запрос на вход в приложение
        Call<AuthObject> userLogin=postRegistrationRequests.userLogin(loginInfo.first,loginInfo.second);
        retrofitMainClass.createRequest(userLogin);

        retrofitMainClass.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object response) {
                Response<AuthObject> currentResponse=(Response<AuthObject>)response;
                Log.d("Вход","3 "+ String.valueOf(currentResponse.code()));
                if(currentResponse.code()==200){
                    UserMainObject userMainObject=currentResponse.body().userMainObject;

                    if(userMainObject==null){
                        mainScreenEnter();
                        return;
                    }

                    // Сохраняем юзера и токен
                    book("user").write("mainInfo", userMainObject);
                    book().write("token",currentResponse.body().accessToken);
                    Log.d("Токен",currentResponse.body().accessToken);

                    if(userMainObject.isProfileFull==0){
                        startActivity(new Intent(SplashScreen.this,UserAddInfoActivity.class));
                        finish();
                    }else{
                        // Узнаём, был ли пройден экран промокода
                        if(Paper.book().read("promocodeScreen",true)){
                            startActivity(new Intent(SplashScreen.this,PromoCodeActivity.class));
                            finish();
                        }else{
                            mainScreenEnter();
                        }
                    }
                }else{
                    mainScreenEnter();
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                mainScreenEnter();
                Log.d("Вход",error.toString());
            }
        });
    }

    // Автовход через ВК
    private void vkLoginRequest(final String token){
        Call<AuthObject> sendVkToken=postRegistrationRequests.sendVkToken(token,premium);
        retrofitMainClass.createRequest(sendVkToken);

        retrofitMainClass.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object response) {
                // Получаем ответ от сервера
                Response<AuthObject> currentResponse=(Response<AuthObject>)response;
                if(currentResponse.code()==200){

                    // Сохраняем информацию о пользователе
                    UserMainObject userMainObject=currentResponse.body().userMainObject;

                    if(userMainObject==null){
                        mainScreenEnter();
                        return;
                    }

                    // Сохраняем юзера
                    book("user").write("mainInfo",userMainObject);
                    book().write("token",currentResponse.body().accessToken);
                    // Сохраняем тип входа
                    book().write("loginType","vk");
                    book().write("vkToken",token);

                    // Проверка на отсутствие информации о юзере
                    if(userMainObject.isProfileFull==0){ // Переходим на основной экран
                        startActivity(new Intent(SplashScreen.this,UserAddInfoActivity.class));
                    }else{
                        // Узнаём, был ли пройден экран промокода
                        if(Paper.book().read("promocodeScreen",true)){
                            startActivity(new Intent(SplashScreen.this,PromoCodeActivity.class));
                            finish();
                        }else{
                            mainScreenEnter();
                        }
                    }
                    finish();


                }else{
                    mainScreenEnter();
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                mainScreenEnter();
            }
        });
    }

    // Отправка ВК токена на сервер
    private void facebookLoginRequest(final String token){
        Call<AuthObject> sendFbToken=postRegistrationRequests.sendFacebookToken(token,premium);
        retrofitMainClass.createRequest(sendFbToken);

        retrofitMainClass.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object response) {
                // Получаем ответ от сервера
                Response<AuthObject> currentResponse=(Response<AuthObject>)response;
                if(currentResponse.code()==200){

                    // Сохраняем информацию о пользователе
                    UserMainObject userMainObject=currentResponse.body().userMainObject;

                    if(userMainObject==null){
                        mainScreenEnter();
                        return;
                    }

                    // Сохраняем юзера и токен
                    book("user").write("mainInfo",userMainObject);
                    book().write("token",currentResponse.body().accessToken);
                    book().write("fbToken",token);
                    // Сохраняем тип входа
                    book().write("loginType","fb");

                    // Проверка на отсутствие информации о юзере
                    if(userMainObject.isProfileFull==0){ // Переходим на основной экран
                        startActivity(new Intent(SplashScreen.this,UserAddInfoActivity.class));
                    }else{
                        // Узнаём, был ли пройден экран промокода
                        if(Paper.book().read("promocodeScreen",true)){
                            startActivity(new Intent(SplashScreen.this,PromoCodeActivity.class));
                            finish();
                        }else{
                            mainScreenEnter();
                        }
                    }
                    finish();


                }else{
                    mainScreenEnter();
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                mainScreenEnter();
            }
        });
    }

    // Првоерка на интернет
    public boolean checkInternet(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    // Переход на главный экран
    public void mainScreenEnter(){
        Intent intent = null;
        if(Paper.book().read("loginType", "").equals("")){
            intent = new Intent(this, StartScreen.class);
        }else{
            intent = new Intent(this, MainActivity.class);
        }

        startActivity(intent);
        finish();
    }
}
